package com.wudstay.pojo;

/**
 * Created by arpit on 3/24/2017.
 */

public class PgPropertyListPojo {
    private boolean isActive;
    private boolean isFlat;
    private String mainImage;
    private double longitude;
    private double latitude;
    private int id;
    private int price;
    private int noOfBeds; // 14/7/17
    private String displayAddress;
    private String urlExt;
    private String displayName;
    private PropertyTypePojo propertyType;
    private PropertyTypePojo propertyCategoryType;
    private PropertyTypePojo propertyCategory;

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isFlat() {
        return isFlat;
    }

    public void setFlat(boolean flat) {
        isFlat = flat;
    }

    public String getMainImage() {
        return mainImage;
    }

    public void setMainImage(String mainImage) {
        this.mainImage = mainImage;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getDisplayAddress() {
        return displayAddress;
    }

    public void setDisplayAddress(String displayAddress) {
        this.displayAddress = displayAddress;
    }

    public String getUrlExt() {
        return urlExt;
    }

    public void setUrlExt(String urlExt) {
        this.urlExt = urlExt;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public PropertyTypePojo getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(PropertyTypePojo propertyType) {
        this.propertyType = propertyType;
    }

    public PropertyTypePojo getPropertyCategoryType() {
        return propertyCategoryType;
    }

    public void setPropertyCategoryType(PropertyTypePojo propertyCategoryType) {
        this.propertyCategoryType = propertyCategoryType;
    }

    public PropertyTypePojo getPropertyCategory() {
        return propertyCategory;
    }

    public void setPropertyCategory(PropertyTypePojo propertyCategory) {
        this.propertyCategory = propertyCategory;
    }
    public int getNoOfBeds() {
        return noOfBeds;
    }

    public void setNoOfBeds(int noOfBeds) {
        this.noOfBeds = noOfBeds;
    }
}
