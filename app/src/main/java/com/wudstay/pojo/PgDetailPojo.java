package com.wudstay.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Yash ji on 9/1/2016.
 */
public class PgDetailPojo implements Serializable{

    private int priceSingleOccupancy;
    private ArrayList<PgDescriptionPojo> descriptionList;
    private String pgName;
    private String pgAddress;
    private int priceDoubleOccupancy;
    private int priceTripleOccupancy;
    private int tokenAmount;
    private String contactPersonName;
    private String contactPersonMobile;
    private String contactPersonEmail;
    private String roomImageMain;
    private ArrayList<String> roomImagesOthers;
    private String displayAddress;
    private String howToReach;
    private PgAmenitiesListPojo city;
    private double longitude;
    private double latitude;
    private PgAmenitiesListPojo roomType;
    private Integer price;
    private ArrayList<PgAmenitiesListPojo> amenityList;
    private String displayName;
    private PgAmenitiesListPojo location;
    private String id;
    private boolean favouritePg;

    public int getPriceSingleOccupancy() {
        return priceSingleOccupancy;
    }

    public void setPriceSingleOccupancy(int priceSingleOccupancy) {
        this.priceSingleOccupancy = priceSingleOccupancy;
    }

    public ArrayList<PgDescriptionPojo> getDescriptionList() {
        return descriptionList;
    }

    public void setDescriptionList(ArrayList<PgDescriptionPojo> descriptionList) {
        this.descriptionList = descriptionList;
    }

    public String getPgName() {
        return pgName;
    }

    public void setPgName(String pgName) {
        this.pgName = pgName;
    }

    public String getPgAddress() {
        return pgAddress;
    }

    public void setPgAddress(String pgAddress) {
        this.pgAddress = pgAddress;
    }

    public int getPriceDoubleOccupancy() {
        return priceDoubleOccupancy;
    }

    public void setPriceDoubleOccupancy(int priceDoubleOccupancy) {
        this.priceDoubleOccupancy = priceDoubleOccupancy;
    }

    public int getPriceTripleOccupancy() {
        return priceTripleOccupancy;
    }

    public void setPriceTripleOccupancy(int priceTripleOccupancy) {
        this.priceTripleOccupancy = priceTripleOccupancy;
    }

    public String getContactPersonName() {
        return contactPersonName;
    }

    public void setContactPersonName(String contactPersonName) {
        this.contactPersonName = contactPersonName;
    }

    public String getContactPersonMobile() {
        return contactPersonMobile;
    }

    public void setContactPersonMobile(String contactPersonMobile) {
        this.contactPersonMobile = contactPersonMobile;
    }

    public String getContactPersonEmail() {
        return contactPersonEmail;
    }

    public void setContactPersonEmail(String contactPersonEmail) {
        this.contactPersonEmail = contactPersonEmail;
    }

    public String getRoomImageMain() {
        return roomImageMain;
    }

    public void setRoomImageMain(String roomImageMain) {
        this.roomImageMain = roomImageMain;
    }

    public ArrayList<String> getRoomImagesOthers() {
        return roomImagesOthers;
    }

    public void setRoomImagesOthers(ArrayList<String> roomImagesOthers) {
        this.roomImagesOthers = roomImagesOthers;
    }

    public String getDisplayAddress() {
        return displayAddress;
    }

    public void setDisplayAddress(String displayAddress) {
        this.displayAddress = displayAddress;
    }

    public PgAmenitiesListPojo getCity() {
        return city;
    }

    public void setCity(PgAmenitiesListPojo city) {
        this.city = city;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public PgAmenitiesListPojo getRoomType() {
        return roomType;
    }

    public void setRoomType(PgAmenitiesListPojo roomType) {
        this.roomType = roomType;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public ArrayList<PgAmenitiesListPojo> getAmenityList() {
        return amenityList;
    }

    public void setAmenityList(ArrayList<PgAmenitiesListPojo> amenityList) {
        this.amenityList = amenityList;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public PgAmenitiesListPojo getLocation() {
        return location;
    }

    public void setLocation(PgAmenitiesListPojo location) {
        this.location = location;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHowToReach() {
        return howToReach;
    }

    public void setHowToReach(String howToReach) {
        this.howToReach = howToReach;
    }

    public boolean isFavouritePg() {
        return favouritePg;
    }

    public void setFavouritePg(boolean favouritePg) {
        this.favouritePg = favouritePg;
    }

    public int getTokenAmount() {
        return tokenAmount;
    }

    public void setTokenAmount(int tokenAmount) {
        this.tokenAmount = tokenAmount;
    }
}
