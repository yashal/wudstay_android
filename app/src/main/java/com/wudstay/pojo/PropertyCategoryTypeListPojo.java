package com.wudstay.pojo;

import java.io.Serializable;

/**
 * Created by arpit on 3/24/2017.
 */

public class PropertyCategoryTypeListPojo  implements Serializable {
    private int categoryTypeId;
    private String categoryTypeName;
    private boolean checked = false;

    public int getCategoryTypeId() {
        return categoryTypeId;
    }

    public void setCategoryTypeId(int categoryTypeId) {
        this.categoryTypeId = categoryTypeId;
    }

    public String getCategoryTypeName() {
        return categoryTypeName;
    }

    public void setCategoryTypeName(String categoryTypeName) {
        this.categoryTypeName = categoryTypeName;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
