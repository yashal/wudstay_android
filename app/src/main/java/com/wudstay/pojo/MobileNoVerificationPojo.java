package com.wudstay.pojo;

/**
 * Created by Yash ji on 9/9/2016.
 */
public class MobileNoVerificationPojo extends BasePojo {

    private boolean responseObject;

    public boolean isResponseObject() {
        return responseObject;
    }

    public void setResponseObject(boolean responseObject) {
        this.responseObject = responseObject;
    }
}
