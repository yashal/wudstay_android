package com.wudstay.pojo;

import java.io.Serializable;

/**
 * Created by Yash on 4/20/2017.
 */

public class pgDetailNewCityPojo implements Serializable {

    private int linkedId;
    private int id;
    private String name;

    public int getLinkedId() {
        return linkedId;
    }

    public void setLinkedId(int linkedId) {
        this.linkedId = linkedId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
