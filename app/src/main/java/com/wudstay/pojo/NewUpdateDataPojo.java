package com.wudstay.pojo;

/**
 * Created by Yash ji on 9/28/2016.
 */

public class NewUpdateDataPojo {

    private String MESSAGE_ON_NONE_MANDATORY_UPDATE;
    private boolean NEW_UPDATE_IS_MANDATORY;
    private String MESSAGE_ON_MANDATORY_UPDATE;
    private LatestVersionPojo LATEST_VERSION;

    public String getMESSAGE_ON_NONE_MANDATORY_UPDATE() {
        return MESSAGE_ON_NONE_MANDATORY_UPDATE;
    }

    public void setMESSAGE_ON_NONE_MANDATORY_UPDATE(String MESSAGE_ON_NONE_MANDATORY_UPDATE) {
        this.MESSAGE_ON_NONE_MANDATORY_UPDATE = MESSAGE_ON_NONE_MANDATORY_UPDATE;
    }

    public boolean isNEW_UPDATE_IS_MANDATORY() {
        return NEW_UPDATE_IS_MANDATORY;
    }

    public void setNEW_UPDATE_IS_MANDATORY(boolean NEW_UPDATE_IS_MANDATORY) {
        this.NEW_UPDATE_IS_MANDATORY = NEW_UPDATE_IS_MANDATORY;
    }

    public String getMESSAGE_ON_MANDATORY_UPDATE() {
        return MESSAGE_ON_MANDATORY_UPDATE;
    }

    public void setMESSAGE_ON_MANDATORY_UPDATE(String MESSAGE_ON_MANDATORY_UPDATE) {
        this.MESSAGE_ON_MANDATORY_UPDATE = MESSAGE_ON_MANDATORY_UPDATE;
    }

    public LatestVersionPojo getLATEST_VERSION() {
        return LATEST_VERSION;
    }

    public void setLATEST_VERSION(LatestVersionPojo LATEST_VERSION) {
        this.LATEST_VERSION = LATEST_VERSION;
    }
}
