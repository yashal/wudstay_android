package com.wudstay.pojo;

import java.io.Serializable;

/**
 * Created by arpit on 3/24/2017.
 */

public class PropertyTypesListPojo implements Serializable {

    private int pgTypeId;
    private boolean checked;
    private String pgType;

    public int getPgTypeId() {
        return pgTypeId;
    }

    public void setPgTypeId(int pgTypeId) {
        this.pgTypeId = pgTypeId;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getPgType() {
        return pgType;
    }

    public void setPgType(String pgType) {
        this.pgType = pgType;
    }
}
