package com.wudstay.pojo;

import java.io.Serializable;

/**
 * Created by arpit on 3/24/2017.
 */

public class PropertyCategoryListPojo implements Serializable {
    private int categoryId;
    private int categoryTypeId;
    private String categoryName;
    private boolean checked;
    private boolean mFlag = false;
    private PropertyCategoryTypeListPojo categoryType;

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getCategoryTypeId() {
        return categoryTypeId;
    }

    public void setCategoryTypeId(int categoryTypeId) {
        this.categoryTypeId = categoryTypeId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public PropertyCategoryTypeListPojo getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(PropertyCategoryTypeListPojo categoryType) {
        this.categoryType = categoryType;
    }

    public boolean ismFlag() {
        return mFlag;
    }

    public void setmFlag(boolean mFlag) {
        this.mFlag = mFlag;
    }
}
