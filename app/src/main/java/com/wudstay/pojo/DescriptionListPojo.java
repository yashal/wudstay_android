package com.wudstay.pojo;

import java.io.Serializable;

/**
 * Created by Yash on 4/20/2017.
 */

public class DescriptionListPojo implements Serializable {

    private String descriptionTitle;
    private String description;

    public String getDescriptionTitle() {
        return descriptionTitle;
    }

    public void setDescriptionTitle(String descriptionTitle) {
        this.descriptionTitle = descriptionTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
