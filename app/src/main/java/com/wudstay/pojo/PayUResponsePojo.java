package com.wudstay.pojo;

/**
 * Created by Yash ji on 9/28/2016.
 */

public class PayUResponsePojo extends BasePojo {

    private PayUResponseDataPojo responseObject;

    public PayUResponseDataPojo getResponseObject() {
        return responseObject;
    }

    public void setResponseObject(PayUResponseDataPojo responseObject) {
        this.responseObject = responseObject;
    }
}
