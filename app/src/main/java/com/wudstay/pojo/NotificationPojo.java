package com.wudstay.pojo;

/**
 * Created by Yash ji on 9/26/2016.
 */

public class NotificationPojo extends BasePojo {

    private NotificationDataPojo responseObject;

    public NotificationDataPojo getResponseObject() {
        return responseObject;
    }

    public void setResponseObject(NotificationDataPojo responseObject) {
        this.responseObject = responseObject;
    }
}
