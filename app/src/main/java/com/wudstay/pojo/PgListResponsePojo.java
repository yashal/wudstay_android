package com.wudstay.pojo;

import java.io.Serializable;

/**
 * Created by Yash ji on 9/1/2016.
 */
public class PgListResponsePojo extends BasePojo implements Serializable {

    private PgListsPojo responseObject;

    public PgListsPojo getResponseObject() {
        return responseObject;
    }

    public void setResponseObject(PgListsPojo responseObject) {
        this.responseObject = responseObject;
    }
}
