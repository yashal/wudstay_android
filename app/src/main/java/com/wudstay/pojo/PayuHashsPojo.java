package com.wudstay.pojo;

/**
 * Created by Yash ji on 9/14/2016.
 */
public class PayuHashsPojo {

    private String salt;
    private String detailsForMobileSdk;
    private String getUserCardHash;
    private String editUserCardHash;
    private String saveUserCardHash;
    private String paymentHash;
    private String merchantCodesHash;
    private String mobileSdk;
    private String deleteHash;
    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDeleteHash() {
        return deleteHash;
    }

    public void setDeleteHash(String deleteHash) {
        this.deleteHash = deleteHash;
    }

    public String getMobileSdk() {
        return mobileSdk;
    }

    public void setMobileSdk(String mobileSdk) {
        this.mobileSdk = mobileSdk;
    }

    public String getMerchantCodesHash() {
        return merchantCodesHash;
    }

    public void setMerchantCodesHash(String merchantCodesHash) {
        this.merchantCodesHash = merchantCodesHash;
    }

    public String getPaymentHash() {
        return paymentHash;
    }

    public void setPaymentHash(String paymentHash) {
        this.paymentHash = paymentHash;
    }

    public String getSaveUserCardHash() {
        return saveUserCardHash;
    }

    public void setSaveUserCardHash(String saveUserCardHash) {
        this.saveUserCardHash = saveUserCardHash;
    }

    public String getEditUserCardHash() {
        return editUserCardHash;
    }

    public void setEditUserCardHash(String editUserCardHash) {
        this.editUserCardHash = editUserCardHash;
    }

    public String getGetUserCardHash() {
        return getUserCardHash;
    }

    public void setGetUserCardHash(String getUserCardHash) {
        this.getUserCardHash = getUserCardHash;
    }

    public String getDetailsForMobileSdk() {
        return detailsForMobileSdk;
    }

    public void setDetailsForMobileSdk(String detailsForMobileSdk) {
        this.detailsForMobileSdk = detailsForMobileSdk;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }
}
