package com.wudstay.pojo;

import java.io.Serializable;

/**
 * Created by arpit on 3/28/2017.
 */

public class OccupancyListPojo implements Serializable {
    private String occupancyName;
    private String priceUnitLabel;
    private int price;
    private int token;
    private int bedPerRoom;
    private int id;

    public String getOccupancyName() {
        return occupancyName;
    }

    public void setOccupancyName(String occupancyName) {
        this.occupancyName = occupancyName;
    }

    public String getPriceUnitLabel() {
        return priceUnitLabel;
    }

    public void setPriceUnitLabel(String priceUnitLabel) {
        this.priceUnitLabel = priceUnitLabel;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getBedPerRoom() {
        return bedPerRoom;
    }

    public void setBedPerRoom(int bedPerRoom) {
        this.bedPerRoom = bedPerRoom;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getToken() {
        return token;
    }

    public void setToken(int token) {
        this.token = token;
    }
}
