package com.wudstay.pojo;

/**
 * Created by Administrator on 12-Sep-16.
 */
public class ScheduleVisitDetailPojo {

    private String BOOKING_ID;
    private String PG_NAME;
    private String PG_PRICE;
    private String GUEST_PHONE;
    private String DATE_AND_TIME_OF_VISIT;
    private String GUEST;
    private String PG_ADDRESS;
    private String GUEST_EMAIL;

    public String getBOOKING_ID() {
        return BOOKING_ID;
    }

    public void setBOOKING_ID(String BOOKING_ID) {
        this.BOOKING_ID = BOOKING_ID;
    }

    public String getPG_NAME() {
        return PG_NAME;
    }

    public void setPG_NAME(String PG_NAME) {
        this.PG_NAME = PG_NAME;
    }

    public String getPG_PRICE() {
        return PG_PRICE;
    }

    public void setPG_PRICE(String PG_PRICE) {
        this.PG_PRICE = PG_PRICE;
    }

    public String getGUEST_PHONE() {
        return GUEST_PHONE;
    }

    public void setGUEST_PHONE(String GUEST_PHONE) {
        this.GUEST_PHONE = GUEST_PHONE;
    }

    public String getDATE_AND_TIME_OF_VISIT() {
        return DATE_AND_TIME_OF_VISIT;
    }

    public void setDATE_AND_TIME_OF_VISIT(String DATE_AND_TIME_OF_VISIT) {
        this.DATE_AND_TIME_OF_VISIT = DATE_AND_TIME_OF_VISIT;
    }

    public String getGUEST() {
        return GUEST;
    }

    public void setGUEST(String GUEST) {
        this.GUEST = GUEST;
    }

    public String getPG_ADDRESS() {
        return PG_ADDRESS;
    }

    public void setPG_ADDRESS(String PG_ADDRESS) {
        this.PG_ADDRESS = PG_ADDRESS;
    }

    public String getGUEST_EMAIL() {
        return GUEST_EMAIL;
    }

    public void setGUEST_EMAIL(String GUEST_EMAIL) {
        this.GUEST_EMAIL = GUEST_EMAIL;
    }
}
