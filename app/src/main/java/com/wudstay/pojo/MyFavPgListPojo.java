package com.wudstay.pojo;

/**
 * Created by yash on 11/16/2016.
 */

public class MyFavPgListPojo {

    private int pgId;
    private String pgName;
    private String pgAddress;
    private MyFavPgdetailPojo pgDetail;


    public int getPgId() {
        return pgId;
    }

    public void setPgId(int pgId) {
        this.pgId = pgId;
    }

    public String getPgName() {
        return pgName;
    }

    public void setPgName(String pgName) {
        this.pgName = pgName;
    }

    public String getPgAddress() {
        return pgAddress;
    }

    public void setPgAddress(String pgAddress) {
        this.pgAddress = pgAddress;
    }

    public MyFavPgdetailPojo getPgDetail() {
        return pgDetail;
    }

    public void setPgDetail(MyFavPgdetailPojo pgDetail) {
        this.pgDetail = pgDetail;
    }
}
