package com.wudstay.pojo;

import java.util.ArrayList;

/**
 * Created by Yash ji on 9/23/2016.
 */

public class PaymentHistoryPojo extends BasePojo{

    private ArrayList<PaymentHistoryDataPojo> responseObject;

    public ArrayList<PaymentHistoryDataPojo> getResponseObject() {
        return responseObject;
    }

    public void setResponseObject(ArrayList<PaymentHistoryDataPojo> responseObject) {
        this.responseObject = responseObject;
    }
}