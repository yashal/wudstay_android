package com.wudstay.pojo;

/**
 * Created by Yash ji on 9/28/2016.
 */

public class LatestVersionPojo {

    private String lastUpdatedDate;
    private String currentVersion;
    private boolean isMandatoryUpdate;
    private String appType;
    private int id;

    public String getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(String lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public String getCurrentVersion() {
        return currentVersion;
    }

    public void setCurrentVersion(String currentVersion) {
        this.currentVersion = currentVersion;
    }

    public boolean isMandatoryUpdate() {
        return isMandatoryUpdate;
    }

    public void setMandatoryUpdate(boolean mandatoryUpdate) {
        isMandatoryUpdate = mandatoryUpdate;
    }

    public String getAppType() {
        return appType;
    }

    public void setAppType(String appType) {
        this.appType = appType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
