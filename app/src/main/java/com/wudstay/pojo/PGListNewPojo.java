package com.wudstay.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by arpit on 3/24/2017.
 */

public class PGListNewPojo implements Serializable {
    private ArrayList<PropertyCategoryTypeListPojo> propCatTypesList;
    private ArrayList<PropertyCategoryListPojo> propCatsList;
    private ArrayList<PropertyTypesListPojo> propTypesList;
//    private ArrayList<PgListLocationPojo> locationList;
    private ArrayList<PgPropertyListPojo> lstProperty;
    private ArrayList<RoomOccupancyListPojo> roomOccupancyList;
    private double cityLongitude;
    private double cityLatitude;
    private int cityMapZoomLevel;
    private int currentPage;
    private int noPartialPages;
    private int totalRecordsCount;
    private int lastRecordsCounter;
    private int errorCode;
    private String error;
    private boolean hasMorePages;

    public ArrayList<PropertyCategoryTypeListPojo> getPropCatTypesList() {
        return propCatTypesList;
    }

    public void setPropCatTypesList(ArrayList<PropertyCategoryTypeListPojo> propCatTypesList) {
        this.propCatTypesList = propCatTypesList;
    }

    public ArrayList<PropertyCategoryListPojo> getPropCatsList() {
        return propCatsList;
    }

    public void setPropCatsList(ArrayList<PropertyCategoryListPojo> propCatsList) {
        this.propCatsList = propCatsList;
    }

    public ArrayList<PropertyTypesListPojo> getPropTypesList() {
        return propTypesList;
    }

    public void setPropTypesList(ArrayList<PropertyTypesListPojo> propTypesList) {
        this.propTypesList = propTypesList;
    }

//    public ArrayList<PgListLocationPojo> getLocationList() {
//        return locationList;
//    }
//
//    public void setLocationList(ArrayList<PgListLocationPojo> locationList) {
//        this.locationList = locationList;
//    }

    public ArrayList<PgPropertyListPojo> getLstProperty() {
        return lstProperty;
    }

    public void setLstProperty(ArrayList<PgPropertyListPojo> lstProperty) {
        this.lstProperty = lstProperty;
    }

    public double getCityLongitude() {
        return cityLongitude;
    }

    public void setCityLongitude(double cityLongitude) {
        this.cityLongitude = cityLongitude;
    }

    public double getCityLatitude() {
        return cityLatitude;
    }

    public void setCityLatitude(double cityLatitude) {
        this.cityLatitude = cityLatitude;
    }

    public int getCityMapZoomLevel() {
        return cityMapZoomLevel;
    }

    public void setCityMapZoomLevel(int cityMapZoomLevel) {
        this.cityMapZoomLevel = cityMapZoomLevel;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getNoPartialPages() {
        return noPartialPages;
    }

    public void setNoPartialPages(int noPartialPages) {
        this.noPartialPages = noPartialPages;
    }

    public int getTotalRecordsCount() {
        return totalRecordsCount;
    }

    public void setTotalRecordsCount(int totalRecordsCount) {
        this.totalRecordsCount = totalRecordsCount;
    }

    public int getLastRecordsCounter() {
        return lastRecordsCounter;
    }

    public void setLastRecordsCounter(int lastRecordsCounter) {
        this.lastRecordsCounter = lastRecordsCounter;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public boolean isHasMorePages() {
        return hasMorePages;
    }

    public void setHasMorePages(boolean hasMorePages) {
        this.hasMorePages = hasMorePages;
    }

    public ArrayList<RoomOccupancyListPojo> getRoomOccupancyList() {
        return roomOccupancyList;
    }

    public void setRoomOccupancyList(ArrayList<RoomOccupancyListPojo> roomOccupancyList) {
        this.roomOccupancyList = roomOccupancyList;
    }
}
