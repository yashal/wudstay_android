package com.wudstay.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by arpit on 3/28/2017.
 */

public class PGDetailNewPojo implements Serializable{
    private pgDetailNewCityPojo city;
    private double longitude;
    private double latitude;
    private int price;
    private int tokenAmount;
    private int selectedOccupancy;
    private int id;
    private String displayAddress;
    private String howToReach;
    private String contactPersonName;
    private String contactPersonNumber;
    private String contactPersonEmail;
    private String displayName;
    private int noOfRooms;
    private boolean isActive;
    private boolean isFlat;
    private boolean favouritePg;
    private ArrayList<PgAmenitiesListPojo> amenityList;
    private ArrayList<OccupancyListPojo> occupancyList;
    private ArrayList<String> imageList;
    private String mainImage;
    private ArrayList<DescriptionListPojo> descriptionList;

    public int getNoOfRooms() {
        return noOfRooms;
    }

    public void setNoOfRooms(int noOfRooms) {
        this.noOfRooms = noOfRooms;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getTokenAmount() {
        return tokenAmount;
    }

    public void setTokenAmount(int tokenAmount) {
        this.tokenAmount = tokenAmount;
    }

    public int getSelectedOccupancy() {
        return selectedOccupancy;
    }

    public void setSelectedOccupancy(int selectedOccupancy) {
        this.selectedOccupancy = selectedOccupancy;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDisplayAddress() {
        return displayAddress;
    }

    public void setDisplayAddress(String displayAddress) {
        this.displayAddress = displayAddress;
    }

    public String getHowToReach() {
        return howToReach;
    }

    public void setHowToReach(String howToReach) {
        this.howToReach = howToReach;
    }

    public String getContactPersonName() {
        return contactPersonName;
    }

    public void setContactPersonName(String contactPersonName) {
        this.contactPersonName = contactPersonName;
    }

    public String getContactPersonNumber() {
        return contactPersonNumber;
    }

    public void setContactPersonNumber(String contactPersonNumber) {
        this.contactPersonNumber = contactPersonNumber;
    }

    public String getContactPersonEmail() {
        return contactPersonEmail;
    }

    public void setContactPersonEmail(String contactPersonEmail) {
        this.contactPersonEmail = contactPersonEmail;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isFlat() {
        return isFlat;
    }

    public void setFlat(boolean flat) {
        isFlat = flat;
    }

    public boolean isFavouritePg() {
        return favouritePg;
    }

    public void setFavouritePg(boolean favouritePg) {
        this.favouritePg = favouritePg;
    }

    public ArrayList<PgAmenitiesListPojo> getAmenityList() {
        return amenityList;
    }

    public void setAmenityList(ArrayList<PgAmenitiesListPojo> amenityList) {
        this.amenityList = amenityList;
    }

    public ArrayList<OccupancyListPojo> getOccupancyList() {
        return occupancyList;
    }

    public void setOccupancyList(ArrayList<OccupancyListPojo> occupancyList) {
        this.occupancyList = occupancyList;
    }

    public ArrayList<String> getImageList() {
        return imageList;
    }

    public void setImageList(ArrayList<String> imageList) {
        this.imageList = imageList;
    }

    public pgDetailNewCityPojo getCity() {
        return city;
    }

    public void setCity(pgDetailNewCityPojo city) {
        this.city = city;
    }

    public String getMainImage() {
        return mainImage;
    }

    public void setMainImage(String mainImage) {
        this.mainImage = mainImage;
    }

    public ArrayList<DescriptionListPojo> getDescriptionList() {
        return descriptionList;
    }

    public void setDescriptionList(ArrayList<DescriptionListPojo> descriptionList) {
        this.descriptionList = descriptionList;
    }
}
