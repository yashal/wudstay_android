package com.wudstay.pojo;

/**
 * Created by arpit on 3/24/2017.
 */

public class PgListNewResponsePojo extends BasePojo {

    private PGListNewPojo responseObject;

    public PGListNewPojo getResponseObject() {
        return responseObject;
    }

    public void setResponseObject(PGListNewPojo responseObject) {
        this.responseObject = responseObject;
    }
}
