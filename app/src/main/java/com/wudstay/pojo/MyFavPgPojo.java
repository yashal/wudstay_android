package com.wudstay.pojo;

import java.util.ArrayList;

/**
 * Created by yash on 11/16/2016.
 */

public class MyFavPgPojo extends BasePojo {

    private ArrayList<MyFavPgListPojo> responseObject;

    public ArrayList<MyFavPgListPojo> getResponseObject() {
        return responseObject;
    }

    public void setResponseObject(ArrayList<MyFavPgListPojo> responseObject) {
        this.responseObject = responseObject;
    }
}
