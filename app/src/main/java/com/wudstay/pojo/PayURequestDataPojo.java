package com.wudstay.pojo;

/**
 * Created by Yash ji on 9/14/2016.
 */
public class PayURequestDataPojo {
    private PayUVOPojo PayUVO;
    private PayuHashsPojo PayuHashs;

    public PayUVOPojo getPayUVO() {
        return PayUVO;
    }

    public void setPayUVO(PayUVOPojo payUVO) {
        PayUVO = payUVO;
    }

    public PayuHashsPojo getPayuHashs() {
        return PayuHashs;
    }

    public void setPayuHashs(PayuHashsPojo payuHashs) {
        PayuHashs = payuHashs;
    }
}
