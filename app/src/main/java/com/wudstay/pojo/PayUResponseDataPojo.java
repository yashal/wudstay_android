package com.wudstay.pojo;

/**
 * Created by Yash ji on 9/28/2016.
 */

public class PayUResponseDataPojo {

    private int amount;
    private String guestName;
    private String guestEmail;
    private int pgId;
    private int sourceOfBooking;
    private String transactionId;
    private String paymentStatus;
    private String occupancy;
    private String roomNumber;
    private String guestMobNo;
    private long moveInDate;
    private int id;

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getGuestName() {
        return guestName;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    public String getGuestEmail() {
        return guestEmail;
    }

    public void setGuestEmail(String guestEmail) {
        this.guestEmail = guestEmail;
    }

    public int getPgId() {
        return pgId;
    }

    public void setPgId(int pgId) {
        this.pgId = pgId;
    }

    public int getSourceOfBooking() {
        return sourceOfBooking;
    }

    public void setSourceOfBooking(int sourceOfBooking) {
        this.sourceOfBooking = sourceOfBooking;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getOccupancy() {
        return occupancy;
    }

    public void setOccupancy(String occupancy) {
        this.occupancy = occupancy;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getGuestMobNo() {
        return guestMobNo;
    }

    public void setGuestMobNo(String guestMobNo) {
        this.guestMobNo = guestMobNo;
    }

    public long getMoveInDate() {
        return moveInDate;
    }

    public void setMoveInDate(long moveInDate) {
        this.moveInDate = moveInDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
