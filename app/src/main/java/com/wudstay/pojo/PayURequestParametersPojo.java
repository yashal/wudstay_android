package com.wudstay.pojo;

/**
 * Created by Yash ji on 9/14/2016.
 */
public class PayURequestParametersPojo extends BasePojo {

   private PayURequestDataPojo responseObject;

    public PayURequestDataPojo getResponseObject() {
        return responseObject;
    }

    public void setResponseObject(PayURequestDataPojo responseObject) {
        this.responseObject = responseObject;
    }
}
