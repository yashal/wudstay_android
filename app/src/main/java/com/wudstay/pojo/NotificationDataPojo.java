package com.wudstay.pojo;

import java.util.ArrayList;

/**
 * Created by Yash ji on 9/26/2016.
 */

public class NotificationDataPojo {

    private int TotalComplaints;
    private ArrayList<NotificationListPojo> ListOfComplaints;

    public int getTotalComplaints() {
        return TotalComplaints;
    }

    public void setTotalComplaints(int totalComplaints) {
        TotalComplaints = totalComplaints;
    }

    public ArrayList<NotificationListPojo> getListOfComplaints() {
        return ListOfComplaints;
    }

    public void setListOfComplaints(ArrayList<NotificationListPojo> listOfComplaints) {
        ListOfComplaints = listOfComplaints;
    }
}
