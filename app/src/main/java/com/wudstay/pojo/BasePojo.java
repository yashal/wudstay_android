package com.wudstay.pojo;

/**
 * Created by Yash ji on 8/31/2016.
 */
public class BasePojo {

    private int responseCode;
    private String message;
    private String title;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
