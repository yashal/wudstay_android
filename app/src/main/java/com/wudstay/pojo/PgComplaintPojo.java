package com.wudstay.pojo;

/**
 * Created by Yash ji on 9/22/2016.
 */

public class PgComplaintPojo extends BasePojo {

    private PgComplaintDataPojo responseObject;

    public PgComplaintDataPojo getResponseObject() {
        return responseObject;
    }

    public void setResponseObject(PgComplaintDataPojo responseObject) {
        this.responseObject = responseObject;
    }
}
