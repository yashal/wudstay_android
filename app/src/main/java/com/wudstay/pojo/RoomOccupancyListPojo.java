package com.wudstay.pojo;

import java.io.Serializable;

/**
 * Created by yash on 5/3/2017.
 */

public class RoomOccupancyListPojo implements Serializable {

    private String occupancyName;
    private String lastUpdatedBy;
    private long lastUpdatedDate;
    private boolean isActive;
    private int bedPerRoom;
    private int id;

    private boolean mflag = false;

    public boolean isMflag() {
        return mflag;
    }

    public void setMflag(boolean mflag) {
        this.mflag = mflag;
    }

    public String getOccupancyName() {
        return occupancyName;
    }

    public void setOccupancyName(String occupancyName) {
        this.occupancyName = occupancyName;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public long getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(long lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public int getBedPerRoom() {
        return bedPerRoom;
    }

    public void setBedPerRoom(int bedPerRoom) {
        this.bedPerRoom = bedPerRoom;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



}
