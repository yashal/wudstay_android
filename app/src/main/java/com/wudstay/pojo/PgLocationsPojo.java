package com.wudstay.pojo;

import java.util.ArrayList;

/**
 * Created by Yash ji on 8/31/2016.
 */
public class PgLocationsPojo extends BasePojo{

    private ArrayList<CityLocationsListPojo> responseObject;

    public ArrayList<CityLocationsListPojo> getResponseObject() {
        return responseObject;
    }

    public void setResponseObject(ArrayList<CityLocationsListPojo> responseObject) {
        this.responseObject = responseObject;
    }
}
