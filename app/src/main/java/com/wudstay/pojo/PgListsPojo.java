package com.wudstay.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Yash ji on 9/1/2016.
 */
public class PgListsPojo implements Serializable{

    private ArrayList<PgListLocationPojo> locationList;
    private ArrayList<PgDetailPojo> wsPgList;
    private ArrayList<PgTypeListPojo> pgTypeList;
    private String cityName;
    private int cityId;

    public ArrayList<PgListLocationPojo> getLocationList() {
        return locationList;
    }

    public void setLocationList(ArrayList<PgListLocationPojo> locationList) {
        this.locationList = locationList;
    }

    public ArrayList<PgDetailPojo> getWsPgList() {
        return wsPgList;
    }

    public void setWsPgList(ArrayList<PgDetailPojo> wsPgList) {
        this.wsPgList = wsPgList;
    }

    public ArrayList<PgTypeListPojo> getPgTypeList() {
        return pgTypeList;
    }

    public void setPgTypeList(ArrayList<PgTypeListPojo> pgTypeList) {
        this.pgTypeList = pgTypeList;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }
}
