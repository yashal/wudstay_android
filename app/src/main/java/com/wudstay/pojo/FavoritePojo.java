package com.wudstay.pojo;

/**
 * Created by arpit on 10/13/2016.
 */

public class FavoritePojo {
    private boolean isActive;
    private String lastUpdatedBy;
    private Long lastUpdatedDate;
    private Long createdDate;
    private int pgGuestId;
    private int pgId;
    private int favoriteId;

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public Long getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(Long lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public Long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Long createdDate) {
        this.createdDate = createdDate;
    }

    public int getPgGuestId() {
        return pgGuestId;
    }

    public void setPgGuestId(int pgGuestId) {
        this.pgGuestId = pgGuestId;
    }

    public int getPgId() {
        return pgId;
    }

    public void setPgId(int pgId) {
        this.pgId = pgId;
    }

    public int getFavoriteId() {
        return favoriteId;
    }

    public void setFavoriteId(int favoriteId) {
        this.favoriteId = favoriteId;
    }
}
