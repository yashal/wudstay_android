package com.wudstay.pojo;

/**
 * Created by Administrator on 14-Sep-16.
 */
public class ProfileDataPojo {

    private String strDateOfBirth;
    private String strArrivalDate;
    private String pgLocation;
    private String pgCity;
    private String pgId;
    private String candidateDp;
    private int pgTariff;
    private int mealsCharges;
    private int otherCharges;
    private String mobile;
    private String firstName;
    private String lastName;
    private String email;
    private String cityName;
    private String occupancy;
    private String phone;
    private String stateId;
    private String gender;
    private String homeAddressLine1;
    private String homeAddressLine2;
    private String pin;
    private String emergencyContactPersonFirstName;
    private String emergencyContactPersonLastName;
    private String emergencyContactPersonMobile;
    private String emergencyContactPersonEmail;
    private String emergencyContactPersonRelation;
    private String emergencyContactPersonFullAddress;
    private String emergencyContactPersonPhone;
    private int securityDeposit;
    private String id;

    public String getStrDateOfBirth() {
        return strDateOfBirth;
    }

    public void setStrDateOfBirth(String strDateOfBirth) {
        this.strDateOfBirth = strDateOfBirth;
    }

    public String getStrArrivalDate() {
        return strArrivalDate;
    }

    public void setStrArrivalDate(String strArrivalDate) {
        this.strArrivalDate = strArrivalDate;
    }

    public String getPgLocation() {
        return pgLocation;
    }

    public void setPgLocation(String pgLocation) {
        this.pgLocation = pgLocation;
    }

    public String getPgCity() {
        return pgCity;
    }

    public void setPgCity(String pgCity) {
        this.pgCity = pgCity;
    }

    public String getPgId() {
        return pgId;
    }

    public void setPgId(String pgId) {
        this.pgId = pgId;
    }

    public String getCandidateDp() {
        return candidateDp;
    }

    public void setCandidateDp(String candidateDp) {
        this.candidateDp = candidateDp;
    }

    public int getPgTariff() {
        return pgTariff;
    }

    public void setPgTariff(int pgTariff) {
        this.pgTariff = pgTariff;
    }

    public int getMealsCharges() {
        return mealsCharges;
    }

    public void setMealsCharges(int mealsCharges) {
        this.mealsCharges = mealsCharges;
    }

    public int getOtherCharges() {
        return otherCharges;
    }

    public void setOtherCharges(int otherCharges) {
        this.otherCharges = otherCharges;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getOccupancy() {
        return occupancy;
    }

    public void setOccupancy(String occupancy) {
        this.occupancy = occupancy;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getHomeAddressLine1() {
        return homeAddressLine1;
    }

    public void setHomeAddressLine1(String homeAddressLine1) {
        this.homeAddressLine1 = homeAddressLine1;
    }

    public String getHomeAddressLine2() {
        return homeAddressLine2;
    }

    public void setHomeAddressLine2(String homeAddressLine2) {
        this.homeAddressLine2 = homeAddressLine2;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getEmergencyContactPersonFirstName() {
        return emergencyContactPersonFirstName;
    }

    public void setEmergencyContactPersonFirstName(String emergencyContactPersonFirstName) {
        this.emergencyContactPersonFirstName = emergencyContactPersonFirstName;
    }

    public String getEmergencyContactPersonLastName() {
        return emergencyContactPersonLastName;
    }

    public void setEmergencyContactPersonLastName(String emergencyContactPersonLastName) {
        this.emergencyContactPersonLastName = emergencyContactPersonLastName;
    }

    public String getEmergencyContactPersonMobile() {
        return emergencyContactPersonMobile;
    }

    public void setEmergencyContactPersonMobile(String emergencyContactPersonMobile) {
        this.emergencyContactPersonMobile = emergencyContactPersonMobile;
    }

    public String getEmergencyContactPersonEmail() {
        return emergencyContactPersonEmail;
    }

    public void setEmergencyContactPersonEmail(String emergencyContactPersonEmail) {
        this.emergencyContactPersonEmail = emergencyContactPersonEmail;
    }

    public String getEmergencyContactPersonRelation() {
        return emergencyContactPersonRelation;
    }

    public void setEmergencyContactPersonRelation(String emergencyContactPersonRelation) {
        this.emergencyContactPersonRelation = emergencyContactPersonRelation;
    }

    public String getEmergencyContactPersonFullAddress() {
        return emergencyContactPersonFullAddress;
    }

    public void setEmergencyContactPersonFullAddress(String emergencyContactPersonFullAddress) {
        this.emergencyContactPersonFullAddress = emergencyContactPersonFullAddress;
    }

    public String getEmergencyContactPersonPhone() {
        return emergencyContactPersonPhone;
    }

    public void setEmergencyContactPersonPhone(String emergencyContactPersonPhone) {
        this.emergencyContactPersonPhone = emergencyContactPersonPhone;
    }

    public int getSecurityDeposit() {
        return securityDeposit;
    }

    public void setSecurityDeposit(int securityDeposit) {
        this.securityDeposit = securityDeposit;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
