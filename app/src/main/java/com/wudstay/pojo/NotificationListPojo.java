package com.wudstay.pojo;

/**
 * Created by Yash ji on 9/26/2016.
 */

public class NotificationListPojo {

    private String description;
    private long lastModifyTime;
    private int pgGuestId;
    private int notificationType;
    private long createdTime;
    private String statusDetail;
    private String complaintTypeDetail;
    private String details;
    private int id;
    private int status;
    private String strCreatedTime;
    private String strLastModifyTime;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getLastModifyTime() {
        return lastModifyTime;
    }

    public void setLastModifyTime(long lastModifyTime) {
        this.lastModifyTime = lastModifyTime;
    }

    public int getPgGuestId() {
        return pgGuestId;
    }

    public void setPgGuestId(int pgGuestId) {
        this.pgGuestId = pgGuestId;
    }

    public int getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(int notificationType) {
        this.notificationType = notificationType;
    }

    public long getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(long createdTime) {
        this.createdTime = createdTime;
    }

    public String getStatusDetail() {
        return statusDetail;
    }

    public void setStatusDetail(String statusDetail) {
        this.statusDetail = statusDetail;
    }

    public String getComplaintTypeDetail() {
        return complaintTypeDetail;
    }

    public void setComplaintTypeDetail(String complaintTypeDetail) {
        this.complaintTypeDetail = complaintTypeDetail;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStrCreatedTime() {
        return strCreatedTime;
    }

    public void setStrCreatedTime(String strCreatedTime) {
        this.strCreatedTime = strCreatedTime;
    }

    public String getStrLastModifyTime() {
        return strLastModifyTime;
    }

    public void setStrLastModifyTime(String strLastModifyTime) {
        this.strLastModifyTime = strLastModifyTime;
    }
}
