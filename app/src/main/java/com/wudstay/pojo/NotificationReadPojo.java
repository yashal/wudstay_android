package com.wudstay.pojo;

/**
 * Created by Yash ji on 9/26/2016.
 */

public class NotificationReadPojo extends BasePojo {

    private NotificationListPojo responseObject;

    public NotificationListPojo getResponseObject() {
        return responseObject;
    }

    public void setResponseObject(NotificationListPojo responseObject) {
        this.responseObject = responseObject;
    }
}
