package com.wudstay.pojo;

/**
 * Created by Yash ji on 9/28/2016.
 */

public class NewUpdatePojo extends BasePojo {

    private NewUpdateDataPojo responseObject;

    public NewUpdateDataPojo getResponseObject() {
        return responseObject;
    }

    public void setResponseObject(NewUpdateDataPojo responseObject) {
        this.responseObject = responseObject;
    }
}
