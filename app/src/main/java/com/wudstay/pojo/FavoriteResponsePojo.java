package com.wudstay.pojo;

/**
 * Created by arpit on 10/13/2016.
 */

public class FavoriteResponsePojo extends BasePojo{

    private FavoritePojo responseObject;

    public FavoritePojo getResponseObject() {
        return responseObject;
    }

    public void setResponseObject(FavoritePojo responseObject) {
        this.responseObject = responseObject;
    }
}
