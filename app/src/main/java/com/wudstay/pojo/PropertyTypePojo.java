package com.wudstay.pojo;

/**
 * Created by arpit on 3/24/2017.
 */

public class PropertyTypePojo {
    private String otherValue;
    private PropertyTypePojo linkedObject;
    private String value;
    private int key;

    public String getOtherValue() {
        return otherValue;
    }

    public void setOtherValue(String otherValue) {
        this.otherValue = otherValue;
    }

    public PropertyTypePojo getLinkedObject() {
        return linkedObject;
    }

    public void setLinkedObject(PropertyTypePojo linkedObject) {
        this.linkedObject = linkedObject;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }
}
