package com.wudstay.pojo;

import java.util.ArrayList;

/**
 * Created by Yash ji on 8/31/2016.
 */
public class PGCitiesPojo extends BasePojo {

    private ArrayList<PGCityDetailPojo> responseObject;

    public ArrayList<PGCityDetailPojo> getResponseObject() {
        return responseObject;
    }

    public void setResponseObject(ArrayList<PGCityDetailPojo> responseObject) {
        this.responseObject = responseObject;
    }
}
