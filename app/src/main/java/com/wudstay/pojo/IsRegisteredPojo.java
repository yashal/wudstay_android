package com.wudstay.pojo;

/**
 * Created by arpit on 1/20/2017.
 */

public class IsRegisteredPojo extends BasePojo{
    private IsRegisteredDataPojo responseObject;

    public IsRegisteredDataPojo getResponseObject() {
        return responseObject;
    }

    public void setResponseObject(IsRegisteredDataPojo responseObject) {
        this.responseObject = responseObject;
    }
}
