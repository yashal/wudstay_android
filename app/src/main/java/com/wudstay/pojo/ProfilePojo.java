package com.wudstay.pojo;

/**
 * Created by Administrator on 14-Sep-16.
 */
public class ProfilePojo extends BasePojo {

    private ProfileDataPojo responseObject;

    public ProfileDataPojo getResponseObject() {
        return responseObject;
    }

    public void setResponseObject(ProfileDataPojo responseObject) {
        this.responseObject = responseObject;
    }
}
