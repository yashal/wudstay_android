package com.wudstay.pojo;

import java.util.ArrayList;

/**
 * Created by Yash ji on 9/22/2016.
 */

public class getPgComplaintDataPojo {

    private int TotalComplaints;
    private ArrayList<PgComplaintDataPojo> ListOfComplaints;

    public int getTotalComplaints() {
        return TotalComplaints;
    }

    public void setTotalComplaints(int totalComplaints) {
        TotalComplaints = totalComplaints;
    }

    public ArrayList<PgComplaintDataPojo> getListOfComplaints() {
        return ListOfComplaints;
    }

    public void setListOfComplaints(ArrayList<PgComplaintDataPojo> listOfComplaints) {
        ListOfComplaints = listOfComplaints;
    }
}
