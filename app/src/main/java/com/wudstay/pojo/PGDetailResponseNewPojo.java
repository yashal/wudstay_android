package com.wudstay.pojo;

/**
 * Created by arpit on 3/28/2017.
 */

public class PGDetailResponseNewPojo extends BasePojo {
    private PGDetailNewPojo responseObject;

    public PGDetailNewPojo getResponseObject() {
        return responseObject;
    }

    public void setResponseObject(PGDetailNewPojo responseObject) {
        this.responseObject = responseObject;
    }
}
