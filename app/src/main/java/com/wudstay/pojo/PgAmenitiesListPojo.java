package com.wudstay.pojo;

import java.io.Serializable;

/**
 * Created by Yash ji on 9/1/2016.
 */
public class PgAmenitiesListPojo implements Serializable{
    private int linkedId;
    private String name;
    private int id;

    public int getLinkedId() {
        return linkedId;
    }

    public void setLinkedId(int linkedId) {
        this.linkedId = linkedId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
