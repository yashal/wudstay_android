package com.wudstay.pojo;

/**
 * Created by Administrator on 12-Sep-16.
 */
public class ScheduleVisitPojo extends BasePojo {
    private ScheduleVisitDetailPojo responseObject;

    public ScheduleVisitDetailPojo getResponseObject() {
        return responseObject;
    }

    public void setResponseObject(ScheduleVisitDetailPojo responseObject) {
        this.responseObject = responseObject;
    }
}
