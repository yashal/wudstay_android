package com.wudstay.pojo;

/**
 * Created by arpit on 1/20/2017.
 */

public class IsRegisteredDataPojo {
    private String mobileNumber;
    private String alreadyRegistered;
    private String customerName;
    private String customerEmail;
    private String userType;
    private String noOfPg;
    private String id;

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getAlreadyRegistered() {
        return alreadyRegistered;
    }

    public void setAlreadyRegistered(String alreadyRegistered) {
        this.alreadyRegistered = alreadyRegistered;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getNoOfPg() {
        return noOfPg;
    }

    public void setNoOfPg(String noOfPg) {
        this.noOfPg = noOfPg;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
