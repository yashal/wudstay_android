package com.wudstay.pojo;

import java.io.Serializable;

/**
 * Created by Yash ji on 9/1/2016.
 */
public class PgDescriptionPojo implements Serializable {
    private String description;
    private String descriptionTitle;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionTitle() {
        return descriptionTitle;
    }

    public void setDescriptionTitle(String descriptionTitle) {
        this.descriptionTitle = descriptionTitle;
    }
}
