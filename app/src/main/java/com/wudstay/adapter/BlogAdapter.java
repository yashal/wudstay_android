package com.wudstay.adapter;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wudstay.R;
import com.wudstay.activity.BlogActivity;
import com.wudstay.pojo.FeedsPojo;

import java.util.ArrayList;

/**
 * Add content to my trips list.
 */
public class BlogAdapter extends BaseAdapter {

    private ArrayList<FeedsPojo> arr;
    private Activity ctx;
    private BlogActivity obj;

    public BlogAdapter(ArrayList<FeedsPojo> arr, Activity ctx) {
        this.arr = arr;
        this.ctx = ctx;
        obj = (BlogActivity) ctx;
    }

    @Override
    public int getCount() {
        return arr.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflator = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

//		int mod = position%2;
        View row;
//		if(mod == 0)
        row = inflator.inflate(R.layout.blog_row_layout, parent, false);
//		else
//			row = inflator.inflate(R.layout.blog_row_layout_right, parent, false);

        TextView blog_title = (TextView) row.findViewById(R.id.blog_title);
        TextView blog_time = (TextView) row.findViewById(R.id.blog_time);
        ImageView imageView = (ImageView) row.findViewById(R.id.blog_image);
        if (arr != null) {
            String date[] = arr.get(position).getPubDate().split(" ");
            blog_title.setText(arr.get(position).getTitle());
            blog_time.setText(date[2] + " " + date[1] + ", " + date[3]);

            obj.setImageInLayout(obj, (int)obj.getResources().getDimension(R.dimen.blog_image_width), (int)obj.getResources().getDimension(R.dimen.blog_image_height), arr.get(position).getImageSrc(), imageView);

        }

        return row;
    }

}
