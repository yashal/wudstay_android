package com.wudstay.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wudstay.R;
import com.wudstay.activity.FilterActivityNew;
import com.wudstay.pojo.PropertyCategoryTypeListPojo;

import java.util.ArrayList;

/**
 * Created by Yash on 4/3/2017.
 */

public class CategoryListAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<PropertyCategoryTypeListPojo> categoryList;

    public CategoryListAdapter(Context c, ArrayList<PropertyCategoryTypeListPojo> categoryList) {
        mContext = c;
        this.categoryList = categoryList;
    }

    @Override
    public int getCount() {
        return (categoryList.size());
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final ViewHolder holder;
        if (convertView == null) {

            //  convertView = new View(mContext);
            convertView = inflater.inflate(R.layout.grid_category_list, null);
            holder = new ViewHolder();
            holder.category_img = (ImageView) convertView.findViewById(R.id.category_img);
            holder.category_layout = (LinearLayout) convertView.findViewById(R.id.category_layout);
            holder.category_name = (TextView) convertView.findViewById(R.id.category_name);


        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(categoryList.size() > 0) {
            holder.category_name.setText(categoryList.get(position).getCategoryTypeName());
            if (categoryList.get(position).getCategoryTypeName().equalsIgnoreCase("PG")) {
                if (categoryList.get(position).isChecked()) {
                    ((FilterActivityNew) mContext).setLightColor(holder.category_layout, holder.category_name, holder.category_img, R.mipmap.gray_pg);
                } else {
                    ((FilterActivityNew) mContext).setDarkColor(holder.category_layout, holder.category_name, holder.category_img, R.mipmap.black_pg);
                }

            } else if (categoryList.get(position).getCategoryTypeName().equalsIgnoreCase("Flat")) {
                if (categoryList.get(position).isChecked()) {
                    ((FilterActivityNew) mContext).setLightColor(holder.category_layout, holder.category_name, holder.category_img, R.mipmap.gray_flat);
                } else {
                    ((FilterActivityNew) mContext).setDarkColor(holder.category_layout, holder.category_name, holder.category_img, R.mipmap.black_flat);
                }
            }

            holder.category_layout.setTag(R.string.key, position);
            holder.category_layout.setTag(R.string.data, categoryList);
            holder.category_layout.setOnClickListener((FilterActivityNew) mContext);
        }
        convertView.setTag(holder);

        return convertView;
    }

    // convert view class
    class ViewHolder {
        private ImageView category_img;
        LinearLayout category_layout;
        TextView category_name;
    }
}
