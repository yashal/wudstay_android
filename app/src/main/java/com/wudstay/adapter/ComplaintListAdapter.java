package com.wudstay.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wudstay.R;
import com.wudstay.activity.ComplaintsActivity;
import com.wudstay.pojo.PgComplaintDataPojo;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 2/9/2016.
 */
public class ComplaintListAdapter extends RecyclerView.Adapter<ComplaintListAdapter.ViewHolder> {

    private ArrayList<PgComplaintDataPojo> arrayCityList;
    private int height, width;

    private ComplaintsActivity context;

    public ComplaintListAdapter(ComplaintsActivity context, ArrayList<PgComplaintDataPojo> arrayCityList) {
        this.context = context;
        this.arrayCityList = arrayCityList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.old_complaint_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final PgComplaintDataPojo data = arrayCityList.get(position);

        holder.itemView.setTag(R.string.key, data);

        if (data.getComplaintTypeDetail() != null) {
            holder.old_complaint_type.setText(data.getComplaintTypeDetail());
        }

        if (data.getStatusDetail() != null) {
            holder.old_complaint_status.setText(data.getStatusDetail());
        }

        if (data.getDescription() != null) {
            holder.old_complaint_description.setText(data.getDescription());
        }

//        long dateInMillis = Long.parseLong(data.getStrCreatedTime());
//        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
//        String dateString = formatter.format(new Date(dateInMillis));
        holder.old_complaint_date.setText(data.getStrCreatedTime());
        holder.itemView.setOnClickListener(context);

    }

    @Override
    public int getItemCount() {
        return arrayCityList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView old_complaint_type, old_complaint_description, old_complaint_date, old_complaint_status;
        private LinearLayout list_row;

        public ViewHolder(View itemView) {
            super(itemView);
            old_complaint_type = (TextView) itemView.findViewById(R.id.old_complaint_type);
            old_complaint_description = (TextView) itemView.findViewById(R.id.old_complaint_description);
            old_complaint_date = (TextView) itemView.findViewById(R.id.old_complaint_date);
            old_complaint_status = (TextView) itemView.findViewById(R.id.old_complaint_status);
        }
    }
}
