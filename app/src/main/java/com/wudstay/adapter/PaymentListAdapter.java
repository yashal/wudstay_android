package com.wudstay.adapter;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wudstay.R;
import com.wudstay.activity.PaymentActivity;
import com.wudstay.pojo.PaymentHistoryDataPojo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by quepplin1 on 2/9/2016.
 */
public class PaymentListAdapter extends RecyclerView.Adapter<PaymentListAdapter.ViewHolder> {

    private ArrayList<PaymentHistoryDataPojo> arrayCityList;
    private int height, width;

    private PaymentActivity context;

    public PaymentListAdapter(PaymentActivity context, ArrayList<PaymentHistoryDataPojo> arrayCityList) {
        this.context = context;
        this.arrayCityList = arrayCityList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.payments_row_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


//        if (position == 0) {
//            holder.paynowLL.setVisibility(View.VISIBLE);
//            holder.payment_row_month.setText("December");
//            holder.payment_row_paid.setVisibility(View.GONE);
//            holder.payment_row_payDate.setText("Due on 06/12/2016");
//            holder.payment_row_rs.setText(context.getResources().getString(R.string.Rs) + " " + "2000");
//            holder.paynowLL.setOnClickListener(context);
//        }
//        else if(position == 1){
//            holder.payment_row_paid.setText("Un-Paid");
//            holder.payment_row_month.setText("November");
//            holder.payment_row_paid.setTextColor(ContextCompat.getColor(context, R.color.due_data_color));
//            holder.payment_row_rs.setText(context.getResources().getString(R.string.Rs) + " " + 1000);
//            holder.payment_row_payDate.setText("Due on  06/11/2016");
//        } else {
//            holder.payment_row_paid.setText("Paid");
//            holder.payment_row_month.setText("October");
//            holder.payment_row_paid.setTextColor(ContextCompat.getColor(context, R.color.paid_date_color));
//            holder.payment_row_rs.setText(context.getResources().getString(R.string.Rs) + " " + 1000);
//            holder.payment_row_payDate.setText("Paid on 06/10/2016");
//        }

        final PaymentHistoryDataPojo data = arrayCityList.get(position);

        holder.paynowLL.setTag(R.string.key, data);

        if (data.getMonth() != null) {
            holder.payment_row_month.setText(data.getMonth());
        }

        if (position == 0) {
            if (data.getBalance() == 0) {
                holder.paynowLL.setVisibility(View.GONE);

                holder.payment_row_paid.setVisibility(View.VISIBLE);
                holder.payment_row_paid.setText("Paid");
                holder.payment_row_paid.setTextColor(ContextCompat.getColor(context, R.color.paid_date_color));
                holder.payment_row_rs.setText(context.getResources().getString(R.string.Rs) + " " + data.getPaidAmount());
                String convertedDate = convertDateFormat(data.getPaidDate());
                holder.payment_row_payDate.setText(convertedDate);
            } else {
                holder.paynowLL.setVisibility(View.VISIBLE);
                holder.payment_row_paid.setVisibility(View.GONE);
                String convertedDate = convertDateFormat(data.getDueDate());
                holder.payment_row_payDate.setText("Due on " + convertedDate);
                holder.payment_row_rs.setText(context.getResources().getString(R.string.Rs) + " " + data.getDueAmount());
                holder.paynowLL.setOnClickListener(context);
            }
        } else {
            holder.paynowLL.setVisibility(View.GONE);
            holder.payment_row_paid.setVisibility(View.VISIBLE);
            if (data.getBalance() == 0) {
                holder.payment_row_paid.setText("Paid");
                holder.payment_row_paid.setTextColor(ContextCompat.getColor(context, R.color.paid_date_color));
                holder.payment_row_rs.setText(context.getResources().getString(R.string.Rs) + " " + data.getPaidAmount());
                String convertedDate = convertDateFormat(data.getPaidDate());
                holder.payment_row_payDate.setText(convertedDate);
            } else {
                holder.payment_row_paid.setText("Un-Paid");
                holder.payment_row_paid.setTextColor(ContextCompat.getColor(context, R.color.due_data_color));
                holder.payment_row_rs.setText(context.getResources().getString(R.string.Rs) + " " + data.getDueAmount());
                String convertedDate = convertDateFormat(data.getDueDate());
                holder.payment_row_payDate.setText("Due on " + convertedDate);
            }
        }

    }

    @Override
    public int getItemCount() {
        return arrayCityList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView payment_row_month, payment_row_rs, payment_row_paid, payment_row_payDate;
        private LinearLayout paynowLL;

        public ViewHolder(View itemView) {
            super(itemView);
            payment_row_month = (TextView) itemView.findViewById(R.id.payment_row_month);
            payment_row_rs = (TextView) itemView.findViewById(R.id.payment_row_rs);
            payment_row_paid = (TextView) itemView.findViewById(R.id.payment_row_paid);
            payment_row_payDate = (TextView) itemView.findViewById(R.id.payment_row_payDate);
            paynowLL = (LinearLayout) itemView.findViewById(R.id.paynowLL);
        }
    }

    private String convertDateFormat(String date) {

        String return_value = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        try {
            Date d = dateFormat.parse(date);
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
            return_value = sdf.format(d.getTime());
        } catch (Exception e) {
            System.out.println("Excep" + e);
        }

        return return_value;
    }

}