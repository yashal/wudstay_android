package com.wudstay.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wudstay.R;
import com.wudstay.activity.FavouritesActivity;
import com.wudstay.pojo.MyFavPgListPojo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by yash on 11/16/2016.
 */

public class FavouriteListAdapter extends RecyclerView.Adapter<FavouriteListAdapter.ViewHolder> {

    private ArrayList<MyFavPgListPojo> arrayCityList;
    private int height, width;

    private FavouritesActivity context;

    public FavouriteListAdapter(FavouritesActivity context, ArrayList<MyFavPgListPojo> arrayCityList) {
        this.context = context;
        this.arrayCityList = arrayCityList;
    }

    @Override
    public FavouriteListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fav_list_row, parent, false);
        FavouriteListAdapter.ViewHolder viewHolder = new FavouriteListAdapter.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(FavouriteListAdapter.ViewHolder holder, int position) {
        final MyFavPgListPojo data = arrayCityList.get(position);


        if (data.getPgName() != null) {
            holder.fav_pg_list_Name.setText(data.getPgName());
        }

        if (data.getPgAddress() != null) {
            holder.fav_pg_list_Address.setText(data.getPgAddress());
        }
        if (data.getPgAddress() != null) {
            holder.fav_pg_list_Address.setText(data.getPgAddress());
        }
        if (data.getPgDetail().getRoomImageMain() != null) {
            context.setImageInLayout(context, (int) context.getResources().getDimension(R.dimen.pg_image_width), (int) context.getResources().getDimension(R.dimen.pg_image_height), data.getPgDetail().getRoomImageMain(), holder.fav_pg_list_Image);
        }
    }

    @Override
    public int getItemCount() {
        return arrayCityList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView fav_pg_list_Name, fav_pg_list_Address;
        private ImageView fav_pg_list_Image;

        public ViewHolder(View itemView) {
            super(itemView);
            fav_pg_list_Name = (TextView) itemView.findViewById(R.id.fav_pg_list_Name);
            fav_pg_list_Address = (TextView) itemView.findViewById(R.id.fav_pg_list_Address);
            fav_pg_list_Image = (ImageView) itemView.findViewById(R.id.fav_pg_list_Image);
        }
    }

    private String convertDateFormat(String date) {

        String return_value = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        try {
            Date d = dateFormat.parse(date);
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
            return_value = sdf.format(d.getTime());
        } catch (Exception e) {
            System.out.println("Excep" + e);
        }

        return return_value;
    }

}