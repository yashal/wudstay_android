package com.wudstay.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.wudstay.fragments.NewComplaintFragment;
import com.wudstay.fragments.OldComplaintFragment;

/**
 * Created by Yash ji on 4/22/2016.
 */
public class ComplaintAdapter extends FragmentPagerAdapter {

    public ComplaintAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {

        switch (index) {
            case 0:
                return new NewComplaintFragment();
            case 1:
                return new OldComplaintFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 2;
    }
}

