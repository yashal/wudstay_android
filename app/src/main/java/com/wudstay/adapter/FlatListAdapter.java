package com.wudstay.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wudstay.R;
import com.wudstay.activity.FilterActivityNew;
import com.wudstay.pojo.PropertyCategoryListPojo;
import com.wudstay.pojo.PropertyCategoryTypeListPojo;

import java.util.ArrayList;

/**
 * Created by Yash on 4/3/2017.
 */

public class FlatListAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<PropertyCategoryListPojo> categoryList;

    public FlatListAdapter(Context c, ArrayList<PropertyCategoryListPojo> categoryList) {
        mContext = c;
        this.categoryList = categoryList;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return (categoryList.size());
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        //View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final ViewHolder holder;
        if (convertView == null) {

            //  convertView = new View(mContext);
            convertView = inflater.inflate(R.layout.grid_flat_list, null);
            holder = new ViewHolder();
            holder.flat_layout = (LinearLayout) convertView.findViewById(R.id.flat_layout);
            holder.flat_name = (TextView) convertView.findViewById(R.id.flat_name);


        } else {
            holder = (ViewHolder) convertView.getTag();

        }

        holder.flat_name.setText(categoryList.get(position).getCategoryName());
        if (categoryList.get(position).isChecked())
        {
            ((FilterActivityNew)mContext).setLightColorLayout(holder.flat_layout, holder.flat_name);
        }
        else
        {
            ((FilterActivityNew)mContext).setDarkColorLayout(holder.flat_layout, holder.flat_name);
        }

        holder.flat_layout.setTag(R.string.key, position);
        holder.flat_layout.setTag(R.string.data, categoryList.get(position));
        holder.flat_layout.setOnClickListener((FilterActivityNew) mContext);

        convertView.setTag(holder);

        return convertView;
    }

    // convert view class
    class ViewHolder {
        LinearLayout flat_layout;
        TextView flat_name;
    }
}
