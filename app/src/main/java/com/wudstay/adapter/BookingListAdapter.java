package com.wudstay.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wudstay.R;
import com.wudstay.activity.FilterActivityNew;
import com.wudstay.pojo.RoomOccupancyListPojo;

import java.util.ArrayList;

/**
 * Created by Yash on 4/3/2017.
 */

public class BookingListAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<RoomOccupancyListPojo> bookingList;

    public BookingListAdapter(Context c, ArrayList<RoomOccupancyListPojo> categoryList) {
        mContext = c;
        this.bookingList = categoryList;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return (bookingList.size());
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        //View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final ViewHolder holder;
        if (convertView == null) {

            //  convertView = new View(mContext);
            convertView = inflater.inflate(R.layout.grid_booking_list, null);
            holder = new ViewHolder();
            holder.booking_img = (ImageView) convertView.findViewById(R.id.booking_img);
            holder.booking_layout = (LinearLayout) convertView.findViewById(R.id.booking_layout);
            holder.booking_name = (TextView) convertView.findViewById(R.id.booking_name);


        } else {
            holder = (ViewHolder) convertView.getTag();

        }

        if (bookingList.get(position).isActive())
        {
            holder.booking_layout.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.booking_layout.setVisibility(View.GONE);
        }
        holder.booking_name.setText(bookingList.get(position).getOccupancyName());
        if (bookingList.get(position).isMflag())
        {
            ((FilterActivityNew)mContext).setLightColor(holder.booking_layout, holder.booking_name, holder.booking_img, R.mipmap.gray_onebed);
        }
        else
        {
            ((FilterActivityNew)mContext).setDarkColor(holder.booking_layout, holder.booking_name, holder.booking_img, R.mipmap.black_onebed);
        }
        /*if (bookingList.get(position).getOccupancyName().equalsIgnoreCase("Single Sharing"))
        {
            if (bookingList.get(position).isMflag())
            {
                ((FilterActivityNew)mContext).setLightColor(holder.booking_layout, holder.booking_name, holder.booking_img, R.mipmap.gray_onebed);
            }
            else
            {
                ((FilterActivityNew)mContext).setDarkColor(holder.booking_layout, holder.booking_name, holder.booking_img, R.mipmap.black_onebed);
            }

        }
        else if (bookingList.get(position).getOccupancyName().equalsIgnoreCase("Double Sharing"))
        {
            if (bookingList.get(position).isMflag())
            {
                ((FilterActivityNew)mContext).setLightColor(holder.booking_layout, holder.booking_name, holder.booking_img, R.mipmap.gray_twobeds);
            }
            else
            {
                ((FilterActivityNew)mContext).setDarkColor(holder.booking_layout, holder.booking_name, holder.booking_img, R.mipmap.black_twobeds);
            }
        }
        else if (bookingList.get(position).getOccupancyName().equalsIgnoreCase("Triple Sharing"))
        {
            if (bookingList.get(position).isMflag())
            {
                ((FilterActivityNew)mContext).setLightColor(holder.booking_layout, holder.booking_name, holder.booking_img, R.mipmap.gray_threebeds);
            }
            else
            {
                ((FilterActivityNew)mContext).setDarkColor(holder.booking_layout, holder.booking_name, holder.booking_img, R.mipmap.black_threebeds);
            }
        }
        else if (bookingList.get(position).getOccupancyName().equalsIgnoreCase("Quad Sharing"))
        {
            if (bookingList.get(position).isMflag())
            {
                ((FilterActivityNew)mContext).setLightColor(holder.booking_layout, holder.booking_name, holder.booking_img, R.mipmap.gray_onebed);
            }
            else
            {
                ((FilterActivityNew)mContext).setDarkColor(holder.booking_layout, holder.booking_name, holder.booking_img, R.mipmap.black_onebed);
            }
        }
        else if (bookingList.get(position).getOccupancyName().equalsIgnoreCase("Penta Sharing"))
        {
            if (bookingList.get(position).isMflag())
            {
                ((FilterActivityNew)mContext).setLightColor(holder.booking_layout, holder.booking_name, holder.booking_img, R.mipmap.gray_onebed);
            }
            else
            {
                ((FilterActivityNew)mContext).setDarkColor(holder.booking_layout, holder.booking_name, holder.booking_img, R.mipmap.black_onebed);
            }
        }
        else if (bookingList.get(position).getOccupancyName().equalsIgnoreCase("Hexa Sharing"))
        {
            if (bookingList.get(position).isMflag())
            {
                ((FilterActivityNew)mContext).setLightColor(holder.booking_layout, holder.booking_name, holder.booking_img, R.mipmap.gray_onebed);
            }
            else
            {
                ((FilterActivityNew)mContext).setDarkColor(holder.booking_layout, holder.booking_name, holder.booking_img, R.mipmap.black_onebed);
            }
        }
        else if (bookingList.get(position).getOccupancyName().equalsIgnoreCase("Septa Sharing"))
        {
            if (bookingList.get(position).isMflag())
            {
                ((FilterActivityNew)mContext).setLightColor(holder.booking_layout, holder.booking_name, holder.booking_img, R.mipmap.gray_onebed);
            }
            else
            {
                ((FilterActivityNew)mContext).setDarkColor(holder.booking_layout, holder.booking_name, holder.booking_img, R.mipmap.black_onebed);
            }
        }*/

        holder.booking_layout.setTag(R.string.key, position);
        holder.booking_layout.setTag(R.string.data, bookingList.get(position));
        holder.booking_layout.setOnClickListener((FilterActivityNew) mContext);

        convertView.setTag(holder);

        return convertView;
    }

    // convert view class
    class ViewHolder {
        private ImageView booking_img;
        LinearLayout booking_layout;
        TextView booking_name;
    }
}
