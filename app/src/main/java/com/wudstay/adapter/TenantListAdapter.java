package com.wudstay.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wudstay.R;
import com.wudstay.activity.FilterActivityNew;
import com.wudstay.pojo.PropertyCategoryTypeListPojo;
import com.wudstay.pojo.PropertyTypesListPojo;

import java.util.ArrayList;

/**
 * Created by Yash on 4/3/2017.
 */

public class TenantListAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<PropertyTypesListPojo> tenantList;

    public TenantListAdapter(Context c, ArrayList<PropertyTypesListPojo> tenantList) {
        mContext = c;
        this.tenantList = tenantList;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return (tenantList.size());
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        //View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final ViewHolder holder;
        if (convertView == null) {

            //  convertView = new View(mContext);
            convertView = inflater.inflate(R.layout.grid_tenant_list, null);
            holder = new ViewHolder();
            holder.tenant_img = (ImageView) convertView.findViewById(R.id.tenant_img);
            holder.tenant_layout = (LinearLayout) convertView.findViewById(R.id.tenant_layout);
            holder.tenant_name = (TextView) convertView.findViewById(R.id.tenant_name);


        } else {
            holder = (ViewHolder) convertView.getTag();

        }
        holder.tenant_name.setText(tenantList.get(position).getPgType());
        if (tenantList.get(position).getPgType().equalsIgnoreCase("Boys"))
        {
            if (tenantList.get(position).isChecked())
            {
                ((FilterActivityNew)mContext).setLightColor(holder.tenant_layout, holder.tenant_name, holder.tenant_img, R.mipmap.male_grey);
            }
            else
            {
                ((FilterActivityNew)mContext).setDarkColor(holder.tenant_layout, holder.tenant_name, holder.tenant_img, R.mipmap.male);
            }

        }
        else if (tenantList.get(position).getPgType().equalsIgnoreCase("Girls"))
        {
            if (tenantList.get(position).isChecked())
            {
                ((FilterActivityNew)mContext).setLightColor(holder.tenant_layout, holder.tenant_name, holder.tenant_img, R.mipmap.female_grey);
            }
            else
            {
                ((FilterActivityNew)mContext).setDarkColor(holder.tenant_layout, holder.tenant_name, holder.tenant_img, R.mipmap.female);
            }
        }
        else if (tenantList.get(position).getPgType().equalsIgnoreCase("Common"))
        {
            if (tenantList.get(position).isChecked())
            {
                ((FilterActivityNew)mContext).setLightColor(holder.tenant_layout, holder.tenant_name, holder.tenant_img, R.mipmap.gray_common);
            }
            else
            {
                ((FilterActivityNew)mContext).setDarkColor(holder.tenant_layout, holder.tenant_name, holder.tenant_img, R.mipmap.black_common);
            }
        }

        holder.tenant_layout.setTag(R.string.key, position);
        holder.tenant_layout.setTag(R.string.data, tenantList);
        holder.tenant_layout.setOnClickListener((FilterActivityNew) mContext);

        convertView.setTag(holder);

        return convertView;
    }

    // convert view class
    class ViewHolder {
        private ImageView tenant_img;
        LinearLayout tenant_layout;
        TextView tenant_name;
    }
}
