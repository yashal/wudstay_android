package com.wudstay.adapter;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wudstay.R;
import com.wudstay.activity.NotificationsActivity;
import com.wudstay.pojo.NotificationListPojo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by quepplin1 on 2/9/2016.
 */
public class NotificationListAdapter extends RecyclerView.Adapter<NotificationListAdapter.ViewHolder> {

    private ArrayList<NotificationListPojo> arrayCityList;
    private int height, width;

    private NotificationsActivity context;

    public NotificationListAdapter(NotificationsActivity context, ArrayList<NotificationListPojo> arrayCityList) {
        this.context = context;
        this.arrayCityList = arrayCityList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_row_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final NotificationListPojo data = arrayCityList.get(position);

        holder.itemView.setTag(R.string.key, data);

        if (data.getComplaintTypeDetail() != null) {
            holder.notification_title_text.setText(data.getComplaintTypeDetail());
        }
        if (data.getDescription() != null) {
            holder.notification_message_text.setText(data.getDescription());
        }

//        long dateInMillis = data.getLastModifyTime();
//        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
//        String dateString = formatter.format(new Date(dateInMillis));
        holder.notification_date_text.setText(data.getStrLastModifyTime());

        if (data.getStatus() == 1)
        {
            // read notifications
            holder.notification_title_text.setTypeface(null, Typeface.NORMAL);
            holder.notification_message_text.setTypeface(null, Typeface.NORMAL);
            holder.notification_date_text.setTypeface(null, Typeface.NORMAL);
        }
        else if (data.getStatus() == 0)
        {
            // unread notifications
            holder.notification_title_text.setTypeface(null, Typeface.BOLD);
            holder.notification_message_text.setTypeface(null, Typeface.BOLD);
            holder.notification_date_text.setTypeface(null, Typeface.BOLD);
        }

        holder.itemView.setOnClickListener((NotificationsActivity) context);

    }

    @Override
    public int getItemCount() {
        return arrayCityList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView notification_title_text, notification_message_text, notification_date_text;

        public ViewHolder(View itemView) {
            super(itemView);
            notification_message_text = (TextView) itemView.findViewById(R.id.notification_message_text);
            notification_title_text = (TextView) itemView.findViewById(R.id.notification_title_text);
            notification_date_text = (TextView) itemView.findViewById(R.id.notification_date_text);
        }
    }


}