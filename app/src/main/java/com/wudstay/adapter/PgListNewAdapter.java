package com.wudstay.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wudstay.R;
import com.wudstay.activity.PGListNewActivity;
import com.wudstay.pojo.PgPropertyListPojo;
import com.wudstay.util.WudStayConstants;

import java.util.ArrayList;

/**
 * Created by arpit on 3/28/2017.
 */

public class PgListNewAdapter extends RecyclerView.Adapter<PgListNewAdapter.ViewHolder> {

    private ArrayList<PgPropertyListPojo> arrayCityList;
    private int lastPosition = -1;
    private PGListNewActivity context;

    public PgListNewAdapter(PGListNewActivity context, ArrayList<PgPropertyListPojo> arrayCityList) {
        this.context = context;
        this.arrayCityList = arrayCityList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pg_list_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final PgPropertyListPojo data = arrayCityList.get(position);

        holder.itemView.setTag(R.string.key, data);

        if (data.getMainImage() != null) {
            String url = WudStayConstants.BASE_URL_IMAGE+"hotel_images/"+data.getId()+"/rooms_pg/"+data.getMainImage();
            context.setImageInLayout(context, (int) context.getResources().getDimension(R.dimen.pg_image_width), (int) context.getResources().getDimension(R.dimen.pg_image_height), url, holder.pgImage);
        }
        if (data.getDisplayName() != null) {
            holder.pgName.setText(data.getDisplayName());
        }
        if (data.getDisplayAddress() != null) {
            holder.pgAddress.setText(data.getDisplayAddress());
        }
        if (data.getPrice() > 0) {
            holder.pgAmount.setText(context.getResources().getString(R.string.Rs) + " " + data.getPrice() + " Onwards");
        }
        if (data.getNoOfBeds() > 0) {
            holder.pg_soldout.setVisibility(View.GONE);
        }else{
            holder.pg_soldout.setVisibility(View.VISIBLE);
            holder.pg_soldout.setText("SOLD OUT");}

        holder.pg_type.setText(data.getPropertyType().getValue()+" "+data.getPropertyCategoryType().getValue());

        holder.itemView.setOnClickListener(context);

        if (position < arrayCityList.size())
            setAnimation(holder.itemView, position);

    }

    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context,
                    (position > lastPosition) ? R.anim.up_from_bottom
                            : R.anim.down_from_top);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public void onViewDetachedFromWindow(ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.itemView.clearAnimation();
    }

    @Override
    public int getItemCount() {
        return arrayCityList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView pgName, pgAddress, pgAmount, pg_type,pg_soldout;
        private ImageView pgImage;
        private LinearLayout pgListRow;

        public ViewHolder(View itemView) {
            super(itemView);
            pgName = (TextView) itemView.findViewById(R.id.pg_list_Name);
            pgAddress = (TextView) itemView.findViewById(R.id.pg_list_Address);
            pgAmount = (TextView) itemView.findViewById(R.id.pg_list_amount);
            pg_type = (TextView) itemView.findViewById(R.id.pg_type);
            pg_soldout = (TextView) itemView.findViewById(R.id.sold_out);
            pgImage = (ImageView) itemView.findViewById(R.id.pg_list_Image);
            pgListRow = (LinearLayout) itemView.findViewById(R.id.list_row);
        }
    }
}
