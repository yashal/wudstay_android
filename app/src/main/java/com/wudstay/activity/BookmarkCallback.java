package com.wudstay.activity;

import com.wudstay.pojo.FavoriteResponsePojo;

/**
 * Created by arpit on 10/13/2016.
 */

public interface BookmarkCallback {
    void onSuccess(FavoriteResponsePojo obj);
    void onError();
}
