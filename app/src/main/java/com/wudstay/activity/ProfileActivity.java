package com.wudstay.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wudstay.R;
import com.wudstay.model.ApiClient;
import com.wudstay.model.ApiInterface;
import com.wudstay.model.FileUploadSer;
import com.wudstay.model.ServiceGenerator;
import com.wudstay.pojo.ImageUploadPojo;
import com.wudstay.pojo.MobileNoVerificationPojo;
import com.wudstay.pojo.ProfilePojo;
import com.wudstay.util.ConnectionDetector;
import com.wudstay.util.ImageLoadingUtils;
import com.wudstay.util.WudStayConstants;
import com.wudstay.util.WudstayDialogs;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class ProfileActivity extends BaseActivity {

    private Activity ctx = this;
    private ConnectionDetector cd;
    private WudstayDialogs dialog;
    private ImageView profile_imgUser;
    private TextView profile_address_change, profile_emergency_contact_change, profile_contact_change, editDoneText;
    private LinearLayout doneLL;
    private EditText profile_userName, profile_addressLine1, profile_addressLine2, profile_emergency_contactName,
            profile_emergency_contactNo, profile_userEmail, profile_userMobile;
    private ImageView back;

    private final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 456;
    private final int SELECT_PHOTO = 457;
    private String mCurrentPhotoPath;
    private String photoPath = "";

    private static final String CAMERA_DIR = "/dcim/";

    // for external permission
    private final static int READ_EXTERNAL_STORAGE = 105;
    private SharedPreferences sharedPreferences;
    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected;

    // component for profile update
    private String mobileNumber = "";
    private String firstName = "";
    private String lastName = "";
    private String email = "";
    private String homeAddressLine1 = "";
    private String homeAddressLine2 = "";
    private String cityName = "";
    private String stateId = "";
    private String pin = "";
    private String phone = "";
    private String emergencyContactPersonFirstName = "";
    private String emergencyContactPersonLastName = "";
    private String emergencyContactPersonMobile = "";
    private String emergencyContactPersonEmail = "";
    private String emergencyContactPersonRelation = "";
    private String emergencyContactPersonFullAddress = "";
    private String emergencyContactPersonPhone = "";

    private Dialog otpDialog;
    private CounterClass timer;
    private EditText edOTP, edNumber;
    private WudstayDialogs progressDialog;
    private boolean mFlag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        getURL();

        profile_imgUser = (ImageView) findViewById(R.id.profile_imgUser);
        profile_userName = (EditText) findViewById(R.id.profile_userName);
        profile_address_change = (TextView) findViewById(R.id.profile_address_change);
        profile_contact_change = (TextView) findViewById(R.id.profile_contact_change);
        profile_addressLine1 = (EditText) findViewById(R.id.profile_addressLine1);
        profile_addressLine2 = (EditText) findViewById(R.id.profile_addressLine2);
        profile_userEmail = (EditText) findViewById(R.id.profile_userEmail);
        profile_userMobile = (EditText) findViewById(R.id.profile_userMobile);
        profile_emergency_contact_change = (TextView) findViewById(R.id.profile_emergency_contact_change);
        profile_emergency_contactName = (EditText) findViewById(R.id.profile_emergency_contactName);
        profile_emergency_contactNo = (EditText) findViewById(R.id.profile_emergency_contactNo);
        back = (ImageView) findViewById(R.id.back);
        doneLL = (LinearLayout) findViewById(R.id.doneLL);
        editDoneText = (TextView) findViewById(R.id.editDoneText);

        cd = new ConnectionDetector(getApplicationContext());
        dialog = new WudstayDialogs(ctx);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        mobileNumber = getFromPrefs(WudStayConstants.MOBILE);

        getHint();

        profile_imgUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mFlag) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        addPermissionDialogMarshMallow();
                    } else {
                        showChooserDialog();
                    }
                }
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        getUserProfile();

       /* profile_userName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doneLL.setVisibility(View.VISIBLE);
                profile_userName.setFocusable(true);
                profile_userName.setFocusableInTouchMode(true);
                change_field = "name";
                openSoftKeyBoard(profile_userName);
            }
        });

        profile_contact_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doneLL.setVisibility(View.VISIBLE);
                profile_userEmail.setFocusable(true);
                profile_userEmail.setFocusableInTouchMode(true);
                profile_userMobile.setFocusable(true);
                profile_userMobile.setFocusableInTouchMode(true);
                change_field = "contact";
                openSoftKeyBoard(profile_userEmail);
            }
        });

        profile_address_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doneLL.setVisibility(View.VISIBLE);
                profile_addressLine1.setFocusable(true);
                profile_addressLine1.setFocusableInTouchMode(true);
                profile_addressLine2.setFocusable(true);
                profile_addressLine2.setFocusableInTouchMode(true);
                change_field = "address";
                openSoftKeyBoard(profile_addressLine1);
            }
        });

        profile_emergency_contact_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doneLL.setVisibility(View.VISIBLE);
                profile_emergency_contactName.setFocusable(true);
                profile_emergency_contactName.setFocusableInTouchMode(true);
                profile_emergency_contactNo.setFocusable(true);
                profile_emergency_contactNo.setFocusableInTouchMode(true);
                change_field = "emergency";
                openSoftKeyBoard(profile_emergency_contactName);
            }
        });*/

        doneLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mFlag) {
                    mFlag = true;
                    editDoneText.setText(getResources().getString(R.string.done));
                    profile_userName.setFocusable(true);
                    profile_userName.setFocusableInTouchMode(true);

//                    profile_userEmail.setFocusable(true);
//                    profile_userEmail.setFocusableInTouchMode(true);

                    profile_addressLine1.setFocusable(true);
                    profile_addressLine1.setFocusableInTouchMode(true);

                    profile_addressLine2.setFocusable(true);
                    profile_addressLine2.setFocusableInTouchMode(true);

                    profile_emergency_contactName.setFocusable(true);
                    profile_emergency_contactName.setFocusableInTouchMode(true);

                    profile_emergency_contactNo.setFocusable(true);
                    profile_emergency_contactNo.setFocusableInTouchMode(true);

                    openSoftKeyBoard(profile_userName);
                } else {
                    firstName = profile_userName.getText().toString().trim();
                    homeAddressLine1 = profile_addressLine1.getText().toString().trim();
                    homeAddressLine2 = profile_addressLine2.getText().toString().trim();
                    emergencyContactPersonFirstName = profile_emergency_contactName.getText().toString().trim();
                    emergencyContactPersonMobile = profile_emergency_contactNo.getText().toString().trim();
                    email = profile_userEmail.getText().toString().trim();
                    mobileNumber = profile_userMobile.getText().toString().trim();

                    if (firstName.isEmpty()) {
                        Toast.makeText(ctx, "Enter Name", Toast.LENGTH_SHORT).show();
                    } else if (email.isEmpty()) {
                        Toast.makeText(ctx, "Enter Email", Toast.LENGTH_SHORT).show();
                    } else if (!email.matches("[0-9]+") && !isValidEmail(email)) {
                        Toast.makeText(ctx, "Please enter valid Email", Toast.LENGTH_SHORT).show();
                    } else if (mobileNumber.isEmpty()) {
                        Toast.makeText(ctx, "Enter Mobile", Toast.LENGTH_SHORT).show();
                    } else if (mobileNumber.length() < 10) {
                        Toast.makeText(ctx, "Please enter valid Mobile", Toast.LENGTH_SHORT).show();
                    } else if ((emergencyContactPersonMobile.length() > 0) && (emergencyContactPersonMobile.length() < 10)) {
                        Toast.makeText(ctx, "Please enter valid Emergency Contact Number", Toast.LENGTH_SHORT).show();
                    } else {
//                        if (mobileNumber.equals(getFromPrefs(WudStayConstants.MOBILE))) {
                            updateProfile();
//                        } else {
//                            getMobileVerificationCode(profile_userName.getText().toString(), profile_userEmail.getText().toString(), mobileNumber);
//                        }

                    }
                }
            }
        });
    }

    private void getHint() {
        if (profile_userName.getText().toString().isEmpty()) {
            profile_userName.setHint("Name");
            profile_userName.setHintTextColor(ContextCompat.getColor(ctx, R.color.hint_color));
        }
        if (profile_addressLine1.getText().toString().isEmpty()) {
            profile_addressLine1.setHint("Enter Address Line1");
            profile_addressLine1.setHintTextColor(getResources().getColor(R.color.hint_color));
        }
        if (profile_addressLine2.getText().toString().isEmpty()) {
            profile_addressLine2.setHint("Enter Address Line2");
            profile_addressLine2.setHintTextColor(getResources().getColor(R.color.hint_color));
        }
        if (profile_userEmail.getText().toString().isEmpty()) {
            profile_userEmail.setHint("Enter Email");
            profile_userEmail.setHintTextColor(getResources().getColor(R.color.hint_color));
        }
        if (profile_userMobile.getText().toString().isEmpty()) {
            profile_userMobile.setHint("Enter Mobile");
            profile_userMobile.setHintTextColor(getResources().getColor(R.color.hint_color));
        }
        if (profile_emergency_contactName.getText().toString().isEmpty()) {
            profile_emergency_contactName.setHint("Enter Emergency Contact Name");
            profile_emergency_contactName.setHintTextColor(getResources().getColor(R.color.hint_color));
        }
        if (profile_emergency_contactNo.getText().toString().isEmpty()) {
            profile_emergency_contactNo.setHint("Enter Emergency Contact No");
            profile_emergency_contactNo.setHintTextColor(getResources().getColor(R.color.hint_color));
        }

    }

    private void updateProfile() {
        if (cd.isConnectingToInternet()) {
            progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<ProfilePojo> call = apiService.updateUserProfile(mobileNumber, firstName, lastName, email, homeAddressLine1, homeAddressLine2, cityName,
                    stateId, pin, phone, emergencyContactPersonFirstName, emergencyContactPersonLastName, emergencyContactPersonMobile, emergencyContactPersonEmail,
                    emergencyContactPersonRelation, emergencyContactPersonFullAddress, emergencyContactPersonPhone);
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<ProfilePojo>() {
                @Override
                public void onResponse(Call<ProfilePojo> call, Response<ProfilePojo> response) {
                    if (response.body().getResponseCode() == 1) {
                        saveIntoPrefs(WudStayConstants.NAME, firstName);
                        saveIntoPrefs(WudStayConstants.EMAIL, email);
                        saveIntoPrefs(WudStayConstants.MOBILE, mobileNumber);
                        saveIntoPrefs(WudStayConstants.IMAGE, response.body().getResponseObject().getCandidateDp());
                        if (photoPath != null && !photoPath.isEmpty()) {
                            uploadProfileDp();
                        } else {
                            progressDialog.getlDialog().dismiss();
                            startActivity(new Intent(ctx, ProfileActivity.class));
                            finish();
                            Toast.makeText(ctx, "Profile Updated Successfully", Toast.LENGTH_SHORT).show();
                        }

                    }
                    else
                    {
                        progressDialog.getlDialog().dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ProfilePojo> call, Throwable t) {
                    // Log error here since request failed
                    System.out.println("retrofit hh failure " + t.getMessage());
                    progressDialog.getlDialog().dismiss();
                }
            });
        } else {
            dialog.displayCommonDialog(WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }

    private void getUserProfile() {
        if (cd.isConnectingToInternet()) {
            final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<ProfilePojo> call = apiService.getUserProfile(getFromPrefs(WudStayConstants.MOBILE));
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<ProfilePojo>() {
                @Override
                public void onResponse(Call<ProfilePojo> call, Response<ProfilePojo> response) {
//                    System.out.println("retrofit URL " + response.body());
                    if (response.body().getResponseCode() == 1) {
                        saveIntoPrefs(WudStayConstants.NAME, response.body().getResponseObject().getFirstName());
                        saveIntoPrefs(WudStayConstants.EMAIL, response.body().getResponseObject().getEmail());
                        saveIntoPrefs(WudStayConstants.MOBILE, response.body().getResponseObject().getMobile());
                        saveIntoPrefs(WudStayConstants.IMAGE, response.body().getResponseObject().getCandidateDp());
                        if (response.body().getResponseObject().getFirstName() != null) {
                            profile_userName.setText(response.body().getResponseObject().getFirstName());
                        }
                        if (response.body().getResponseObject().getHomeAddressLine1() != null) {
                            profile_addressLine1.setText(response.body().getResponseObject().getHomeAddressLine1());
                        }
                        if (response.body().getResponseObject().getHomeAddressLine2() != null) {
                            profile_addressLine2.setText(response.body().getResponseObject().getHomeAddressLine2());
                        }
                        if (response.body().getResponseObject().getEmergencyContactPersonFirstName() != null) {
                            profile_emergency_contactName.setText(response.body().getResponseObject().getEmergencyContactPersonFirstName());
                        }
                        if (response.body().getResponseObject().getEmergencyContactPersonMobile() != null) {
                            profile_emergency_contactNo.setText(response.body().getResponseObject().getEmergencyContactPersonMobile());
                        }
                        if (response.body().getResponseObject().getEmail() != null) {
                            profile_userEmail.setText(response.body().getResponseObject().getEmail());
                        }
                        if (response.body().getResponseObject().getMobile() != null) {
                            profile_userMobile.setText(response.body().getResponseObject().getMobile());
                        }
                        if (response.body().getResponseObject().getCandidateDp() != null) {
                            setProfileImageInLayout(ctx, (int) getResources().getDimension(R.dimen.profile_image), (int) getResources().getDimension(R.dimen.profile_image),
                                    response.body().getResponseObject().getCandidateDp(), profile_imgUser);
                        }
                    }
                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<ProfilePojo> call, Throwable t) {
                    // Log error here since request failed
                    System.out.println("retrofit hh failure " + t.getMessage());
                    progressDialog.getlDialog().dismiss();
                }
            });
        } else {
            dialog.displayCommonDialog(WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }

    private void addPermissionDialogMarshMallow() {
        ArrayList<String> permissions = new ArrayList<>();
        int resultCode = 0;

        permissions.add(WRITE_EXTERNAL_STORAGE);
        resultCode = READ_EXTERNAL_STORAGE;


        //filter out the permissions we have already accepted
        permissionsToRequest = findUnAskedPermissions(permissions);
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.
        permissionsRejected = findRejectedPermissions(permissions);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (permissionsToRequest.size() > 0) {//we need to ask for permissions
                //but have we already asked for them?
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                //mark all these as asked..
                for (String perm : permissionsToRequest) {
                    markAsAsked(perm, sharedPreferences);
                }
            } else {
                //show the success banner
                if (permissionsRejected.size() < permissions.size()) {
                    //this means we can show success because some were already accepted.
                    //permissionSuccess.setVisibility(View.VISIBLE);
                    showChooserDialog();
                }

                if (permissionsRejected.size() > 0) {
                    //we have none to request but some previously rejected..tell the user.
                    //It may be better to show a dialog here in a prod application

                    requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                    //mark all these as asked..
                    for (String perm : permissionsToRequest) {
                        markAsAsked(perm, sharedPreferences);
                    }
                }
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case READ_EXTERNAL_STORAGE:
                if (hasPermission(WRITE_EXTERNAL_STORAGE)) {
                    //        permissionSuccess.setVisibility(View.VISIBLE);
                    showChooserDialog();
                } else {
                    permissionsRejected.add(WRITE_EXTERNAL_STORAGE);
                    clearMarkAsAsked(WRITE_EXTERNAL_STORAGE);
                    String message = "permission for access external storage was rejected. Please allow to run the app.";
                    makePostRequestSnack(message, permissionsRejected.size());
                }
                break;
        }

    }

    public void showChooserDialog() {

        mFlag = true;
        editDoneText.setText(getResources().getString(R.string.done));
        doneLL.setVisibility(View.VISIBLE);
        final Dialog dialogr = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogr.setContentView(R.layout.custom_imageupload_dialog);
        dialogr.setCancelable(true);

        // set the custom dialog components - text, image and button
        ImageView gallery = (ImageView) dialogr.findViewById(R.id.galleryimage);
        gallery.setImageResource(R.mipmap.gallery_icon);
        ImageView camera = (ImageView) dialogr.findViewById(R.id.cameraimage);
        camera.setImageResource(R.mipmap.camera_icon);

        gallery.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, SELECT_PHOTO);
                dialogr.dismiss();
                dialogr.hide();
            }
        });

        camera.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // give the image a name so we can store it in the phone's
                // default location
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                try {

                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                    String imageFileName = "bmh" + timeStamp + "_";
                    File albumF = getAlbumDir();
                    File imageF = File.createTempFile(imageFileName, "bmh", albumF);


                    mCurrentPhotoPath = imageF.getAbsolutePath();
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageF));
                } catch (IOException e) {
                    e.printStackTrace();

                }

                startActivityForResult(takePictureIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                dialogr.dismiss();
                dialogr.hide();
            }
        });
        dialogr.show();
    }

    private File getAlbumDir() {
        File storageDir = null;

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
                storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "CameraPicture");
            } else {
                storageDir = new File(Environment.getExternalStorageDirectory() + CAMERA_DIR + "CameraPicture");
            }

            if (storageDir != null) {
                if (!storageDir.mkdirs()) {
                    if (!storageDir.exists()) {
                        return null;
                    }
                }
            }

        } else {
        }

        return storageDir;
    }


    public void onActivityResult(final int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null) {
        }

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_PHOTO) {

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = this.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String imagePath1 = cursor.getString(columnIndex);

                File f = new File(compressImage(imagePath1, 1));
                Bitmap myBitmap = BitmapFactory.decodeFile(f.getAbsolutePath());

                profile_imgUser.setImageBitmap(myBitmap);
                photoPath = f.getAbsolutePath();

            } else if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {

                File f = new File(compressImage(mCurrentPhotoPath, 0));
                Bitmap myBitmap = BitmapFactory.decodeFile(f.getAbsolutePath());

                profile_imgUser.setImageBitmap(myBitmap);
                photoPath = f.getAbsolutePath();
            }
        }
    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;
    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = this.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

//    private void showEnterOTPDialog() {
//        otpDialog = new Dialog(ctx, R.style.Theme_Dialog_Custom_Anim);
//        otpDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        otpDialog.setContentView(R.layout.dialog_enter_otp);
//        ImageView cancelDialog = (ImageView) otpDialog.findViewById(R.id.imgClose);
//        final TextView tvCounter = (TextView) otpDialog.findViewById(R.id.tvVeifyingSec);
//        edNumber = (EditText) otpDialog.findViewById(R.id.edNumber);
//        timer = new CounterClass(30 * 1000, 1000, tvCounter);
//
//        edOTP = (EditText) otpDialog.findViewById(R.id.edOTP);
//        edOTP.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if (!edOTP.getText().toString().trim().isEmpty() && event != null)
//                    if (actionId == EditorInfo.IME_ACTION_GO || event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
//                        submitOTP();
//                        return true;
//                    } else {
//                        Toast.makeText(getApplicationContext(), "Please enter OTP", Toast.LENGTH_SHORT).show();
//                    }
//                return false;
//            }
//        });
//
//        TextView tvEditMobile = (TextView) otpDialog.findViewById(R.id.tvEdit);
//        final TextView tvResend = (TextView) otpDialog.findViewById(R.id.tvResend);
//        edNumber.setText(mobileNumber + "");
//        edNumber.setEnabled(false);
//
//
//        tvEditMobile.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                edNumber.setClickable(true);
//                edNumber.setFocusable(true);
//                edNumber.setEnabled(true);
//                edNumber.requestFocus();
//                edNumber.setSelection(edNumber.getText().length());
//            }
//        });
//        tvResend.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String no = edNumber.getText().toString().trim();
//
//                if (no.isEmpty()) {
//                    Toast.makeText(ctx, "Enter Mobile", Toast.LENGTH_SHORT).show();
//                } else if (no.length() < 10) {
//                    Toast.makeText(ctx, "Please enter valid Mobile", Toast.LENGTH_SHORT).show();
//                } else {
//                    mobileNumber = no;
//                    getMobileVerificationCode(profile_userName.getText().toString(), profile_userEmail.getText().toString(), mobileNumber);
//                    timer.cancel();
//                    timer = new CounterClass(30 * 1000, 1000, tvCounter);
//                    timer.start();
//                }
//            }
//        });
//        cancelDialog.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View arg0) {
//                otpDialog.dismiss();
//            }
//        });
//
//        tvCounter.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////				if(tvCounter.getText().toString().trim().equalsIgnoreCase("Resend OTP")){
////				}
//            }
//        });
//
//        timer.start();
//        otpDialog.show();
//    }

    public class CounterClass extends CountDownTimer {

        TextView tvCounter;

        public CounterClass(long millisInFuture, long countDownInterval, TextView tv) {
            super(millisInFuture, countDownInterval);
            tvCounter = tv;
        }

        @Override
        public void onTick(long millisUntilFinished) {
            long millis = millisUntilFinished;
            if (TimeUnit.MILLISECONDS.toSeconds(millis) != 1) {
                tvCounter.setText("Verifying in " + TimeUnit.MILLISECONDS.toSeconds(millis) + " Sec");
            } else {
                tvCounter.setVisibility(View.GONE);
                TextView tvmsg = (TextView) otpDialog.findViewById(R.id.tvMsg);
                tvmsg.setVisibility(View.VISIBLE);
                Button btnSubmit = (Button) otpDialog.findViewById(R.id.btnSubmit);
                btnSubmit.setVisibility(View.VISIBLE);
                btnSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (edNumber.getText().toString().trim().isEmpty()) {
                            Toast.makeText(ctx, "Enter Mobile", Toast.LENGTH_SHORT).show();
                        } else if (edNumber.length() < 10) {
                            Toast.makeText(ctx, "Please enter valid Mobile", Toast.LENGTH_SHORT).show();
                        } else if (edOTP.getText().toString().trim().isEmpty()) {
                            Toast.makeText(ctx, "Please enter OTP", Toast.LENGTH_SHORT).show();
                        } else {
                            submitOTP();
                        }
                    }
                });
            }
        }

        @Override
        public void onFinish() {
//			tvTimer.setText("Completed.");
        }
    }

    private void submitOTP() {
        String otp = edOTP.getText().toString().trim();
        if (!otp.isEmpty()) {
            otpDialog.dismiss();
            if (cd.isConnectingToInternet()) {
                WudStayConstants.BASE_URL = WudStayConstants.BASE_URL_WUDSTAY;
                final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
                progressDialog.showProgressDialog(ctx);
                progressDialog.getlDialog().setCancelable(Boolean.FALSE);
                ApiInterface apiService =
                        ApiClient.getClient().create(ApiInterface.class);

                Call<MobileNoVerificationPojo> call = apiService.getSendOTP(mobileNumber, otp);
                System.out.println("retrofit URL " + call.request());
                call.enqueue(new Callback<MobileNoVerificationPojo>() {
                    @Override
                    public void onResponse(Call<MobileNoVerificationPojo> call, Response<MobileNoVerificationPojo> response) {
                        if (response.body().getResponseCode() == 1) {
                            if (response.body().isResponseObject()) {
                                saveIntoPrefs(WudStayConstants.MOBILE, mobileNumber);
                                updateProfile();
                            }
                        } else {
                            dialog.displayCommonDialog(WudStayConstants.WRONG_OTP_MSG);
//                            Toast.makeText(ctx, "Please enter the correct OTP", Toast.LENGTH_LONG).show();
                        }
                        progressDialog.getlDialog().dismiss();
                    }

                    @Override
                    public void onFailure(Call<MobileNoVerificationPojo> call, Throwable t) {
                        // Log error here since request failed
                        System.out.println("retrofit hh failure " + t.getMessage());
                        progressDialog.getlDialog().dismiss();
                    }
                });
            } else {
                dialog.displayCommonDialog(WudStayConstants.NO_INTERNET_CONNECTED);
            }
        }

    }


//    public void getMobileVerificationCode(String name, String email, String phoneNo) {
//        if (cd.isConnectingToInternet()) {
//            WudStayConstants.BASE_URL = WudStayConstants.BASE_URL_WUDSTAY;
//            final ProgressDialog d = WudstayDialogs.showLoading(ctx);
//            d.setCanceledOnTouchOutside(false);
//            ApiInterface apiService =
//                    ApiClient.getClient().create(ApiInterface.class);
//
//            Call<MobileNoVerificationPojo> call = apiService.getMobileNoVerification(name, email, phoneNo);
//            System.out.println("retrofit URL " + call.request());
//            call.enqueue(new Callback<MobileNoVerificationPojo>() {
//                @Override
//                public void onResponse(Call<MobileNoVerificationPojo> call, Response<MobileNoVerificationPojo> response) {
//                    if (response.body().getResponseCode() == 1) {
//                        if (response.body().isResponseObject()) {
//                            showEnterOTPDialog();
//                        }
//                    }
//                    d.dismiss();
//                }
//
//                @Override
//                public void onFailure(Call<MobileNoVerificationPojo> call, Throwable t) {
//                    // Log error here since request failed
//                    d.dismiss();
//                }
//            });
//        } else {
//            dialog.displayCommonDialog(WudStayConstants.NO_INTERNET_CONNECTED);
//        }
//    }

    @Override
    public void onStart() {
        super.onStart();
        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(ctx);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WudStayConstants.ACTION_RECEIVED_OTP);
        bManager.registerReceiver(OTPBroadcastReceiver, intentFilter);
    }

    public void onStop() {
        super.onStop();
        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(ctx);
        bManager.unregisterReceiver(OTPBroadcastReceiver);

    }

    BroadcastReceiver OTPBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(WudStayConstants.ACTION_RECEIVED_OTP)) {
                if (edOTP != null) {
                    edOTP.setText(intent.getStringExtra("otpcode"));
                    submitOTP();
                }
            }
        }
    };

    private void uploadProfileDp() {
        // create upload service client
        FileUploadSer.FileUploadService service =
                ServiceGenerator.createService(FileUploadSer.FileUploadService.class);

//        // https://github.com/iPaulPro/aFileChooser/blob/master/aFileChooser/src/com/ipaulpro/afilechooser/utils/FileUtils.java
//        // use the FileUtils to get the actual file by uri
//        File file = FileUtils.getFile(this, fileUri);
        File file = new File(photoPath);

        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("uploadedFile", file.getName(), requestFile);

        // add another part within the multipart request
        String mobileString = getFromPrefs(WudStayConstants.MOBILE);
        RequestBody mobileNumber_requestbody =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), mobileString);

        // finally, execute the request
        Call<ImageUploadPojo> call = service.upload(mobileNumber_requestbody, body);

        call.enqueue(new Callback<ImageUploadPojo>() {
            @Override
            public void onResponse(Call<ImageUploadPojo> call,
                                   Response<ImageUploadPojo> response) {
                System.out.println("retrofit URL " + call.request());
                if (response != null && response.body() != null) {

                    if (response.body().getResponseCode() == 1) {
                        Toast.makeText(ProfileActivity.this, "Profile updated successfully", Toast.LENGTH_SHORT).show();
                        saveIntoPrefs(WudStayConstants.IMAGE, response.body().getResponseObject());
                        if (progressDialog.getlDialog() != null && progressDialog.getlDialog().isShowing())

                            startActivity(new Intent(ctx, ProfileActivity.class));
                        finish();
                    } else {
                        Toast.makeText(ProfileActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                progressDialog.getlDialog().dismiss();
            }

            @Override
            public void onFailure(Call<ImageUploadPojo> call, Throwable t) {
                System.out.println("retrofit URL " + call.request());
                progressDialog.getlDialog().dismiss();
            }
        });
    }

    public String compressImage(String imageUri, int flag) {
        String filePath;
        if (flag == 1) {
            filePath = getRealPathFromURI(imageUri);
        }

        filePath = imageUri;
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;
        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        ImageLoadingUtils utils = new ImageLoadingUtils(this);
        options.inSampleSize = utils.calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inDither = false;
//        options.inPurgeable = true;
//        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));


        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }
}
