package com.wudstay.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;

import com.wudstay.R;
import com.wudstay.util.WudStayConstants;
import com.wudstay.util.WudstayDialogs;

import java.util.ArrayList;

import static android.Manifest.permission.RECEIVE_SMS;

/**
 * Created by Yash ji on 9/23/2016.
 */

public class RegistrationActivity extends BaseActivity {

    private String navigate_from = "";

    private final static int RECEIVE_SMS_RESULT = 102;
    private SharedPreferences sharedPreferences;
    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected;
    private RegistrationActivity ctx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx);

        navigate_from = getIntent().getStringExtra("navigate_from");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            addPermissionDialogMarshMallowForSMS();
        } else {
            goAhead();
        }

    }

    private void addPermissionDialogMarshMallowForSMS() {
        ArrayList<String> permissions = new ArrayList<>();
        int resultCode = 0;

        permissions.add(RECEIVE_SMS);
        resultCode = RECEIVE_SMS_RESULT;

        //filter out the permissions we have already accepted
        permissionsToRequest = ctx.findUnAskedPermissions(permissions);
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.
        permissionsRejected = ctx.findRejectedPermissions(permissions);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (permissionsToRequest.size() > 0) {//we need to ask for permissions
                //but have we already asked for them?
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                //mark all these as asked..
                for (String perm : permissionsToRequest) {
                    ctx.markAsAsked(perm, sharedPreferences);
                }
            } else {
                //show the success banner
                if (permissionsRejected.size() < permissions.size()) {
                    //this means we can show success because some were already accepted.
                    //permissionSuccess.setVisibility(View.VISIBLE);
                    goAhead();
                }

                if (permissionsRejected.size() > 0) {
                    //we have none to request but some previously rejected..tell the user.
                    //It may be better to show a dialog here in a prod application
                    requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                    //mark all these as asked..
                    for (String perm : permissionsToRequest) {
                        ctx.markAsAsked(perm, sharedPreferences);
                    }
                }
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case RECEIVE_SMS_RESULT:
                if (ctx.hasPermission(RECEIVE_SMS)) {
                    //        permissionSuccess.setVisibility(View.VISIBLE);
                    goAhead();
                } else {
                    permissionsRejected.add(RECEIVE_SMS);
                    ctx.clearMarkAsAsked(RECEIVE_SMS);
                    String message = "permission for send and view SMS was rejected. Please allow to run the app.";
                    ctx.makePostRequestSnack(message, permissionsRejected.size());
                }
                break;
        }
    }

    private void goAhead() {
        WudstayDialogs mobileRegistrationDialog = new WudstayDialogs(ctx);
        mobileRegistrationDialog.showEnterMobileNoNewDialog(navigate_from);
    }

    public void NavigateToProfileActivity() {
        Intent intent = new Intent(ctx, ProfileActivity.class);
        startActivity(intent);
        finish();
    }

    public void navigateToPayments() {
        Intent mIntent = new Intent(ctx, PaymentActivity.class);
        startActivity(mIntent);
        finish();
    }

    public void navigateToComplaints() {
        Intent mIntent = new Intent(ctx, ComplaintsActivity.class);
        startActivity(mIntent);
        finish();
    }

    public void navigateTofavourites() {
        Intent mIntent = new Intent(ctx, FavouritesActivity.class);
        startActivity(mIntent);
        finish();
    }

    @Override
    public void onStart() {
        super.onStart();
        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WudStayConstants.ACTION_RECEIVED_OTP);
        bManager.registerReceiver(OTPBroadcastReceiver, intentFilter);
    }

    public void onStop() {
        super.onStop();
        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(this);
        bManager.unregisterReceiver(OTPBroadcastReceiver);
    }

    BroadcastReceiver OTPBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(WudStayConstants.ACTION_RECEIVED_OTP)) {
                if (otpEditText != null) {
                    otpEditText.setText(intent.getStringExtra("otpcode"));
                    submitOTP(otpEditText);
                }
            }
        }
    };

    public void finishActivity() {
        finish();
    }
}