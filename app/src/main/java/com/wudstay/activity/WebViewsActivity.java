package com.wudstay.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.wudstay.R;
import com.wudstay.util.ConnectionDetector;
import com.wudstay.util.WudStayConstants;
import com.wudstay.util.WudstayDialogs;

public class WebViewsActivity extends BaseActivity {

    private WebView webview;
    private WebViewsActivity ctx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        setDrawerAndToolbar("About Us");
        WudStayConstants.ACTIVITIES.add(ctx);
        webview = (WebView) findViewById(R.id.webview);

        TextView header_text = (TextView) findViewById(R.id.header_text);
//        ImageView back = (ImageView) findViewById(R.id.back);

        header_text.setText(getIntent().getStringExtra("header"));
/*        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });*/

        final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
        progressDialog.showProgressDialog(ctx);

        webview.getSettings().setJavaScriptEnabled(true);

        webview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress >= 85) {
                    if (progressDialog.getlDialog() != null && progressDialog.getlDialog().isShowing())
                        progressDialog.getlDialog().dismiss();
                }
            }
        });

        webview.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if(url.equals("http://www.wudstay.com/m/"))
                    finish();
                else if (url.toLowerCase().startsWith("http") || url.toLowerCase().startsWith("https") || url.toLowerCase().startsWith("file")) {
                    view.loadUrl(url);
                } else {
                    try {
                        Uri uri = Uri.parse(url);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    } catch (Exception e) {
                        Log.d("JSLogs", "Webview Error:" + e.getMessage());
                    }
                }
                return (true);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }
        });

        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        WudstayDialogs dialog = new WudstayDialogs(ctx);
        if (cd.isConnectingToInternet()) {
            openURL(getIntent().getStringExtra("url"));
        } else {
            if (progressDialog.getlDialog() != null && progressDialog.getlDialog().isShowing())
                progressDialog.getlDialog().dismiss();
            dialog.displayCommonDialog(WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }

    private void openURL(String url) {
        webview.loadUrl(url);
        webview.requestFocus();
    }
}
