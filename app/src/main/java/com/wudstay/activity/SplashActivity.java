package com.wudstay.activity;

import android.app.ActivityManager;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wudstay.R;
import com.wudstay.model.ApiClient;
import com.wudstay.model.ApiInterface;
import com.wudstay.pojo.NewUpdatePojo;
import com.wudstay.util.ConnectionDetector;
import com.wudstay.util.WudStayConstants;
import com.wudstay.util.WudstayDialogs;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends BaseActivity {

    private SplashActivity ctx = this;
    private Handler splashTimeHandler;
    private Runnable finalizer;
    private static final String TAG = "SplashActivity";
    private ConnectionDetector cd;
    private WudstayDialogs dialog;
    private String version;
    private String serverVersion;
    private Dialog lDialog;
    private boolean mFlag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        cd = new ConnectionDetector(getApplicationContext());
        dialog = new WudstayDialogs(ctx);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(ctx, R.color.splash_header));
        }

        WudStayConstants.ACTIVITIES = new ArrayList<>();

        checkUpdate();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (splashTimeHandler != null && finalizer != null)
            splashTimeHandler.removeCallbacks(finalizer);
    }

    private void goAhead() {
        splashTimeHandler = new Handler();
        finalizer = new Runnable() {
            public void run() {
                Intent mainIntent = new Intent(ctx, HomeActivity.class);
                startActivity(mainIntent);
                finish();
            }
        };
        splashTimeHandler.postDelayed(finalizer, 1000);
    }

    public void checkUpdate() {
        PackageInfo pInfo;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (cd.isConnectingToInternet()) {
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<NewUpdatePojo> call = apiService.getNewUpdate("ANDROID", version);
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<NewUpdatePojo>() {
                @Override
                public void onResponse(Call<NewUpdatePojo> call, Response<NewUpdatePojo> response) {
                    if (response.body() != null && response.body().getResponseObject() != null) {
                        if (response.body().getResponseCode() == 1) {

                            if (response.body().getResponseObject().getLATEST_VERSION() != null) {
                                if (response.body().getResponseObject().getLATEST_VERSION().getCurrentVersion() != null) {
                                    serverVersion = response.body().getResponseObject().getLATEST_VERSION().getCurrentVersion();
                                }
                                serverVersion = serverVersion.replace(".", "");
                                version = version.replace(".", "");
                                if (serverVersion.length() < 3)
                                    serverVersion = serverVersion + "0";
                                if (version.length() < 3)
                                    version = version + "0";

                                if (version != null && serverVersion != null && Integer.parseInt(version) < Integer.parseInt(serverVersion)) {

                                    if (!response.body().getResponseObject().getLATEST_VERSION().isMandatoryUpdate()) {
                                        // show dialog non mandatory update dialog
                                        mFlag = false;
                                        showTwoButtonDialog("App Update available.", "A new app version is available. Do you want to update Wudstay now?", "Update", "Will do later.", false, false, mFlag);
                                    } else {
                                        mFlag = true;
                                        showTwoButtonDialog("App Update available.", "A new app version is available. Please update and enjoy Wudstay.", "Update", "Will do later.", false, false, mFlag);
                                    }
                                } else {
                                    goAhead();
                                }
                            }

                        } else {
                            goAhead();
                        }
                    }
                }

                @Override
                public void onFailure(Call<NewUpdatePojo> call, Throwable t) {
                    goAhead();
                }
            });
        } else {
            dialog.displayCommonDialog(WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }

    public void showTwoButtonDialog(String title, String message, String positiveBtnTitle, String NegativeBtnTitle, final boolean cancelOntouchOutside, final boolean showXbutton, final boolean isUpadteMendatory) {
        lDialog = new Dialog(ctx, R.style.Theme_Dialog_Custom_Anim);
        lDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        lDialog.setContentView(R.layout.dialog_cancel_booking);

        TextView tvTitle = (TextView) lDialog.findViewById(R.id.tvTitle);
        TextView tvMessage = (TextView) lDialog.findViewById(R.id.tvMessage);
        ImageView imgClose = (ImageView) lDialog.findViewById(R.id.imgClose);
        tvTitle.setText(title);
        tvMessage.setText(message);
        Button btnUpdate = (Button) lDialog.findViewById(R.id.btnYes);
        btnUpdate.setText(positiveBtnTitle);
        Button willDoLater = (Button) lDialog.findViewById(R.id.btnNo);
        willDoLater.setText(NegativeBtnTitle);

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lDialog.dismiss();
                launchMarket(ctx);
            }
        });
        willDoLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!isUpadteMendatory) {
                    goAhead();
                } else {
                    finish();
                }
                lDialog.dismiss();
            }
        });
        LinearLayout llOut = (LinearLayout) lDialog.findViewById(R.id.llOutside);

        llOut.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (cancelOntouchOutside)
                    lDialog.dismiss();
            }
        });
        if (!showXbutton) {
            imgClose.setVisibility(View.GONE);
        }
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lDialog.dismiss();
            }
        });
        ActivityManager am = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
        ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
        if (cn.getShortClassName().equals(".activity.SplashActivity"))
            lDialog.show();
    }
}
