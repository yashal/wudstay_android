package com.wudstay.activity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.wudstay.R;
import com.wudstay.model.ApiClient;
import com.wudstay.model.ApiInterface;
import com.wudstay.pojo.FavoriteResponsePojo;
import com.wudstay.pojo.PGDetailNewPojo;
import com.wudstay.pojo.ScheduleVisitPojo;
import com.wudstay.util.ConnectionDetector;
import com.wudstay.util.WudStayConstants;
import com.wudstay.util.WudstayDialogs;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.RECEIVE_SMS;

public class ScheduleVisitActivity extends BaseActivity implements AdapterView.OnItemSelectedListener, BookmarkCallback {

    private PGDetailNewPojo pgDetail;
    private ScheduleVisitActivity ctx = this;
    private String address = "";
    private TextView today_text, tomorrow_text, pic_date_text;
    private ImageView favorite;
    private TextView hrs, min, am_pm;
    private Calendar myCalendar;
    private int month, year, day;
    private String visitDate = "";
    private String visitTime;
    private ConnectionDetector cd;
    private WudstayDialogs dialog;
    private final static int RECEIVE_SMS_RESULT = 102;
    private SharedPreferences sharedPreferences;
    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected;
    //    private CollapsingToolbarLayout collapsingToolbar;
    private String select_day;
    private int int_selecteHour, int_selectedMinute;
    SimpleDateFormat inputParser = new SimpleDateFormat("hh:mm", Locale.US);

    private String[] time = {"8:00 AM", "8:30 AM", "9:00 AM", "9:30 AM", "10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM", "12:00 PM",
            "12:30 PM", "1:00 PM", "1:30 PM", "2:00 PM", "2:30 PM", "3:00 PM", "3:30 PM", "4:00 PM", "4:30 PM", "5:00 PM",
            "5:30 PM", "6:00 PM", "6:30 PM", "7:00 PM", "7:30 PM", "8:00 PM", "8:30 PM", "9:00 PM"};

    private Spinner spinnerTime;
    private String selState;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_visit);

        setDrawerAndToolbar("Payments");
        WudStayConstants.ACTIVITIES.add(ctx);

        ImageView back = (ImageView) findViewById(R.id.back);
        favorite = (ImageView) findViewById(R.id.favorite);
        ImageView pgImage = (ImageView) findViewById(R.id.pgImage);
        final LinearLayout pic_date_layout = (LinearLayout) findViewById(R.id.pic_date_layout);
        final LinearLayout tomorrow_layout = (LinearLayout) findViewById(R.id.tomorrow_layout);
        final LinearLayout today_layout = (LinearLayout) findViewById(R.id.today_layout);
        final LinearLayout confirm_booking = (LinearLayout) findViewById(R.id.confirm_booking);
        today_text = (TextView) findViewById(R.id.today_text);
        tomorrow_text = (TextView) findViewById(R.id.tomorrow_text);
        pic_date_text = (TextView) findViewById(R.id.pic_date_text);
        TextView pd_address = (TextView) findViewById(R.id.pd_address);
        TextView contact_person_name = (TextView) findViewById(R.id.contact_person_name);
        TextView contact_person_mobile = (TextView) findViewById(R.id.contact_person_mobile);
        hrs = (TextView) findViewById(R.id.hrs);
        min = (TextView) findViewById(R.id.min);
        am_pm = (TextView) findViewById(R.id.am_pm);
        LinearLayout time_picker = (LinearLayout) findViewById(R.id.time_picker);

        /*spinnerTime = (Spinner) findViewById(R.id.time_spinner);
        ArrayAdapter<String> adapter_state = new ArrayAdapter<>(this, R.layout.spinner_item, time);
        adapter_state.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);*/
        spinnerTime = (Spinner) findViewById(R.id.time_spinner);
        ArrayAdapter<String> adapter_state = new ArrayAdapter<String>(this,
                R.layout.spinner_item, time);
        adapter_state
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        pgDetail = (PGDetailNewPojo) getIntent().getSerializableExtra("pgDetail");

        if (pgDetail.getMainImage() != null)
        {
            setImageInLayout(ctx, 600, (int) (getScreenHeight() / 2.4), pgDetail.getMainImage(), pgImage);
        }


        if (pgDetail.isFavouritePg())
            favorite.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.favorite_red));
        else
            favorite.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.favorite));

        cd = new ConnectionDetector(getApplicationContext());
        dialog = new WudstayDialogs(ctx);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        myCalendar = Calendar.getInstance();
        day = myCalendar.DAY_OF_MONTH;
        month = myCalendar.MONTH;
        year = myCalendar.YEAR;

        int hour = myCalendar.get(Calendar.HOUR_OF_DAY);
        int minute = myCalendar.get(Calendar.MINUTE);

        if (hour >= 12)
            am_pm.setText("pm");
        else
            am_pm.setText("am");

        hrs.setText(hour + "hrs");
        min.setText(minute + "min");

        visitTime = hour + ":" + minute + " " + am_pm.getText().toString();

        address = getAddress(pgDetail.getDisplayAddress());

        pd_address.setText(address);
        contact_person_name.setText(pgDetail.getContactPersonName());
        contact_person_mobile.setText(pgDetail.getContactPersonNumber());

        today_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                select_day = "today";

                setLightColorLayout(today_layout, today_text);
                setDarkColorLayout(tomorrow_layout, tomorrow_text);
                setDarkColorLayout(pic_date_layout, pic_date_text);
                pic_date_text.setText(getResources().getString(R.string.pickDate));

                visitDate = getTodayDate();
            }
        });

        tomorrow_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                select_day = "tomorrow";
                setDarkColorLayout(today_layout, today_text);
                setLightColorLayout(tomorrow_layout, tomorrow_text);
                setDarkColorLayout(pic_date_layout, pic_date_text);
                pic_date_text.setText(getResources().getString(R.string.pickDate));

                visitDate = getTomorrowDate();
            }
        });

        pic_date_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                select_day = "other";
                setDarkColorLayout(today_layout, today_text);
                setDarkColorLayout(tomorrow_layout, tomorrow_text);
                setLightColorLayout(pic_date_layout, pic_date_text);
                showDatePicker();
            }
        });

        spinnerTime.setAdapter(adapter_state);
        spinnerTime.setOnItemSelectedListener(ctx);

        time_picker.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showTimePicker();
            }
        });

        confirm_booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                getWudstayURL();
                int hour = now.get(Calendar.HOUR_OF_DAY);
                int minute = now.get(Calendar.MINUTE);
                String compareStringTwo = int_selecteHour + ":" + int_selectedMinute;

                Date date = parseDate(hour + ":" + minute);
                Date dateCompareTwo = parseDate(compareStringTwo);
                if (visitDate.equals("")) {
                    Toast.makeText(ctx, "Please select a date to visit", Toast.LENGTH_SHORT).show();
                } else if (select_day.equals("today") && !dateCompareTwo.after(date)) {
                    Toast.makeText(getApplicationContext(),
                            "Please select a valid time", Toast.LENGTH_LONG).show();
                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    addPermissionDialogMarshMallowForSMS();
                } else {
                    goAhead();
                }
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("pgDetail", pgDetail);
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        });

        favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddRemoveFavoriteNew(pgDetail, ctx);
            }
        });
//        setToolBarValues();
    }

    private Date parseDate(String date) {

        try {
            return inputParser.parse(date);
        } catch (java.text.ParseException e) {
            return new Date(0);
        }
    }

    /*private void setToolBarValues() {
//        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapse_toolbar);
//        collapsingToolbar.setTitle("");
//
//        collapsingToolbar.setCollapsedTitleTextColor(ContextCompat.getColor(ctx, R.color.white));
//        collapsingToolbar.setExpandedTitleColor(ContextCompat.getColor(ctx, R.color.white));

        int height = (int) (getScreenHeight() / 2.4);

        AppBarLayout appbar = (AppBarLayout) findViewById(R.id.MyAppbar);
        appbar.requestLayout();
        appbar.getLayoutParams().height = height;

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) appbar.getLayoutParams();
        AppBarLayout.Behavior behavior = new AppBarLayout.Behavior();
        behavior.setDragCallback(new AppBarLayout.Behavior.DragCallback() {
            @Override
            public boolean canDrag(AppBarLayout appBarLayout) {
                return false;
            }
        });
        params.setBehavior(behavior);
    }*/

    public void goAhead() {
        if (getFromPrefs(WudStayConstants.NAME) == null || getFromPrefs(WudStayConstants.NAME).equals("")) {
            WudstayDialogs enterMobileDialog = new WudstayDialogs(ctx);
            enterMobileDialog.showEnterMobileNoNewDialog("ScheduleVisitActivity");
        } else
            scheduleVisit();
    }

    private void addPermissionDialogMarshMallowForSMS() {
        ArrayList<String> permissions = new ArrayList<>();
        int resultCode = 0;

        permissions.add(RECEIVE_SMS);
        resultCode = RECEIVE_SMS_RESULT;

        //filter out the permissions we have already accepted
        permissionsToRequest = findUnAskedPermissions(permissions);
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.
        permissionsRejected = findRejectedPermissions(permissions);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (permissionsToRequest.size() > 0) {//we need to ask for permissions
                //but have we already asked for them?
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                //mark all these as asked..
                for (String perm : permissionsToRequest) {
                    markAsAsked(perm, sharedPreferences);
                }
            } else {
                //show the success banner
                if (permissionsRejected.size() < permissions.size()) {
                    //this means we can show success because some were already accepted.
                    //permissionSuccess.setVisibility(View.VISIBLE);
                    goAhead();
                }

                if (permissionsRejected.size() > 0) {
                    //we have none to request but some previously rejected..tell the user.
                    //It may be better to show a dialog here in a prod application

                    requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                    //mark all these as asked..
                    for (String perm : permissionsToRequest) {
                        markAsAsked(perm, sharedPreferences);
                    }
                }
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case RECEIVE_SMS_RESULT:
                if (hasPermission(RECEIVE_SMS)) {
                    //        permissionSuccess.setVisibility(View.VISIBLE);
                    goAhead();
                } else {
                    permissionsRejected.add(RECEIVE_SMS);
                    clearMarkAsAsked(RECEIVE_SMS);
                    String message = "permission for send and view SMS was rejected. Please allow to run the app.";
                    makePostRequestSnack(message, permissionsRejected.size());
                }
                break;
        }
    }

    private void updateLabel() {
        SimpleDateFormat sdf = getDateFormat();
        visitDate = sdf.format(myCalendar.getTime());
        pic_date_text.setText(sdf.format(myCalendar.getTime()));
    }

    private void showTimePicker() {
        int hour = myCalendar.get(Calendar.HOUR_OF_DAY);
        int minute = myCalendar.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(ctx, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                hrs.setText(selectedHour + "hrs");
                min.setText(selectedMinute + "min");
                if (selectedHour >= 12)
                    am_pm.setText("pm");
                else
                    am_pm.setText("am");
                int_selectedMinute = selectedMinute;
                int_selecteHour = selectedHour;
                visitTime = selectedHour + ":" + selectedMinute + " " + am_pm.getText().toString();
                myCalendar.set(year, month, day, selectedHour, selectedMinute, 00);
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    private void showDatePicker() {
        DatePickerDialog dialog = new DatePickerDialog(ctx, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker arg0, int year, int month, int day) {
                myCalendar.set(year, month, day);
                ctx.year = year;
                ctx.month = month;
                ctx.day = day;
                updateLabel();
            }
        }, year, month, day);
        dialog.getDatePicker().setMinDate((System.currentTimeMillis() + 2*24*60*60*1000) - 1000);
        dialog.show();
    }

    public void scheduleVisit() {
        getWudstayURL();
        if (cd.isConnectingToInternet()) {
            final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<ScheduleVisitPojo> call = apiService.scheduleVisit(Long.valueOf(pgDetail.getId()), getFromPrefs(WudStayConstants.NAME), getFromPrefs(WudStayConstants.EMAIL), getFromPrefs(WudStayConstants.MOBILE), selState, visitDate);
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<ScheduleVisitPojo>() {
                @Override
                public void onResponse(Call<ScheduleVisitPojo> call, Response<ScheduleVisitPojo> response) {
                    if (response.body() != null && response.body().getResponseCode() == 1) {

                        WudstayDialogs mobileRegistrationDialog = new WudstayDialogs(ctx);
                        mobileRegistrationDialog.showThanksDialog("Your visit is scheduled for " + response.body().getResponseObject().getPG_NAME() + " at " + response.body().getResponseObject().getDATE_AND_TIME_OF_VISIT() + ".");
//                        Toast.makeText(ctx, "Your visit is scheduled for " + response.body().getResponseObject().getPG_NAME() + " at " + response.body().getResponseObject().getDATE_AND_TIME_OF_VISIT() + ".", Toast.LENGTH_LONG).show();
//                        finish();
                    } else {
                        if (response.body() != null && response.body().getMessage() != null) {
                            dialog.displayCommonDialog(response.body().getMessage());
                        }
                    }
                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<ScheduleVisitPojo> call, Throwable t) {
                    // Log error here since request failed
                    System.out.println("hh failure: " + t.getMessage());
                    t.printStackTrace();
                    progressDialog.getlDialog().dismiss();
                }
            });
        } else {
            dialog.displayCommonDialog(WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WudStayConstants.ACTION_RECEIVED_OTP);
        bManager.registerReceiver(OTPBroadcastReceiver, intentFilter);
    }

    public void onStop() {
        super.onStop();
        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(this);
        bManager.unregisterReceiver(OTPBroadcastReceiver);
    }

    BroadcastReceiver OTPBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(WudStayConstants.ACTION_RECEIVED_OTP)) {
                if (otpEditText != null) {
                    otpEditText.setText(intent.getStringExtra("otpcode"));
                    submitOTP(otpEditText);
                }
            }
        }
    };

    public void onItemSelected(AdapterView<?> parent, View view, int position,
                               long id) {
        spinnerTime.setSelection(position);
         selState = (String) spinnerTime.getSelectedItem();
        String splitArr = getReminingTime(selState);
        String[] splitTimeArr = splitArr.split(":");
        int_selecteHour = Integer.parseInt(splitTimeArr[0]);
        int_selectedMinute = Integer.parseInt(splitTimeArr[1]);

    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    private String getReminingTime(String _12HourTime) {

        String returning_Obj = "";
        try {
            SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
            SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
            Date _12HourDt = _12HourSDF.parse(_12HourTime);
            returning_Obj = ""+_24HourSDF.format(_12HourDt);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return returning_Obj;
    }
    @Override
    public void onSuccess(FavoriteResponsePojo obj) {
        if (obj != null && obj.getResponseObject().isActive()) {
            favorite.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.favorite_red));
            pgDetail.setFavouritePg(true);
        } else {
            favorite.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.favorite));
            pgDetail.setFavouritePg(false);
        }
    }

    @Override
    public void onError() {

    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("pgDetail", pgDetail);
        setResult(RESULT_OK, returnIntent);
        finish();
    }
}