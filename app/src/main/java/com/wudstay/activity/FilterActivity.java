package com.wudstay.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wudstay.R;
import com.wudstay.pojo.CityLocationsListPojo;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class FilterActivity extends BaseActivity {

    private FilterActivity ctx = this;
    private TextView coming_saturday, coming_sunday, booking_personName, booking_address, pic_date_text,
            booking_girlText, booking_boyText, display_location, change_location;
    private ImageView booking_boyImg, booking_girlImg;
    private LinearLayout boy_layout, girl_layout, datePick, saturday_layout, sunday_layout;
    private LinearLayout single_sharing_layout, double_sharing_layout, triple_sharing_layout;
    private TextView single_sharing_text, double_sharing_text, triple_sharing_text;
    private ImageView single_sharing_image, double_sharing_image, triple_sharing_image;
    private Calendar myCalendar;
    private int month, year, day;
    private String visitDate = "";
    private int genderId = 0;
    private String city_name = "";
    private String occupancy_type = "";
    private String locationsName = "";
    private Dialog location_list_dialog;
    private RecyclerView mRecyclerViewDialog;
    private ArrayList<CityLocationsListPojo> locationList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        findAllGlobalViews();

        LinearLayout search = (LinearLayout) findViewById(R.id.search);

        locationList = (ArrayList<CityLocationsListPojo>) getIntent().getSerializableExtra("locationList");

//        gps_icon.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    addPermissionDialogMarshMallow();
//                } else {
//                    gpsRequest();
//                }
//            }
//        });

        visitDate = getIntent().getStringExtra("visitDate");
        occupancy_type = getIntent().getStringExtra("occupancy_type");
        locationsName = getIntent().getStringExtra("locationsName");
        if (getIntent().getStringExtra("genderId") != null && !getIntent().getStringExtra("genderId").equals(""))
            genderId = Integer.parseInt(getIntent().getStringExtra("genderId"));

        if (genderId == 1)
            setMaleSelected();
        else if (genderId == 2)
            setFemaleSelected();

        if (locationsName.equals(""))
            display_location.setText(getString(R.string.all_location));
        else
            display_location.setText(locationsName);

        change_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayLocationDialog();
            }
        });

        myCalendar = Calendar.getInstance();
        day = myCalendar.DAY_OF_MONTH;
        month = myCalendar.MONTH;
        year = myCalendar.YEAR;

        // for Saturday
        String saturday = getNextSaturday();
        final String[] separated = saturday.split("/");
        coming_saturday.setText(separated[0] + getDayOfMonthSuffix(Integer.parseInt(separated[0])) + "\n" + separated[1]);

        // for Sunday
        String sunday = getNextSunday();
        final String[] checkOut_separated = sunday.split("/");
        coming_sunday.setText(checkOut_separated[0] + getDayOfMonthSuffix(Integer.parseInt(checkOut_separated[0])) + "\n" + checkOut_separated[1]);

        if (!visitDate.equals("")) {
            day = Integer.parseInt(visitDate.split("/")[0]);
            month = Integer.parseInt(visitDate.split("/")[1]) - 1;
            year = Integer.parseInt(visitDate.split("/")[2]);
            if (day == Integer.parseInt(separated[0]) && month + 1 == Integer.parseInt(getMonth(separated[1]))) {
                setSaturdaySelected();
            } else if (day == Integer.parseInt(checkOut_separated[0]) && month + 1 == Integer.parseInt(getMonth(checkOut_separated[1]))) {
                setSundaySelected();
            } else {
                setPickDateSelected();
                myCalendar.set(year, month, day);
                updateLabel();
            }
        }

        if (!occupancy_type.equals("")) {
            if (occupancy_type.equals("1"))
                setSingleSharingSelected();
            else if (occupancy_type.equals("2"))
                setDoubleSharingSelected();
            else
                setTripleSharingSelected();
        }

        boy_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setMaleSelected();
            }
        });

        girl_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFemaleSelected();
            }
        });

        saturday_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSaturdaySelected();
                pic_date_text.setText(getResources().getString(R.string.pick_date));

                if (separated != null && separated.length > 2)
                    visitDate = separated[0] + "/" + getMonth(separated[1]) + "/" + separated[2];
            }
        });

        sunday_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSundaySelected();
                pic_date_text.setText(getResources().getString(R.string.pick_date));

                if (checkOut_separated != null && checkOut_separated.length > 2)
                    visitDate = checkOut_separated[0] + "/" + getMonth(checkOut_separated[1]) + "/" + checkOut_separated[2];
            }
        });

        datePick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPickDateSelected();
                showDatePicker();
            }
        });

        single_sharing_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSingleSharingSelected();
            }
        });

        double_sharing_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDoubleSharingSelected();
            }
        });

        triple_sharing_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setTripleSharingSelected();
            }
        });

        ImageView cross = (ImageView) findViewById(R.id.cross);
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        city_name = getIntent().getStringExtra("city_name");

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                intent.putExtra("visitDate", visitDate);
                intent.putExtra("genderId", genderId);
                intent.putExtra("occupancy_type", occupancy_type);
                intent.putExtra("city_name", city_name);
                intent.putExtra("locationsName", locationsName);
                intent.putExtra("locationList", locationList);
                finish();
            }
        });

    }

    private void displayLocationDialog() {
        location_list_dialog = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        location_list_dialog.setContentView(R.layout.locations_list_dialog);

        mRecyclerViewDialog = (RecyclerView) location_list_dialog.findViewById(R.id.mp3songs_artistdialog_recycler);
        LinearLayout dialog_doneLL = (LinearLayout) location_list_dialog.findViewById(R.id.dialog_doneLL);

        location_list_dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        mRecyclerViewDialog.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(ctx);
        mRecyclerViewDialog.setLayoutManager(mLayoutManager);

        location_list_dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                setText();
            }
        });

        dialog_doneLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                location_list_dialog.dismiss();
            }
        });

        LocationListAdapter adapter = new LocationListAdapter(ctx, locationList);
        if (mRecyclerViewDialog != null)
            mRecyclerViewDialog.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        location_list_dialog.show();
    }

    private void setText() {
        locationsName = "";
        for (int i = 0; i < locationList.size(); i++) {
            if (locationList.get(i).isSelected()) {
                locationsName += locationList.get(i).getDisplayName() + ", ";
            }
        }

        if (locationsName.length() > 0)
            locationsName = locationsName.substring(0, locationsName.length() - 2);

        if (locationsName.equals(""))
            display_location.setText(getString(R.string.all_location));
        else
            display_location.setText(locationsName);

    }

    private void setSingleSharingSelected() {
        setLightColor(single_sharing_layout, single_sharing_text, single_sharing_image, R.mipmap.onebed_grey);
        setDarkColor(double_sharing_layout, double_sharing_text, double_sharing_image, R.mipmap.twobed_black);
        setDarkColor(triple_sharing_layout, triple_sharing_text, triple_sharing_image, R.mipmap.threebed_black);
        occupancy_type = "1";
    }

    private void setDoubleSharingSelected() {
        setDarkColor(single_sharing_layout, single_sharing_text, single_sharing_image, R.mipmap.onebed_black);
        setLightColor(double_sharing_layout, double_sharing_text, double_sharing_image, R.mipmap.twobed_grey);
        setDarkColor(triple_sharing_layout, triple_sharing_text, triple_sharing_image, R.mipmap.threebed_black);
        occupancy_type = "2";
    }

    private void setTripleSharingSelected() {
        setDarkColor(single_sharing_layout, single_sharing_text, single_sharing_image, R.mipmap.onebed_black);
        setDarkColor(double_sharing_layout, double_sharing_text, double_sharing_image, R.mipmap.twobed_black);
        setLightColor(triple_sharing_layout, triple_sharing_text, triple_sharing_image, R.mipmap.threebed_grey);
        occupancy_type = "3";
    }

    private void setSaturdaySelected() {
        setLightColorLayout(saturday_layout, coming_saturday);
        setDarkColorLayout(sunday_layout, coming_sunday);
        setDarkColorLayout(datePick, pic_date_text);
    }

    private void setSundaySelected() {
        setDarkColorLayout(saturday_layout, coming_saturday);
        setLightColorLayout(sunday_layout, coming_sunday);
        setDarkColorLayout(datePick, pic_date_text);
    }

    private void setPickDateSelected() {
        setDarkColorLayout(saturday_layout, coming_saturday);
        setDarkColorLayout(sunday_layout, coming_sunday);
        setLightColorLayout(datePick, pic_date_text);
    }

    private void findAllGlobalViews() {
        single_sharing_layout = (LinearLayout) findViewById(R.id.single_sharing_layout);
        double_sharing_layout = (LinearLayout) findViewById(R.id.double_sharing_layout);
        triple_sharing_layout = (LinearLayout) findViewById(R.id.triple_sharing_layout);

        single_sharing_image = (ImageView) findViewById(R.id.single_sharing_image);
        double_sharing_image = (ImageView) findViewById(R.id.double_sharing_image);
        triple_sharing_image = (ImageView) findViewById(R.id.triple_sharing_image);

        single_sharing_text = (TextView) findViewById(R.id.single_sharing_text);
        double_sharing_text = (TextView) findViewById(R.id.double_sharing_text);
        triple_sharing_text = (TextView) findViewById(R.id.triple_sharing_text);

        saturday_layout = (LinearLayout) findViewById(R.id.saturday_layout);
        sunday_layout = (LinearLayout) findViewById(R.id.sunday_layout);
        coming_saturday = (TextView) findViewById(R.id.coming_saturday);
        coming_sunday = (TextView) findViewById(R.id.coming_sunday);
        datePick = (LinearLayout) findViewById(R.id.datePick);
        pic_date_text = (TextView) findViewById(R.id.booking_pic_date_text);
        display_location = (TextView) findViewById(R.id.display_location);
        change_location = (TextView) findViewById(R.id.change_location);

        boy_layout = (LinearLayout) findViewById(R.id.boy_layout);
        girl_layout = (LinearLayout) findViewById(R.id.girl_layout);

        booking_boyImg = (ImageView) findViewById(R.id.booking_boyImg);
        booking_girlImg = (ImageView) findViewById(R.id.booking_girlImg);
        booking_girlText = (TextView) findViewById(R.id.booking_girlText);
        booking_boyText = (TextView) findViewById(R.id.booking_boyText);
    }

    private void setMaleSelected() {
        setLightColor(boy_layout, booking_boyText, booking_boyImg, R.mipmap.male_grey);
        setDarkColor(girl_layout, booking_girlText, booking_girlImg, R.mipmap.female);
        genderId = 1;
    }

    private void setFemaleSelected() {
        setLightColor(girl_layout, booking_girlText, booking_girlImg, R.mipmap.female_grey);
        setDarkColor(boy_layout, booking_boyText, booking_boyImg, R.mipmap.male);
        genderId = 2;
    }

    private void showDatePicker() {
        DatePickerDialog dialog = new DatePickerDialog(ctx, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker arg0, int year, int month, int day) {
                myCalendar.set(year, month, day);
                ctx.year = year;
                ctx.month = month;
                ctx.day = day;
                updateLabel();
            }
        }, year, month, day);
        dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        dialog.show();
    }

    private void updateLabel() {
        SimpleDateFormat sdf = getDateFormat();
        visitDate = sdf.format(myCalendar.getTime());
        DateFormat outputFormat = new SimpleDateFormat("dd/MMMM");
        String inputDateStr = sdf.format(myCalendar.getTime());
        Date date = null;
        try {
            date = sdf.parse(inputDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outputDateStr = outputFormat.format(date);
        String[] separated = outputDateStr.split("/");
        pic_date_text.setText(separated[0] + getDayOfMonthSuffix(Integer.parseInt(separated[0])) + "\n" + separated[1]);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        finish();
    }

    public class LocationListAdapter extends RecyclerView.Adapter<LocationListAdapter.ViewHolder> {

        private ArrayList<CityLocationsListPojo> locationList;
        private Activity context;

        public LocationListAdapter(Activity context, ArrayList<CityLocationsListPojo> arrayArtistList) {
            this.context = context;
            this.locationList = arrayArtistList;
        }

        @Override
        public LocationListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.locations_row_layout, parent, false);
            v.setOnClickListener(new MyOnClickListener());
            ViewHolder viewHolder = new ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final LocationListAdapter.ViewHolder holder, int position) {
            final CityLocationsListPojo data = locationList.get(position);

            holder.artist_name.setText(data.getDisplayName());
            if (data.isSelected()) {
                holder.check.setChecked(true);
                holder.check.setButtonDrawable(R.mipmap.checkbox_selected);
            } else {
                holder.check.setChecked(false);
                holder.check.setButtonDrawable(R.mipmap.checkbox);
            }
        }

        class MyOnClickListener implements View.OnClickListener {
            @Override
            public void onClick(View v) {
                int itemPosition = mRecyclerViewDialog.getChildPosition(v);
                final CityLocationsListPojo data = locationList.get(itemPosition);
                CheckBox check = (CheckBox) v.findViewById(R.id.check_box);
                if (!data.isSelected()) {
                    check.setButtonDrawable(R.mipmap.checkbox_selected);
                    check.setChecked(true);
                    data.setSelected(true);
                } else {
                    check.setButtonDrawable(R.mipmap.checkbox);
                    check.setChecked(false);
                    data.setSelected(false);
                }
            }
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            private TextView artist_name;
            private CheckBox check;

            public ViewHolder(View itemView) {
                super(itemView);
                artist_name = (TextView) itemView.findViewById(R.id.artist_name);
                check = (CheckBox) itemView.findViewById(R.id.check_box);
            }
        }

        @Override
        public int getItemCount() {
            return locationList.size();
        }
    }

}
