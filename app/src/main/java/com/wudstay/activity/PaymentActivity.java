package com.wudstay.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.payu.india.Interfaces.PaymentRelatedDetailsListener;
import com.payu.india.Model.PaymentParams;
import com.payu.india.Model.PayuConfig;
import com.payu.india.Model.PayuHashes;
import com.payu.india.Model.PayuResponse;
import com.payu.india.Payu.Payu;
import com.payu.india.Payu.PayuConstants;
import com.payu.india.Payu.PayuErrors;
import com.payu.payuui.Activity.PayUBaseActivity;
import com.wudstay.R;
import com.wudstay.adapter.PaymentListAdapter;
import com.wudstay.model.ApiClient;
import com.wudstay.model.ApiInterface;
import com.wudstay.pojo.PayURequestDataPojo;
import com.wudstay.pojo.PayURequestParametersPojo;
import com.wudstay.pojo.PayUResponsePojo;
import com.wudstay.pojo.PayUVOPojo;
import com.wudstay.pojo.PaymentHistoryDataPojo;
import com.wudstay.pojo.PaymentHistoryPojo;
import com.wudstay.pojo.PayuHashsPojo;
import com.wudstay.util.ConnectionDetector;
import com.wudstay.util.PaymentActPayuGateaway;
import com.wudstay.util.WudStayConstants;
import com.wudstay.util.WudstayDialogs;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentActivity extends BaseActivity implements View.OnClickListener, PaymentRelatedDetailsListener {

    private PaymentActivity ctx = this;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private ConnectionDetector cd;
    private WudstayDialogs dialog;
    private ArrayList<PaymentHistoryDataPojo> paymentList;
    private String transaction_id = "";
    private String user_credaintials;
    private PaymentParams mPaymentParams;
    private PayuConfig payuConfig;
    private Intent intent;
    private LinearLayout no_payments_layout;

    private PayuResponse mPayuResponse;
    private PaymentActPayuGateaway payuGateway;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payments);
        setDrawerAndToolbar("Payments");
        WudStayConstants.ACTIVITIES.add(ctx);

        getURL();

        no_payments_layout = (LinearLayout) findViewById(R.id.no_payments_layout);
        recyclerView = (RecyclerView) findViewById(R.id.payments_recycler_view);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);
//        ImageView back = (ImageView) findViewById(R.id.back);

        cd = new ConnectionDetector(ctx);
        dialog = new WudstayDialogs(ctx);

//        back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//            }
//        });

        getPaymentHistory();
    }

    private void getPaymentHistory() {
        if (cd.isConnectingToInternet()) {
            final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

//            Call<PaymentHistoryPojo> call = apiService.getPaymentHistory(ctx.getFromPrefs(WudStayConstants.MOBILE));
            Call<PaymentHistoryPojo> call = apiService.getPaymentHistory("9601274648");
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<PaymentHistoryPojo>() {
                @Override
                public void onResponse(Call<PaymentHistoryPojo> call, Response<PaymentHistoryPojo> response) {
                    if (response.body().getResponseCode() == 1) {
                        paymentList = response.body().getResponseObject();
                        if (paymentList != null && paymentList.size() > 0) {
                            mAdapter = new PaymentListAdapter(ctx, paymentList);
                            recyclerView.setAdapter(mAdapter);
                            no_payments_layout.setVisibility(View.GONE);
                        } else {
                            no_payments_layout.setVisibility(View.VISIBLE);
                        }
                    } else {
                        no_payments_layout.setVisibility(View.VISIBLE);
                    }
                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<PaymentHistoryPojo> call, Throwable t) {
                    // Log error here since request failed
                    progressDialog.getlDialog().dismiss();
                }
            });
        } else {
            dialog.displayCommonDialog(WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }

    @Override
    public void onClick(View view) {

        Intent intent;
        int id = view.getId();
        if (id == R.id.paynowLL) {
            PaymentHistoryDataPojo data_payment_history = (PaymentHistoryDataPojo) view.getTag(R.string.key);
//            processPayment(getFromPrefs(WudStayConstants.NAME), getFromPrefs(WudStayConstants.EMAIL), getFromPrefs(WudStayConstants.MOBILE), "" + data_payment_history.getDueAmount(), data_payment_history.getInvoiceNumber(), data_payment_history.getOccupancyType(), data_payment_history.getPgId()+"", data_payment_history.getMoveInDate());
            getPayuRequestParameters( "" + data_payment_history.getDueAmount(), data_payment_history.getInvoiceNumber(), data_payment_history.getOccupancyType(), data_payment_history.getPgId()+"", data_payment_history.getMoveInDate());
        } /*else if (id == R.id.linear_layout_credit_debit_card) {
            if (payuGateway != null) {
                Dialog d = (Dialog) view.getTag();
                if (d != null) {
                    d.dismiss();
                }
                intent = new Intent(ctx, PayUCreditDebitCardActivity.class);
                intent.putParcelableArrayListExtra(PayuConstants.CREDITCARD, mPayuResponse.getCreditCard());
                intent.putParcelableArrayListExtra(PayuConstants.DEBITCARD, mPayuResponse.getDebitCard());
                payuGateway.launchActivity(intent);
            }
        } else if (id == R.id.linear_layout_netbanking) {// this button is inside Payonline dialog, just hadelling the diload button click here
            if (payuGateway != null) {
                Dialog d = (Dialog) view.getTag();
                if (d != null) {
                    d.dismiss();
                }
                intent = new Intent(ctx, PayUNetBankingActivity.class);
                intent.putParcelableArrayListExtra(PayuConstants.NETBANKING, mPayuResponse.getNetBanks());
                payuGateway.launchActivity(intent);
            }
        } else if (id == R.id.linear_layout_payumoney) {// this button is inside Payonline dialog, just hadelling the diload button click here
            if (payuGateway != null) {
                Dialog d = (Dialog) view.getTag();
                if (d != null) {
                    d.dismiss();
                }
                payuGateway.launchPayumoney();
            }
        }*/
    }

  /*  public void processPayment(String guestName, String guestEmail, String mobileNumber, String totalAmount, String invoiceNumber, String occupancyType, String pgId, String move_in_date) {
        String isMonthlyRent = "YES";
        String visitDate = "";
        if (cd.isConnectingToInternet()) {
            // first get payu data from payu sdk and then show the dialog
            payuGateway = new PaymentActPayuGateaway(ctx, pgId, totalAmount, occupancyType, visitDate, guestName, guestEmail, mobileNumber, isMonthlyRent, invoiceNumber, move_in_date);
            payuGateway.navigateToPayUSdk();
        } else {
            dialog.displayCommonDialog(WudStayConstants.NO_INTERNET_CONNECTED);
        }


    }*/

    public void getPayuRequestParameters(String totalAmount, String invoiceNumber, String occupancy_type, String pgId, String move_in_date) {
        String isMonthlyRent = "YES";
        if (cd.isConnectingToInternet()) {

            final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Call<PayURequestParametersPojo> call = apiService.PaymentPayuRequestParameters(getFromPrefs(WudStayConstants.NAME), getFromPrefs(WudStayConstants.EMAIL),
                    getFromPrefs(WudStayConstants.MOBILE), pgId, totalAmount,
                    WudStayConstants.SOURCE, occupancy_type, "", move_in_date, "44fZt5:"+getFromPrefs(WudStayConstants.MOBILE), isMonthlyRent, invoiceNumber);
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<PayURequestParametersPojo>() {
                @Override
                public void onResponse(Call<PayURequestParametersPojo> call, Response<PayURequestParametersPojo> response) {
                    if (response.body().getResponseCode() == 1) {
                        transaction_id = response.body().getResponseObject().getPayUVO().getTransactionId();
                        saveIntoPrefs("transaction_id", transaction_id);
                        navigateToBaseActivity(response.body().getResponseObject());
                    }
                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<PayURequestParametersPojo> call, Throwable t) {
                    // Log error here since request failed
                    System.out.println("retrofit hh failure" + t.getMessage());
                    progressDialog.getlDialog().dismiss();
                }
            });
        } else {
            dialog.displayCommonDialog(WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }

    private void navigateToBaseActivity(PayURequestDataPojo mPaymentParamsPojo) {
        intent = new Intent(this, PayUBaseActivity.class);
        mPaymentParams = new PaymentParams();
        payuConfig = new PayuConfig();

        PayUVOPojo payUVO = mPaymentParamsPojo.getPayUVO();
        PayuHashsPojo payUHashVO = mPaymentParamsPojo.getPayuHashs();

        mPaymentParams.setKey(payUVO.getMerchantKey());
        mPaymentParams.setAmount(payUVO.getAmount() + "");
        mPaymentParams.setProductInfo(payUVO.getProductInfo());
        mPaymentParams.setFirstName(payUVO.getFirstName());
        mPaymentParams.setEmail(payUVO.getEmail());
        mPaymentParams.setTxnId(payUVO.getTransactionId());
        mPaymentParams.setSurl(payUVO.getSurl());
        mPaymentParams.setFurl(payUVO.getFurl());
        mPaymentParams.setUdf1(payUVO.getUdf1());
        mPaymentParams.setUdf2("");
        mPaymentParams.setUdf3("");
        mPaymentParams.setUdf4("");
        mPaymentParams.setUdf5("");
        mPaymentParams.setUserCredentials(payUVO.getMerchantKey()+":"+getFromPrefs(WudStayConstants.MOBILE));

        payuConfig.setEnvironment(PayuConstants.PRODUCTION_ENV);

        PayuHashes payuHashes = new PayuHashes();
        payuHashes.setPaymentHash(payUHashVO.getPaymentHash());
        payuHashes.setMerchantIbiboCodesHash(payUHashVO.getMerchantCodesHash());
        payuHashes.setPaymentRelatedDetailsForMobileSdkHash(payUHashVO.getDetailsForMobileSdk());
        payuHashes.setVasForMobileSdkHash(payUHashVO.getMobileSdk());
        payuHashes.setDeleteCardHash(payUHashVO.getDeleteHash());
        payuHashes.setEditCardHash(payUHashVO.getEditUserCardHash());
        payuHashes.setSaveCardHash(payUHashVO.getSaveUserCardHash());

        launchSdkUI(payuHashes);
    }

    public void launchSdkUI(PayuHashes payuHashes) {

        mPaymentParams.setHash(payuHashes.getPaymentHash());

        intent.putExtra(PayuConstants.PAYU_CONFIG, payuConfig);
        intent.putExtra(PayuConstants.PAYMENT_PARAMS, mPaymentParams);
        intent.putExtra(PayuConstants.PAYU_HASHES, payuHashes);

        intent.putExtra(PayuConstants.ENV, PayuConstants.PRODUCTION_ENV);

        Payu.setInstance(this);

        startActivityForResult(intent, PayuConstants.PAYU_REQUEST_CODE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PayuConstants.PAYU_REQUEST_CODE) {
            if (data != null) {
                if (resultCode == RESULT_OK) {
                    if (cd.isConnectingToInternet()) {
                        getIosPayuResponse();
                    } else {
                        dialog.displayCommonDialog(WudStayConstants.NO_INTERNET_CONNECTED);
                    }
                } else {

                    if (!data.getStringExtra("result").contains("status=failure")) {
                        new AlertDialog.Builder(this)
                                .setCancelable(false)
                                .setMessage(data.getStringExtra("result"))
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {

                                    }
                                }).show();
                    } else {
                        new AlertDialog.Builder(this)
                                .setCancelable(false)
                                .setMessage("Transaction failed, please try again")
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {

                                    }
                                }).show();
                    }
                }

            } else {
//                Toast.makeText(this, "Could not receive data", Toast.LENGTH_LONG).show();
            }
        }
    }


    private void getIosPayuResponse() {
        final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
        progressDialog.showProgressDialog(ctx);
        progressDialog.getlDialog().setCancelable(Boolean.FALSE);
        final WudstayDialogs logout_dialogs = new WudstayDialogs(ctx);

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<PayUResponsePojo> call = apiService.getPayuResponse(getFromPrefs("transaction_id"));
        System.out.println("retrofit URL " + call.request());
        call.enqueue(new Callback<PayUResponsePojo>() {
            @Override
            public void onResponse(Call<PayUResponsePojo> call, Response<PayUResponsePojo> response) {
                if (response.body().getResponseCode() == 1) {

                    if (response.body().getResponseObject() != null) {
                        if(response.body().getResponseObject().getPaymentStatus().equalsIgnoreCase("success")) {
                            Toast.makeText(ctx, "Payment Successful", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(ctx, HomeActivity.class);
                            startActivity(intent);
                            finish();
                        }
                        else if(response.body().getResponseObject().getPaymentStatus().equalsIgnoreCase("waiting")){
                            Toast.makeText(ctx, "Your transaction is under process", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            logout_dialogs.displayCommonDialog("Payment Failed");
                        }
                    } else {
                        Toast.makeText(ctx, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
                progressDialog.getlDialog().dismiss();
            }

            @Override
            public void onFailure(Call<PayUResponsePojo> call, Throwable t) {
                // Log error here since request failed
                progressDialog.getlDialog().dismiss();
            }
        });
    }


    private void showPayonlineDialog() {
        final WudstayDialogs d = new WudstayDialogs(ctx);
        d.showPaymentModeDialog(this, mPayuResponse);
    }

    @Override
    public void onPaymentRelatedDetailsResponse(PayuResponse payuResponse) {
        mPayuResponse = payuResponse;
//        payuGateway.progressDialog.closeDialog();
        if (payuResponse.isResponseAvailable() && payuResponse.getResponseStatus().getCode() == PayuErrors.NO_ERROR) { // ok we are good to go
            showPayonlineDialog();
        } else {
            Toast.makeText(this, "Something went wrong : " + payuResponse.getResponseStatus().getResult(), Toast.LENGTH_LONG).show();
        }

    }
}
