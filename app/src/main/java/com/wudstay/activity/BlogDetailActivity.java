package com.wudstay.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.wudstay.R;
import com.wudstay.pojo.FeedsPojo;
import com.wudstay.util.WudstayDialogs;

public class BlogDetailActivity extends AppCompatActivity {

    private WebView webview;
    private boolean isLoadingVisible = false;
    private BlogDetailActivity ctx = this;

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog_detail);

        webview = (WebView) findViewById(R.id.webview);

        final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
        progressDialog.showProgressDialog(ctx);

        webview.setWebViewClient(new MyWebViewClient());
        webview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress >= 85) {
                    if (progressDialog.getlDialog() != null && progressDialog.getlDialog().isShowing()) {
                        progressDialog.getlDialog().dismiss();
                    }
                } else {
                    if (!isLoadingVisible)
                        progressDialog.getlDialog().show();
                    isLoadingVisible = true;
                }
            }
        });

        webview.getSettings().setJavaScriptEnabled(true);
        if(getIntent().getStringExtra("from") != null && getIntent().getStringExtra("from").equals("notification"))
        {
            openURL(getIntent().getStringExtra("url"));
        }
        else
        {
            FeedsPojo obj = (FeedsPojo) getIntent().getSerializableExtra("blogData");
            openURL(obj.getLink());
        }
    }

    private void openURL(String url) {
        webview.loadUrl(url);
        webview.requestFocus();
    }
}
