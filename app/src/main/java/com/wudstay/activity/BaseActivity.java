package com.wudstay.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.wudstay.R;
import com.wudstay.fragments.DrawerFragment;
import com.wudstay.model.ApiClient;
import com.wudstay.model.ApiInterface;
import com.wudstay.pojo.FavoriteResponsePojo;
import com.wudstay.pojo.MobileNoVerificationPojo;
import com.wudstay.pojo.PGDetailNewPojo;
import com.wudstay.pojo.PgDetailPojo;
import com.wudstay.util.ConnectionDetector;
import com.wudstay.util.WudStayConstants;
import com.wudstay.util.WudstayDialogs;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Administrator on 22-Aug-16.
 */
public class BaseActivity extends AppCompatActivity {

    public DrawerLayout mDrawerLayout;
    private RelativeLayout notification_layout;
    private String mobileNumber;
    private String guestName;
    private String guestEmail;
    private Dialog otpDialog;
    private ConnectionDetector cd;
    private WudstayDialogs dialog;
    private String from = "";
    private Activity ctx;
    public EditText otpEditText;
    private boolean closeActivity = true;
    private SharedPreferences sharedPreferences;
    private FavoriteResponsePojo result = null;
    private String user_type = "wudstay";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    }

    public Toolbar setDrawerAndToolbar(String name) {
        Toolbar appbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(appbar);
        getSupportActionBar().setTitle("");
        final LinearLayout drawer_button = (LinearLayout) findViewById(R.id.drawerButton);
        notification_layout = (RelativeLayout) findViewById(R.id.notification_layout);
        TextView notification_quantity = (TextView) findViewById(R.id.notification_quantity);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();
                if (mDrawerLayout != null && !mDrawerLayout.isDrawerOpen(Gravity.RIGHT))
                    mDrawerLayout.openDrawer(Gravity.RIGHT);
                else if (mDrawerLayout != null)
                    mDrawerLayout.closeDrawers();
            }
        });
        DrawerFragment fragment = (DrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_drawer);
        fragment.setUp(mDrawerLayout);
        return appbar;
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void setImageInLayout(Context ctx, int width, int height, String url, ImageView image) {
        Picasso.with(ctx).load(url).resize(width, height).placeholder(R.mipmap.logo).error(R.mipmap.logo).into(image);
    }

    public void setProfileImageInLayout(Context ctx, int width, int height, String url, ImageView image) {
        Picasso.with(ctx).load(url).placeholder(R.mipmap.logo).error(R.mipmap.logo).into(image);
//        Picasso.with(ctx).load(url).fit().placeholder(R.mipmap.logo).error(R.mipmap.logo).centerCrop().into(image);
    }

    public void setGridViewHeightBasedOnChildren(GridView gridView, int columns) {
        ListAdapter listAdapter = gridView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        int items = listAdapter.getCount();
        int rows = 0;

        View listItem = listAdapter.getView(0, null, gridView);
        listItem.measure(0, 0);
        totalHeight = listItem.getMeasuredHeight();

        float x = 1;
        if (items > columns) {
            x = items / columns;
            rows = (int) (x + 1);
            totalHeight *= rows;
        }

        ViewGroup.LayoutParams params = gridView.getLayoutParams();
        params.height = totalHeight;
        gridView.setLayoutParams(params);

    }

    public String getAddress(String address) {
        if (address != null) {
            String add[] = address.split(",");
            if (add.length > 1)
                address = add[0] + ", " + add[1];
            if (add.length >= 3)
                address += "," + "\n" + add[2].trim();
            if (add.length >= 4)
                address += "," + "\n" + add[3].trim();
        }
        return address;
    }

    public String getDayOfMonthSuffix(final int n) {
        if (n >= 11 && n <= 13) {
            return "th";
        }
        switch (n % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    /*public String getNextSaturday() {
        String returning_str = "";
        Calendar checkIn_cal = Calendar.getInstance();
        DateFormat checkin_df_day = new SimpleDateFormat("EEE", Locale.ENGLISH);
        String today_day = checkin_df_day.format(checkIn_cal.getTime());
        checkIn_cal.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
        checkIn_cal.set(Calendar.HOUR_OF_DAY, 0);
        checkIn_cal.set(Calendar.MINUTE, 0);
        checkIn_cal.set(Calendar.SECOND, 0);
//        DateFormat df=new SimpleDateFormat("EEE yyyy/MM/dd HH:mm:ss");
        DateFormat checkin_df = new SimpleDateFormat("dd/MMMM/yyyy", Locale.ENGLISH);

        if (today_day.equals("Sun")) {
            checkIn_cal.add(Calendar.DATE, 7);
            returning_str = checkin_df.format(checkIn_cal.getTime());
        } else {
            returning_str = checkin_df.format(checkIn_cal.getTime());
        }

        return returning_str;
    }

    public String getNextSunday() {
//        Calendar c=Calendar.getInstance();
//        c.set(Calendar.DAY_OF_WEEK,Calendar.SUNDAY);
//        c.set(Calendar.HOUR_OF_DAY,0);
//        c.set(Calendar.MINUTE,0);
//        c.set(Calendar.SECOND,0);
//        DateFormat df=new SimpleDateFormat("EEE yyyy/MM/dd HH:mm:ss");
//        c.add(Calendar.DATE,7);
//        c.add(Calendar.DATE,7);

        String returning_str = "";
        String manufactures = android.os.Build.MANUFACTURER;
        if (manufactures.equals("samsung")) {
            Calendar checkIn_cal = Calendar.getInstance();
            Calendar today = Calendar.getInstance();
            DateFormat checkin_df_day = new SimpleDateFormat("EEE", Locale.ENGLISH);

            String today_day = checkin_df_day.format(checkIn_cal.getTime());
            checkIn_cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
            if (checkIn_cal.getTimeInMillis() > today.getTimeInMillis()) {
                checkIn_cal.set(Calendar.WEEK_OF_MONTH, today.get(Calendar.WEEK_OF_MONTH) - 1);
            }
            checkIn_cal.set(Calendar.HOUR_OF_DAY, 0);
            checkIn_cal.set(Calendar.MINUTE, 0);
            checkIn_cal.set(Calendar.SECOND, 0);
//        DateFormat df=new SimpleDateFormat("EEE yyyy/MM/dd HH:mm:ss");
            DateFormat checkin_df = new SimpleDateFormat("dd/MMMM/yyyy", Locale.ENGLISH);

            if (today_day.equals("Sun")) {
                checkIn_cal.add(Calendar.DATE, 14);
                returning_str = checkin_df.format(checkIn_cal.getTime());
            } else {
                checkIn_cal.add(Calendar.DATE, 7);
                returning_str = checkin_df.format(checkIn_cal.getTime());
            }
        } else {
            Calendar checkIn_cal = Calendar.getInstance();
            DateFormat checkin_df_day = new SimpleDateFormat("EEE", Locale.ENGLISH);
            String today_day = checkin_df_day.format(checkIn_cal.getTime());
            checkIn_cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
            checkIn_cal.set(Calendar.HOUR_OF_DAY, 0);
            checkIn_cal.set(Calendar.MINUTE, 0);
            checkIn_cal.set(Calendar.SECOND, 0);
//        DateFormat df=new SimpleDateFormat("EEE yyyy/MM/dd HH:mm:ss");
            DateFormat checkin_df = new SimpleDateFormat("dd/MMMM/yyyy", Locale.ENGLISH);

            if (today_day.equals("Sun")) {
                checkIn_cal.add(Calendar.DATE, 7);
                returning_str = checkin_df.format(checkIn_cal.getTime());
            } else {
//                checkIn_cal.add(Calendar.DATE, 7);
                returning_str = checkin_df.format(checkIn_cal.getTime());
            }
        }
        return returning_str;
    }*/

    public String getNextSaturday() {
        String returning_str = "";

        String manufactures = android.os.Build.MANUFACTURER;

        if (manufactures.equals("samsung")) {
            // Samsung phone
            Calendar checkIn_cal = Calendar.getInstance();
            DateFormat checkin_df_day = new SimpleDateFormat("EEE");
            String today_day = checkin_df_day.format(checkIn_cal.getTime());
            checkIn_cal.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
            checkIn_cal.set(Calendar.HOUR_OF_DAY, 0);
            checkIn_cal.set(Calendar.MINUTE, 0);
            checkIn_cal.set(Calendar.SECOND, 0);
//        DateFormat df=new SimpleDateFormat("EEE yyyy/MM/dd HH:mm:ss");
            DateFormat checkin_df = new SimpleDateFormat("dd/MMMM/yyyy");

            if (today_day.equals("Sun")) {
                checkIn_cal.add(Calendar.DATE, 7);
                returning_str = checkin_df.format(checkIn_cal.getTime());
            }
            if (today_day.equals("Sat")) {
                checkIn_cal.add(Calendar.DATE, 0);
                returning_str = checkin_df.format(checkIn_cal.getTime());
            } else {
                checkIn_cal.add(Calendar.DATE, 0);
                returning_str = checkin_df.format(checkIn_cal.getTime());
            }

        } else {
            // others phone
            Calendar checkIn_cal = Calendar.getInstance();
            DateFormat checkin_df_day = new SimpleDateFormat("EEE");
            String today_day = checkin_df_day.format(checkIn_cal.getTime());
            checkIn_cal.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
            checkIn_cal.set(Calendar.HOUR_OF_DAY, 0);
            checkIn_cal.set(Calendar.MINUTE, 0);
            checkIn_cal.set(Calendar.SECOND, 0);
//        DateFormat df=new SimpleDateFormat("EEE yyyy/MM/dd HH:mm:ss");
            DateFormat checkin_df = new SimpleDateFormat("dd/MMMM/yyyy");

            if (today_day.equals("Sun")) {
//            checkIn_cal.add(Calendar.DATE, 7);
                returning_str = checkin_df.format(checkIn_cal.getTime());
            } else {
                returning_str = checkin_df.format(checkIn_cal.getTime());
            }

        }

        return returning_str;
    }

    public String getNextSunday() {
        String returning_str = "";
        String manufactures = android.os.Build.MANUFACTURER;

        if (manufactures.equals("samsung")) {
            // Samsung phone
            Calendar checkIn_cal = Calendar.getInstance();
            Calendar today = Calendar.getInstance();
            DateFormat checkin_df_day = new SimpleDateFormat("EEE");

            String today_day = checkin_df_day.format(checkIn_cal.getTime());
            checkIn_cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
            if (checkIn_cal.getTimeInMillis() > today.getTimeInMillis()) {
                checkIn_cal.set(Calendar.WEEK_OF_MONTH, today.get(Calendar.WEEK_OF_MONTH) - 1);
            }
            checkIn_cal.set(Calendar.HOUR_OF_DAY, 0);
            checkIn_cal.set(Calendar.MINUTE, 0);
            checkIn_cal.set(Calendar.SECOND, 0);
//        DateFormat df=new SimpleDateFormat("EEE yyyy/MM/dd HH:mm:ss");
            DateFormat checkin_df = new SimpleDateFormat("dd/MMMM/yyyy");

            if (today_day.equals("Sun")) {
                checkIn_cal.add(Calendar.DATE, 0);
                returning_str = checkin_df.format(checkIn_cal.getTime());
            }
            if (today_day.equals("Sat")) {
                checkIn_cal.add(Calendar.DATE, 7);
                returning_str = checkin_df.format(checkIn_cal.getTime());
            } else {
                checkIn_cal.add(Calendar.DATE, 7);
                returning_str = checkin_df.format(checkIn_cal.getTime());
            }
        } else {
            // others phone
            Calendar checkIn_cal = Calendar.getInstance();
            DateFormat checkin_df_day = new SimpleDateFormat("EEE");
            String today_day = checkin_df_day.format(checkIn_cal.getTime());
            checkIn_cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
            checkIn_cal.set(Calendar.HOUR_OF_DAY, 0);
            checkIn_cal.set(Calendar.MINUTE, 0);
            checkIn_cal.set(Calendar.SECOND, 0);
//        DateFormat df=new SimpleDateFormat("EEE yyyy/MM/dd HH:mm:ss");
            DateFormat checkin_df = new SimpleDateFormat("dd/MMMM/yyyy");

            if (today_day.equals("Sun")) {
                checkIn_cal.add(Calendar.DATE, 7);
                returning_str = checkin_df.format(checkIn_cal.getTime());
            } else {
                checkIn_cal.add(Calendar.DATE, 7);
                returning_str = checkin_df.format(checkIn_cal.getTime());
            }

        }


        return returning_str;
    }

    public String getMonth(String month) {
        SimpleDateFormat inputFormat = new SimpleDateFormat("MMMM");
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(inputFormat.parse(month));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat outputFormat = new SimpleDateFormat("MM");
        return outputFormat.format(cal.getTime());
    }

    public SimpleDateFormat getDateFormat() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        return sdf;
    }

    public String getTodayDate() {
        String date = "";
        Calendar myCalendar = Calendar.getInstance();
        SimpleDateFormat sdf = getDateFormat();
        date = sdf.format(myCalendar.getTime());

        return date;
    }

    public String getTomorrowDate() {
        String date = "";
        Calendar myCalendar = Calendar.getInstance();
        myCalendar.add(Calendar.DATE, 1);
        SimpleDateFormat sdf = getDateFormat();
        date = sdf.format(myCalendar.getTime());

        return date;
    }

    // change layout of image view layout when layout is clicked
    public void setDarkColor(LinearLayout layout, TextView text, ImageView imgView, int resId) {
        layout.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.default_done_button));
        text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
        imgView.setImageResource(resId);
    }

    public void setLightColor(LinearLayout layout, TextView text, ImageView imgView, int resId) {
        layout.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.bg_light_grey));
        text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.hint_color));
        imgView.setImageResource(resId);
    }

    // change layout of date picker layout when layout is clicked
    public void setDarkColorLayout(LinearLayout layout, TextView text) {
        layout.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.default_done_button));
        text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
    }

    public void setLightColorLayout(LinearLayout layout, TextView text) {
        layout.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.bg_light_grey));
        text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.hint_color));
    }

    public void saveIntoPrefs(String key, String value) {
        SharedPreferences prefs = getSharedPreferences(WudStayConstants.PREF_NAME, MODE_PRIVATE);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putString(key, value);
        edit.commit();
    }

    public String getFromPrefs(String key) {
        SharedPreferences prefs = getSharedPreferences(WudStayConstants.PREF_NAME, MODE_PRIVATE);
        return prefs.getString(key, WudStayConstants.DEFAULT_VALUE);
    }

    public int getScreenHeight() {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;

        return height;
    }


    // dynamically permission added

    /**
     * method that will return whether the permission is accepted. By default it is true if the user is using a device below
     * version 23
     *
     * @param permission
     * @return
     */

    public boolean hasPermission(String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (canMakeSmores()) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    /**
     * method to determine whether we have asked
     * for this permission before.. if we have, we do not want to ask again.
     * They either rejected us or later removed the permission.
     *
     * @param permission
     * @return
     */
    public boolean shouldWeAsk(String permission) {
        return (sharedPreferences.getBoolean(permission, true));
    }

    /**
     * we will save that we have already asked the user
     *
     * @param permission
     */
    public void markAsAsked(String permission, SharedPreferences sharedPreferences) {
        sharedPreferences.edit().putBoolean(permission, false).apply();
    }

    /**
     * We may want to ask the user again at their request.. Let's clear the
     * marked as seen preference for that permission.
     *
     * @param permission
     */
    public void clearMarkAsAsked(String permission) {
        sharedPreferences.edit().putBoolean(permission, true).apply();
    }


    /**
     * This method is used to determine the permissions we do not have accepted yet and ones that we have not already
     * bugged the user about.  This comes in handle when you are asking for multiple permissions at once.
     *
     * @param wanted
     * @return
     */
    public ArrayList<String> findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    /**
     * this will return us all the permissions we have previously asked for but
     * currently do not have permission to use. This may be because they declined us
     * or later revoked our permission. This becomes useful when you want to tell the user
     * what permissions they declined and why they cannot use a feature.
     *
     * @param wanted
     * @return
     */
    public ArrayList<String> findRejectedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(perm) && !shouldWeAsk(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    /**
     * Just a check to see if we have marshmallows (version 23)
     *
     * @return
     */
    public boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    /**
     * a method that will centralize the showing of a snackbar
     */
    public void makePostRequestSnack(String message, int size) {

        Toast.makeText(getApplicationContext(), String.valueOf(size) + " " + message, Toast.LENGTH_SHORT).show();

        finish();
    }

    public boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public void openSoftKeyBoard(EditText et) {
        et.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(et, InputMethodManager.SHOW_IMPLICIT);
    }

    public void getMobileVerificationCode(String name, String email, final String phoneNo, String from, Activity ctx) {
        guestName = name;
        mobileNumber = phoneNo;
        guestEmail = email;
        this.from = from;
        this.ctx = ctx;
        cd = new ConnectionDetector(getApplicationContext());
        dialog = new WudstayDialogs(BaseActivity.this);
        if (otpDialog == null)
            otpDialog = new Dialog(BaseActivity.this, R.style.Theme_Dialog_Custom_Anim);
        if (cd.isConnectingToInternet()) {

            getURL();

            final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Call<MobileNoVerificationPojo> call = apiService.getMobileNoVerification(name, email, phoneNo);
            call.enqueue(new Callback<MobileNoVerificationPojo>() {
                @Override
                public void onResponse(Call<MobileNoVerificationPojo> call, Response<MobileNoVerificationPojo> response) {
                    if (response.body() != null && response.body().getResponseCode() == 1) {
                        if (response.body().isResponseObject() && !otpDialog.isShowing()) {
                            showEnterOTPDialog();
                        }

                    }
                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<MobileNoVerificationPojo> call, Throwable t) {
                    // Log error here since request failed
                    progressDialog.getlDialog().dismiss();
                }
            });
        } else {
            dialog.displayCommonDialog(WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }

    private void showEnterOTPDialog() {
        otpDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        otpDialog.setContentView(R.layout.dialog_enter_otp);
        ImageView cancelDialog = (ImageView) otpDialog.findViewById(R.id.imgClose);
        final TextView tvCounter = (TextView) otpDialog.findViewById(R.id.tvVeifyingSec);
        final EditText edMobileNo = (EditText) otpDialog.findViewById(R.id.edNumber);
        otpEditText = (EditText) otpDialog.findViewById(R.id.edOTP);

        final CounterClass timer = new CounterClass(30 * 1000, 1000, tvCounter, otpEditText, edMobileNo);

        otpEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (!otpEditText.getText().toString().trim().isEmpty()) {
                    if (actionId == EditorInfo.IME_ACTION_GO || event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                        submitOTP(otpEditText);
                        return true;
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please enter OTP", Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });

        otpDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                otpDialog = null;
                if (timer != null)
                    timer.cancel();
                if (closeActivity) {
                    if (!from.equals("ScheduleVisitActivity") && !from.equals("BookingConfirmationActivity")) {
                        RegistrationActivity act = (RegistrationActivity) ctx;
                        act.finishActivity();
                    }
                }
            }
        });

        TextView tvEditMobile = (TextView) otpDialog.findViewById(R.id.tvEdit);
        final TextView tvResend = (TextView) otpDialog.findViewById(R.id.tvResend);
        edMobileNo.setText(mobileNumber + "");
        edMobileNo.setEnabled(false);

        tvEditMobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer.cancel();
                edMobileNo.setClickable(true);
                edMobileNo.setFocusable(true);
                edMobileNo.setEnabled(true);
                edMobileNo.requestFocus();
                edMobileNo.setSelection(edMobileNo.getText().length());
            }
        });

        tvResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String no = edMobileNo.getText().toString().trim();
                otpEditText.setText("");
                if (no.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Please enter Mobile", Toast.LENGTH_SHORT).show();
                } else if (no.length() < 10) {
                    Toast.makeText(getApplicationContext(), "Please enter valid Mobile", Toast.LENGTH_SHORT).show();
                } else {
                    mobileNumber = no;
                    WudstayDialogs progressDialog = new WudstayDialogs(ctx);
                    progressDialog.checkIsRegisteredUser(guestName, guestEmail, mobileNumber, from, ctx);
                    timer.cancel();
                    timer.start();
                }
            }
        });

        cancelDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                otpDialog.dismiss();
            }
        });

        timer.start();
        otpDialog.show();
    }

    public void submitOTP(EditText edOTP) {
        String otp = edOTP.getText().toString().trim();
        if (cd == null)
            cd = new ConnectionDetector(getApplicationContext());
        if (dialog == null)
            dialog = new WudstayDialogs(BaseActivity.this);
        hideSoftKeyboard();
        if (!otp.isEmpty()) {
            if (cd.isConnectingToInternet()) {
                final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
                progressDialog.showProgressDialog(ctx);
                progressDialog.getlDialog().setCancelable(Boolean.FALSE);

                ApiInterface apiService =
                        ApiClient.getClient().create(ApiInterface.class);

                Call<MobileNoVerificationPojo> call = apiService.getSendOTP(mobileNumber, otp);
                call.enqueue(new Callback<MobileNoVerificationPojo>() {
                    @Override
                    public void onResponse(Call<MobileNoVerificationPojo> call, Response<MobileNoVerificationPojo> response) {
                        System.out.println("retrofit URL " + call.request());
                        if (response.body().getResponseCode() == 1) {
                            if (response.body().isResponseObject()) {
                                closeActivity = false;
                                saveIntoPrefs(WudStayConstants.NAME, guestName);
                                saveIntoPrefs(WudStayConstants.EMAIL, guestEmail);
                                saveIntoPrefs(WudStayConstants.MOBILE, mobileNumber);

                                if (otpDialog != null)
                                    otpDialog.dismiss();
                                if (from.equals("ScheduleVisitActivity")) {
                                    ScheduleVisitActivity acti_schedule = (ScheduleVisitActivity) ctx;
                                    acti_schedule.scheduleVisit();
                                } else if (from.equals("ScheduleVisitActivity")) {
                                    ScheduleVisitActivity acti_schedule = (ScheduleVisitActivity) ctx;
                                    acti_schedule.scheduleVisit();
                                } else if (from.equals("BookingConfirmationActivity")) {
                                    BookingConfirmationActivity acti = (BookingConfirmationActivity) ctx;
                                    acti.getPayuRequestParameters();
//                                    acti.processPayment();
                                } else if (from.equals("BookingConfirmationActivity")) {
                                    BookingConfirmationActivity acti = (BookingConfirmationActivity) ctx;
                                    acti.getPayuRequestParameters();
//                                    acti.processPayment();
                                } else if (from.equals("MyAcc")) {
                                    RegistrationActivity acti = (RegistrationActivity) ctx;
                                    acti.NavigateToProfileActivity();
                                } else if (from.equals("PaymentActivity")) {
                                    RegistrationActivity acti = (RegistrationActivity) ctx;
                                    acti.navigateToPayments();
                                } else if (from.equals("ComplaintsActivity")) {
                                    RegistrationActivity acti = (RegistrationActivity) ctx;
                                    acti.navigateToComplaints();
                                } else if (from.equals("FavouritesActivity")) {
                                    RegistrationActivity acti = (RegistrationActivity) ctx;
                                    acti.navigateTofavourites();
                                } else if (from.equals("Favourite")) {
                                    RegistrationActivity acti = (RegistrationActivity) ctx;
                                    acti.finish();
                                }
                            }
                        } else {
                            dialog.displayCommonDialog(WudStayConstants.WRONG_OTP_MSG);
//                            Toast.makeText(ctx, "Please enter the correct OTP", Toast.LENGTH_LONG).show();
                        }
                        if (progressDialog.getlDialog() != null && progressDialog.getlDialog().isShowing())
                            progressDialog.getlDialog().dismiss();
                    }

                    @Override
                    public void onFailure(Call<MobileNoVerificationPojo> call, Throwable t) {
                        // Log error here since request failed
                        System.out.println("hh failure: " + t.getMessage());
                        if (progressDialog.getlDialog() != null && progressDialog.getlDialog().isShowing())
                            progressDialog.getlDialog().dismiss();
                    }
                });
            } else {
                dialog.displayCommonDialog(WudStayConstants.NO_INTERNET_CONNECTED);
            }
        }
    }

    public class CounterClass extends CountDownTimer {

        TextView tvCounter;
        private EditText otp_edit, edNumber;

        public CounterClass(long millisInFuture, long countDownInterval, TextView tv, EditText edOTP, EditText edNumber) {
            super(millisInFuture, countDownInterval);
            otp_edit = edOTP;
            this.edNumber = edNumber;
            tvCounter = tv;
        }

        @Override
        public void onTick(long millisUntilFinished) {
            long millis = millisUntilFinished;
            if (TimeUnit.MILLISECONDS.toSeconds(millis) != 1) {
                tvCounter.setText("Verifying in " + TimeUnit.MILLISECONDS.toSeconds(millis) + " Sec");
            } else {
                tvCounter.setVisibility(View.GONE);
                if (otpDialog != null) {
                    TextView tvmsg = (TextView) otpDialog.findViewById(R.id.tvMsg);
                    tvmsg.setVisibility(View.VISIBLE);
                }
                Button btnSubmit = (Button) otpDialog.findViewById(R.id.btnSubmit);
                btnSubmit.setVisibility(View.VISIBLE);
                btnSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (edNumber.getText().toString().trim().isEmpty()) {
                            Toast.makeText(ctx, "Enter Mobile", Toast.LENGTH_SHORT).show();
                        } else if (edNumber.length() < 10) {
                            Toast.makeText(ctx, "Please enter valid Mobile", Toast.LENGTH_SHORT).show();
                        } else if (otp_edit.getText().toString().trim().isEmpty()) {
                            Toast.makeText(ctx, "Please enter OTP", Toast.LENGTH_SHORT).show();
                        } else {
                            submitOTP(otp_edit);
                        }
                    }
                });
            }
        }

        @Override
        public void onFinish() {
//			tvTimer.setText("Completed.");
        }
    }

    public int getImage(int linkedId) {
        if (linkedId == 11 || linkedId == 1)
            return R.mipmap.ac_black;
        else if (linkedId == 12 || linkedId == 2)
            return R.mipmap.wifi_black;
        else if (linkedId == 13)
            return R.mipmap.housekeeping_black;
        else if (linkedId == 14)
            return R.mipmap.food_black;
        else if (linkedId == 15 || linkedId == 3)
            return R.mipmap.tv_black;
        else if (linkedId == 16)
            return R.mipmap.support_black;
        else if (linkedId == 17 || linkedId == 6)
            return R.mipmap.parking_black;
        else if (linkedId == 18 || linkedId == 8)
            return R.mipmap.card_black;
        else if (linkedId == 19 || linkedId == 5)
            return R.mipmap.laundry_black;
        else if (linkedId == 20 || linkedId == 4)
            return R.mipmap.gyser_black;
        else if (linkedId == 21 || linkedId == 7)
            return R.mipmap.fridge_black;
        else if (linkedId == 22)
            return R.mipmap.elevator_black;
        else if (linkedId == 23)
            return R.mipmap.cctv_black;
        else if (linkedId == 24)
            return R.mipmap.power_backup_black;
        else if (linkedId == 25)
            return R.mipmap.dryer_black;
        else if (linkedId == 26)
            return R.mipmap.safe_black;
        else
            return R.mipmap.app_icon;
    }

    public void getURL() {
        if (getFromPrefs(WudStayConstants.USER_TYPE).equalsIgnoreCase("TMS"))
            WudStayConstants.BASE_URL = WudStayConstants.BASE_URL_TMS;
        else
            WudStayConstants.BASE_URL = WudStayConstants.BASE_URL_WUDSTAY;

        ApiClient.changeApiBaseUrl(WudStayConstants.BASE_URL);
    }

    public void getWudstayURL() {
        WudStayConstants.BASE_URL = WudStayConstants.BASE_URL_WUDSTAY;
        ApiClient.changeApiBaseUrl(WudStayConstants.BASE_URL);
    }

    public static void launchMarket(Activity activity) {
        Uri uri = Uri.parse("market://details?id=" + activity.getPackageName());
        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            activity.startActivity(myAppLinkToMarket);
        } catch (ActivityNotFoundException e) {

            Toast.makeText(activity, WudStayConstants.APP_NOT_AVAILABLE, Toast.LENGTH_SHORT).show();
        }
    }

    public void AddRemoveFavorite(PgDetailPojo detail, final BookmarkCallback callback) {

        if (getFromPrefs("mobile") == null || getFromPrefs("mobile").equals("")) {
            Intent intent = new Intent(BaseActivity.this, RegistrationActivity.class);
            intent.putExtra("navigate_from", "Favourite");
            startActivity(intent);
        } else {

            AddRemoveFavoriteHeart(detail, callback);
        }

    }

    public void AddRemoveFavoriteHeart(PgDetailPojo detail, final BookmarkCallback callback) {
        cd = new ConnectionDetector(getApplicationContext());
        dialog = new WudstayDialogs(BaseActivity.this);
        if (cd.isConnectingToInternet()) {
            getWudstayURL();
            final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            String remove;
            if (detail.isFavouritePg())
                remove = "YES";
            else
                remove = "NO";
            Call<FavoriteResponsePojo> call = apiService.updateFavorite(getFromPrefs(WudStayConstants.MOBILE), detail.getId(), remove);
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<FavoriteResponsePojo>() {

                @Override
                public void onResponse(Call<FavoriteResponsePojo> call, Response<FavoriteResponsePojo> response) {
                    if (response.body().getResponseCode() == 1) {
                        result = response.body();
                        callback.onSuccess(response.body());
                    } else {
                        dialog.displayCommonDialog(response.body().getMessage());
                    }
                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<FavoriteResponsePojo> call, Throwable t) {
                    // Log error here since request failed
                    System.out.println("retrofit hh failure " + t.getMessage());
                    callback.onError();
                    progressDialog.getlDialog().dismiss();
                }
            });
        } else {
            dialog.displayCommonDialog(WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }

    public void AddRemoveFavoriteNew(PGDetailNewPojo detail, final BookmarkCallback callback) {

        if (getFromPrefs("mobile") == null || getFromPrefs("mobile").equals("")) {
            Intent intent = new Intent(BaseActivity.this, RegistrationActivity.class);
            intent.putExtra("navigate_from", "Favourite");
            startActivity(intent);
        } else {
            AddRemoveFavoriteHeartNew(detail, callback);
        }

    }

    public void AddRemoveFavoriteHeartNew(PGDetailNewPojo detail, final BookmarkCallback callback) {
        cd = new ConnectionDetector(getApplicationContext());
        dialog = new WudstayDialogs(BaseActivity.this);
        if (cd.isConnectingToInternet()) {
            getWudstayURL();
            final WudstayDialogs progressDialog = new WudstayDialogs(BaseActivity.this);
            progressDialog.showProgressDialog(BaseActivity.this);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            String remove;
            if (detail.isFavouritePg())
                remove = "YES";
            else
                remove = "NO";
            Call<FavoriteResponsePojo> call = apiService.updateFavorite(getFromPrefs(WudStayConstants.MOBILE), "" + detail.getId(), remove);
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<FavoriteResponsePojo>() {

                @Override
                public void onResponse(Call<FavoriteResponsePojo> call, Response<FavoriteResponsePojo> response) {
                    if (response.body().getResponseCode() == 1) {
                        result = response.body();
                        callback.onSuccess(response.body());
                    } else {
                        dialog.displayCommonDialog(response.body().getMessage());
                    }
                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<FavoriteResponsePojo> call, Throwable t) {
                    // Log error here since request failed
                    System.out.println("retrofit hh failure " + t.getMessage());
                    callback.onError();
                    progressDialog.getlDialog().dismiss();
                }
            });
        } else {
            dialog.displayCommonDialog(WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }
}