package com.wudstay.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wudstay.R;
import com.wudstay.adapter.CustomAutoCompleteAdapter;
import com.wudstay.model.ApiClient;
import com.wudstay.model.ApiInterface;
import com.wudstay.pojo.PGCitiesPojo;
import com.wudstay.pojo.PGCityDetailPojo;
import com.wudstay.util.ConnectionDetector;
import com.wudstay.util.WudStayConstants;
import com.wudstay.util.WudstayDialogs;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends BaseActivity {

    private HomeActivity ctx = this;
    private AutoCompleteTextView autoCompleteText_city;
    private ConnectionDetector cd;
    private WudstayDialogs dialog;
    boolean doubleBackToExitPressedOnce = false;
    private ArrayList<PGCityDetailPojo> cities_list;
    private int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        setDrawerAndToolbar("Home");
        WudStayConstants.ACTIVITIES.add(ctx);

        getWudstayURL();

        cd = new ConnectionDetector(getApplicationContext());
        cities_list = new ArrayList<>();
        dialog = new WudstayDialogs(ctx);

        autoCompleteText_city = (AutoCompleteTextView) findViewById(R.id.autoCompleteText_city);
        autoCompleteText_city.setThreshold(0);

        autoCompleteText_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (autoCompleteText_city.getText().toString().length() == 0) {
                    autoCompleteText_city.showDropDown();
                }
            }
        });

        autoCompleteText_city.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                hideSoftKeyboard();

                PGCityDetailPojo data = (PGCityDetailPojo) autoCompleteText_city.getAdapter().getItem(position);
                autoCompleteText_city.setText(data.getCityName());
                autoCompleteText_city.dismissDropDown();

                Intent intent = new Intent(ctx, PGListNewActivity.class);
                intent.putExtra("city_id", data.getCityId());
                intent.putExtra("cityList", data);
                intent.putExtra("city_name", data.getCityName());
                intent.putExtra("location_id", 0);
                startActivity(intent);

            }
        });

        softKeyboardDoneClickListener(autoCompleteText_city);

        LinearLayout main_bg = (LinearLayout) findViewById(R.id.main_bg);
        main_bg.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideSoftKeyboard();
                return false;
            }
        });

        ImageView search_city = (ImageView) findViewById(R.id.search_city);
        search_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectCity();
            }
        });

        getPGCities();
    }

    private void getPGCities() {
        if (cd.isConnectingToInternet()) {
            final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<PGCitiesPojo> call = apiService.getPGCities();
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<PGCitiesPojo>() {
                @Override
                public void onResponse(Call<PGCitiesPojo> call, Response<PGCitiesPojo> response) {
                    if (response.body().getResponseCode() == 1 && response.body().getResponseObject() != null) {
                        cities_list = response.body().getResponseObject();
                        CustomAutoCompleteAdapter adapter = new CustomAutoCompleteAdapter(cities_list, ctx);
                        autoCompleteText_city.setAdapter(adapter);
                    }
                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<PGCitiesPojo> call, Throwable t) {
                    // Log error here since request failed

                    System.out.println("hh failure: " + t.getMessage());
                    progressDialog.getlDialog().dismiss();
                }
            });
        } else {
            dialog.displayCommonDialog(WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }

    @Override
    protected void onPause() {
        hideSoftKeyboard();
        super.onPause();
    }

    @Override
    protected void onStop() {
        hideSoftKeyboard();
        super.onStop();
    }

    public void softKeyboardDoneClickListener(AutoCompleteTextView edittext) {
        edittext.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    selectCity();
                    return true;
                }
                return false;
            }
        });
    }

    private void selectCity() {
        String text = autoCompleteText_city.getText().toString().trim();
        boolean city_found = false;

        if(count == 0)
        if (text.length() >= 3 && cities_list != null) {
            for (int i = 0; i < cities_list.size(); i++) {
                if (text.equalsIgnoreCase(cities_list.get(i).getCityName())) {
                    city_found = true;
                    count = 1;
                    hideSoftKeyboard();
                    Intent intent = new Intent(ctx, PGListNewActivity.class);
                    intent.putExtra("city_id", cities_list.get(i).getCityId());
                    intent.putExtra("cityList", cities_list.get(i));
                    intent.putExtra("city_name", cities_list.get(i).getCityName());
                    intent.putExtra("location_id", 0);
                    startActivity(intent);
                    break;
                }
            }
            if(!city_found)
                Toast.makeText(ctx, "Please make sure you typed correct city name", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(ctx, "Please type at least 3 characters for city name", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    protected void onResume() {
        super.onResume();
        count = 0;
        hideSoftKeyboard();
        autoCompleteText_city.setText("");
    }
}
