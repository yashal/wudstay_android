package com.wudstay.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.wudstay.R;
import com.wudstay.adapter.FavouriteListAdapter;
import com.wudstay.model.ApiClient;
import com.wudstay.model.ApiInterface;
import com.wudstay.pojo.MyFavPgListPojo;
import com.wudstay.pojo.MyFavPgPojo;
import com.wudstay.util.ConnectionDetector;
import com.wudstay.util.WudStayConstants;
import com.wudstay.util.WudstayDialogs;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by yash on 11/16/2016.
 */

public class FavouritesActivity extends BaseActivity {

    private FavouritesActivity ctx = this;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private ConnectionDetector cd;
    private WudstayDialogs dialog;
    private LinearLayout no_favourites_layout;
    private ArrayList<MyFavPgListPojo> favList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourites);

        setDrawerAndToolbar("Favourites");
        WudStayConstants.ACTIVITIES.add(ctx);

        getWudstayURL();

        no_favourites_layout = (LinearLayout) findViewById(R.id.no_favourites_layout);
        recyclerView = (RecyclerView) findViewById(R.id.favourites_recycler_view);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);
//        ImageView back = (ImageView) findViewById(R.id.back);

        cd = new ConnectionDetector(ctx);
        dialog = new WudstayDialogs(ctx);

//        back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//            }
//        });

        getFavouriteList();

    }

    private void getFavouriteList() {
        if (cd.isConnectingToInternet()) {
            final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<MyFavPgPojo> call = apiService.getFavPgList(ctx.getFromPrefs(WudStayConstants.MOBILE));
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<MyFavPgPojo>() {
                @Override
                public void onResponse(Call<MyFavPgPojo> call, Response<MyFavPgPojo> response) {
                    if (response.body().getResponseCode() == 1) {
                        favList = response.body().getResponseObject();
                        if (favList != null && favList.size() > 0) {
                            mAdapter = new FavouriteListAdapter(ctx, favList);
                            recyclerView.setAdapter(mAdapter);
                            no_favourites_layout.setVisibility(View.GONE);
                        } else {
                            no_favourites_layout.setVisibility(View.VISIBLE);
                        }
                    } else {
                        no_favourites_layout.setVisibility(View.VISIBLE);
                    }
                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<MyFavPgPojo> call, Throwable t) {
                    // Log error here since request failed
                    progressDialog.getlDialog().dismiss();
                }
            });
        } else {
            dialog.displayCommonDialog(WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }

}
