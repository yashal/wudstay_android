package com.wudstay.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.wudstay.R;
import com.wudstay.adapter.PgListNewAdapter;
import com.wudstay.model.ApiClient;
import com.wudstay.model.ApiInterface;
import com.wudstay.pojo.CityLocationsListPojo;
import com.wudstay.pojo.PgDetailPojo;
import com.wudstay.pojo.PgListNewResponsePojo;
import com.wudstay.pojo.PgLocationsPojo;
import com.wudstay.pojo.PgPropertyListPojo;
import com.wudstay.pojo.PropertyCategoryListPojo;
import com.wudstay.pojo.PropertyCategoryTypeListPojo;
import com.wudstay.pojo.PropertyTypesListPojo;
import com.wudstay.pojo.RoomOccupancyListPojo;
import com.wudstay.util.ConnectionDetector;
import com.wudstay.util.WudStayConstants;
import com.wudstay.util.WudstayDialogs;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PGListNewActivity extends BaseActivity implements OnMapReadyCallback, AppBarLayout.OnOffsetChangedListener, View.OnClickListener {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private PGListNewActivity ctx = this;
    private ConnectionDetector cd;
    private WudstayDialogs dialog;
    private TextView accommodation_count;
    private int cityId, location_id;
    private GoogleMap googleMap;
    private ArrayList<PgPropertyListPojo> pgList;
    private String locationId = "";
    private String visitDate = "";
    private String genderId = "";
    private String categoryId = "";
    private String occupancy_type = "";
    private String cityName;
    private ArrayList<CityLocationsListPojo> locationList;
    private String locationsName = "";
    private PgPropertyListPojo pgListData;
    private ArrayList<PgDetailPojo> pgDetailArr;
    private ImageView back, menu_img;
    private ArrayList<PropertyCategoryListPojo> propCatsList;
    private ArrayList<PropertyCategoryTypeListPojo> propCatTypesList;
    private ArrayList<PropertyTypesListPojo> propTypesList;
    private ArrayList<RoomOccupancyListPojo> roomOccupancyList, tempArrList;

    private String cityUrl, sortBy, propertyCatIds;
    private String propertyTypeIds="";
    private String occupancyIds="";
    private int currentPage, lowestPrice, maximumPrice;
    private long propLocationId;
    private long propCategoryTypeId;
    private long propCategoryCatId;
    private TextView cityName_txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pglist_new);

        hideSoftKeyboard();

        setDrawerAndToolbar("Pg List");
        WudStayConstants.ACTIVITIES.add(ctx);

        getWudstayURL();

        cityId = getIntent().getIntExtra("city_id", 0);
        location_id = getIntent().getIntExtra("location_id", 0);
        cityName = getIntent().getStringExtra("city_name");
        locationList = new ArrayList<>();
        propCatsList = new ArrayList<>();
        propCatTypesList = new ArrayList<>();
        propTypesList = new ArrayList<>();
        roomOccupancyList = new ArrayList<>();
        tempArrList = new ArrayList<>();

        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        cd = new ConnectionDetector(getApplicationContext());
        dialog = new WudstayDialogs(ctx);
        accommodation_count = (TextView) findViewById(R.id.pglist_accommodation_count);
        recyclerView = (RecyclerView) findViewById(R.id.pglist_recycler_view);
        cityName_txt = (TextView) findViewById(R.id.cityName);
        cityName_txt.setText(cityName);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);

        back = (ImageView) findViewById(R.id.back);
        menu_img = (ImageView) findViewById(R.id.menu_img);

        ImageView filter = (ImageView) findViewById(R.id.filter);
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ctx, FilterActivityNew.class);
                intent.putExtra("city_name", cityName);
                intent.putExtra("visitDate", visitDate);
                intent.putExtra("genderId", genderId);
                intent.putExtra("locationsName", locationsName);
                intent.putExtra("locationList", locationList);
                intent.putExtra("propCatsList", propCatsList);
                intent.putExtra("propCatTypesList", propCatTypesList);
                intent.putExtra("propTypesList", propTypesList);
                intent.putExtra("roomOccupancyList", roomOccupancyList);
//                intent.putExtra("occupancy_type", occupancy_type);
                intent.putExtra("occupancy_id", propCategoryTypeId);
                startActivityForResult(intent, 1);
            }
        });

//        final NestedScrollView mainScrollView = (NestedScrollView) findViewById(R.id.main_scrollview);
//        ImageView transparentImageView = (ImageView) findViewById(R.id.transparent_image);

        /*transparentImageView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        mainScrollView.requestDisallowInterceptTouchEvent(true);
                        // Disable touch on transparent view
                        return false;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        mainScrollView.requestDisallowInterceptTouchEvent(false);
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        mainScrollView.requestDisallowInterceptTouchEvent(true);
                        return false;

                    default:
                        return true;
                }
            }
        });*/

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

//        setToolBarValues();
        getPGList();
        getLocationsList();
    }

    /*private void setToolBarValues() {
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.MyAppbar);

        appBarLayout.addOnOffsetChangedListener(this);

        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapse_toolbar);
        collapsingToolbar.setTitle(cityName);

        collapsingToolbar.setCollapsedTitleTextColor(ContextCompat.getColor(ctx, R.color.white));
        collapsingToolbar.setCollapsedTitleTypeface(Typeface.DEFAULT_BOLD);
        collapsingToolbar.setExpandedTitleColor(ContextCompat.getColor(ctx, R.color.orange));
        collapsingToolbar.setExpandedTitleTypeface(Typeface.DEFAULT_BOLD);

        int height = (int) (getScreenHeight() / 2.4);

        AppBarLayout appbar = (AppBarLayout) findViewById(R.id.MyAppbar);
        appbar.requestLayout();
        appbar.getLayoutParams().height = height;

//        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) appbar.getLayoutParams();
//        AppBarLayout.Behavior behavior = new AppBarLayout.Behavior();
//        behavior.setDragCallback(new AppBarLayout.Behavior.DragCallback() {
//            @Override
//            public boolean canDrag(AppBarLayout appBarLayout) {
//                return false;
//            }
//        });
//        params.setBehavior(behavior);
    }*/

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        this.googleMap.getUiSettings().setZoomControlsEnabled(false);
        this.googleMap.getUiSettings().setCompassEnabled(false);
        this.googleMap.getUiSettings().setZoomGesturesEnabled(true);
    }

    private void udpateMapView(double lat, double lan, int zoomLevel) {
        CameraPosition cameraPosition = null;
        if (googleMap != null) {
            googleMap.clear();
            for (PgPropertyListPojo obj : pgList) {
                googleMap.addMarker(new MarkerOptions().position(new LatLng(obj.getLatitude(), obj.getLongitude())).icon(BitmapDescriptorFactory.fromResource(R.mipmap.map_pin))
                        .title(obj.getDisplayName()).snippet(obj.getDisplayAddress()));
            }
            cameraPosition = new CameraPosition.Builder().target(new LatLng(lat, lan)).zoom(zoomLevel).build();
            if (cameraPosition != null) {
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 2000, null);
            }
        }
    }

    private void getPGList() {
        if (cd.isConnectingToInternet()) {
            final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            if (location_id > 0)
                locationId = String.valueOf(location_id);

            Call<PgListNewResponsePojo> call = apiService.getPGListNew("" + cityId, cityUrl, currentPage, sortBy, propLocationId, locationId,
                    propCategoryTypeId, propertyTypeIds, propCategoryCatId, propertyCatIds, lowestPrice, maximumPrice, occupancyIds);
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<PgListNewResponsePojo>() {
                @Override
                public void onResponse(Call<PgListNewResponsePojo> call, Response<PgListNewResponsePojo> response) {
                    if (response.body() != null && response.body().getResponseCode() == 1) {

                        pgList = response.body().getResponseObject().getLstProperty();
                        if (pgList != null && pgList.size() > 0) {
                            PgListNewAdapter adapter = new PgListNewAdapter(ctx, response.body().getResponseObject().getLstProperty());
                            recyclerView.setAdapter(adapter);
                        }

                        accommodation_count.setText(response.body().getResponseObject().getLstProperty().size() + " PGs/Flats Available ");

                        if (response.body().getResponseObject().getPropCatsList() != null && response.body().getResponseObject().getPropCatsList().size() > 0) {
                            propCatsList = response.body().getResponseObject().getPropCatsList();
                        }
                        if (response.body().getResponseObject().getPropCatTypesList() != null && response.body().getResponseObject().getPropCatTypesList().size() > 0) {
                            propCatTypesList = response.body().getResponseObject().getPropCatTypesList();
                        }
                        if (response.body().getResponseObject().getPropTypesList() != null && response.body().getResponseObject().getPropTypesList().size() > 0) {
                            propTypesList = response.body().getResponseObject().getPropTypesList();
                        }
                        if (response.body().getResponseObject().getRoomOccupancyList() != null && response.body().getResponseObject().getRoomOccupancyList().size() > 0) {
//                            roomOccupancyList = response.body().getResponseObject().getRoomOccupancyList();

                            tempArrList = response.body().getResponseObject().getRoomOccupancyList();
                            for (int i = 0; i < tempArrList.size(); i++)
                            {
                                if (tempArrList.get(i).isActive())
                                {
                                    roomOccupancyList.add(tempArrList.get(i));
                                }
                            }
                        }
                        /*if (response.body().getResponseObject().getLstProperty() != null && response.body().getResponseObject().getLstProperty().size() > 0) {
//                            roomOccupancyList = response.body().getResponseObject().getRoomOccupancyList();

                            pgList = response.body().getResponseObject().getLstProperty();
                            for (int i = 0; i < pgList.size(); i++)
                            {
                                if (pgList.get(i).getNoOfBeds()<=0)
                                {
                                    ""gfrewgfwer
                                }
                            }
                        }*/
                        udpateMapView(response.body().getResponseObject().getCityLatitude(), response.body().getResponseObject().getCityLongitude(), response.body().getResponseObject().getCityMapZoomLevel());

                    }
                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<PgListNewResponsePojo> call, Throwable t) {
                    // Log error here since request failed
                    System.out.println("retrofit hh failure " + t.getMessage());
                    progressDialog.getlDialog().dismiss();
                }
            });
        } else {
            dialog.displayCommonDialog(WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }
    private void getPGListSearch() {
        if (cd.isConnectingToInternet()) {
            final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            if (location_id > 0)
                locationId = String.valueOf(location_id);

            Call<PgListNewResponsePojo> call = apiService.getPGListNew("" + cityId, cityUrl, currentPage, sortBy, propLocationId, locationId,
                    propCategoryTypeId, propertyTypeIds, propCategoryCatId, propertyCatIds, lowestPrice, maximumPrice, occupancyIds);
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<PgListNewResponsePojo>() {
                @Override
                public void onResponse(Call<PgListNewResponsePojo> call, Response<PgListNewResponsePojo> response) {
                    if (response.body() != null && response.body().getResponseCode() == 1) {

                        pgList = response.body().getResponseObject().getLstProperty();

                        udpateMapView(response.body().getResponseObject().getCityLatitude(), response.body().getResponseObject().getCityLongitude(), response.body().getResponseObject().getCityMapZoomLevel());
                        PgListNewAdapter adapter = new PgListNewAdapter(ctx, pgList);
                        recyclerView.setAdapter(adapter);
                        accommodation_count.setText(response.body().getResponseObject().getLstProperty().size() + " PGs/Flats Available ");
                    }
                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<PgListNewResponsePojo> call, Throwable t) {
                    // Log error here since request failed
                    System.out.println("retrofit hh failure " + t.getMessage());
                    progressDialog.getlDialog().dismiss();
                }
            });
        } else {
            dialog.displayCommonDialog(WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }

    private void getLocationsList() {
        if (cd.isConnectingToInternet()) {

            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<PgLocationsPojo> call = apiService.getLocationsList(Long.parseLong(cityId + ""));
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<PgLocationsPojo>() {
                @Override
                public void onResponse(Call<PgLocationsPojo> call, Response<PgLocationsPojo> response) {
                    if (response.body() != null && response.body().getResponseCode() == 1) {
                        locationList = response.body().getResponseObject();
                    }
                }

                @Override
                public void onFailure(Call<PgLocationsPojo> call, Throwable t) {
                    // Log error here since request failed
                    System.out.println("retrofit hh failure " + t.getMessage());
                }
            });
        } else {
//            dialog.displayCommonDialog(WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (verticalOffset == 0)
        {
            // Collapsed
            back.setImageResource(R.mipmap.back_left);
            menu_img.setImageResource(R.mipmap.menu);
        }
        else
        {
            // Not collapsed
            back.setImageResource(R.mipmap.back_white);
            menu_img.setImageResource(R.mipmap.nav);
        }
    }

    @Override
    public void onClick(View v) {

        pgListData = (PgPropertyListPojo)v.getTag(R.string.key);
//        if(pgListData.getNoOfBeds()>0) {
            Intent intent = new Intent(ctx, PgDetailNewActivity.class);
            intent.putExtra("id", pgListData.getId());
            intent.putExtra("visitDate", visitDate);
            intent.putExtra("genderId", genderId);
//        intent.putExtra("occupancy_type", occupancy_type);
            intent.putExtra("propCatsList", propCatsList);
            intent.putExtra("roomOccupancyList", roomOccupancyList);
            intent.putExtra("propTypesList", propTypesList);
            intent.putExtra("occupancy_id", propCategoryTypeId);
            startActivity(intent);
//        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 1
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                locationsName = "";
                locationId = "";
                occupancyIds = "";
                propertyCatIds = "";
                propCategoryTypeId = 0;
                propertyTypeIds = "";
                locationList = (ArrayList<CityLocationsListPojo>) data.getSerializableExtra("locationList");
                for (int i = 0; i < locationList.size(); i++) {
                    if (locationList.get(i).isSelected()) {
                        locationId += locationList.get(i).getLocationId() + ",";
                        locationsName += locationList.get(i).getDisplayName() + ", ";
                    }
                }
                if (locationsName.length() > 0)
                    locationsName = locationsName.substring(0, locationsName.length() - 2);

                if (locationId.length() > 0)
                    locationId = locationId.substring(0, locationId.length() - 1);
                cityName_txt.setText(cityName);

                // for pg occupancy type
                roomOccupancyList = (ArrayList<RoomOccupancyListPojo>) data.getSerializableExtra("roomOccupancyList");
                for (int i = 0; i < roomOccupancyList.size(); i++) {
                    if (roomOccupancyList.get(i).isMflag()) {
                        occupancyIds += String.valueOf(roomOccupancyList.get(i).getId()) + ",";
                    }
                }
                if (occupancyIds.length() > 0)
                    occupancyIds = occupancyIds.substring(0, occupancyIds.length() - 1);

                // for flat occupancy type
                propCatsList = (ArrayList<PropertyCategoryListPojo>) data.getSerializableExtra("propCatsList");
                for (int i = 0; i < propCatsList.size(); i++) {
                    if (propCatsList.get(i).isChecked()) {
                        propertyCatIds += String.valueOf(Long.valueOf(propCatsList.get(i).getCategoryId())) + ",";
                    }
                }
                if (propertyCatIds.length() > 0)
                    propertyCatIds = propertyCatIds.substring(0, propertyCatIds.length() - 1);

                //for PG/Flat
                propCatTypesList = (ArrayList<PropertyCategoryTypeListPojo>) data.getSerializableExtra("propCatTypesList");
                for (int i = 0; i < propCatTypesList.size(); i++) {
                    if (propCatTypesList.get(i).isChecked()) {
                        propCategoryTypeId = Long.valueOf(propCatTypesList.get(i).getCategoryTypeId());
                    }
                }

                // for boy/girl/common
                propTypesList = (ArrayList<PropertyTypesListPojo>) data.getSerializableExtra("propTypesList");
                for (int i = 0; i < propTypesList.size(); i++) {
                    if (propTypesList.get(i).isChecked()) {
                        propertyTypeIds = String.valueOf(propTypesList.get(i).getPgTypeId());
                    }
                }



                visitDate = data.getStringExtra("visitDate");
                if (visitDate == null)
                    visitDate = "";
               /* occupancyIds = data.getStringExtra("occupancy_type");
                if (occupancyIds == null)
                    occupancyIds = "";*/
                /*propertyTypeIds = String.valueOf(data.getIntExtra("genderId", 0));
                if (propertyTypeIds.equals("0"))
                    propertyTypeIds = "";*/

              /*  propCategoryTypeId = Long.valueOf(data.getIntExtra("occupancy_id", 0));
                if (propCategoryTypeId == 0)
                    propCategoryTypeId = 0;*/

               /* propertyCatIds = String.valueOf(data.getIntExtra("flat_type", 0));
                if (propertyCatIds.equals("0"))
                    propertyCatIds = "";*/
                getPGListSearch();
            }
        }
        if (resultCode == RESULT_CANCELED) {
            //Write your code if there's no result
        }
    }
}
