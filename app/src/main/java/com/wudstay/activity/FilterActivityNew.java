package com.wudstay.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wudstay.R;
import com.wudstay.adapter.BookingListAdapter;
import com.wudstay.adapter.CategoryListAdapter;
import com.wudstay.adapter.FlatListAdapter;
import com.wudstay.adapter.TenantListAdapter;
import com.wudstay.pojo.CityLocationsListPojo;
import com.wudstay.pojo.PropertyCategoryListPojo;
import com.wudstay.pojo.PropertyCategoryTypeListPojo;
import com.wudstay.pojo.PropertyTypesListPojo;
import com.wudstay.pojo.RoomOccupancyListPojo;
import com.wudstay.util.WudstayDialogs;

import java.util.ArrayList;

public class FilterActivityNew extends BaseActivity implements View.OnClickListener {
    private CategoryListAdapter category_adapter;
    private BookingListAdapter booking_adapter;
    private FlatListAdapter flat_adapter;
    private TenantListAdapter tenant_adapter;
    private WudstayDialogs dialog;
    private FilterActivityNew ctx = this;
    private GridView category_grid, tenant_grid, booking_grid, flat_grid;
    private TextView display_location, change_location;
    private ArrayList<CityLocationsListPojo> locationList;
    private ArrayList<PropertyCategoryListPojo> propCatsList, flatType;
    private ArrayList<PropertyCategoryTypeListPojo> propCatTypesList;
    private ArrayList<PropertyTypesListPojo> propTypesList;
    private ArrayList<RoomOccupancyListPojo> roomOccupancyList, pgType;
    private RecyclerView mRecyclerViewDialog;
    private Dialog location_list_dialog;
    private String locationsName = "";
    private String visitDate = "";
    //    private String occupancy_type = "";
    private LinearLayout tenantTypeLL, bookingTypeLL, flatTypeLL;
    private int genderId = 0;
    private int categoryId = 0;
    private int bookingTypeId = 0;
    private String city_name;
    private int flatTypeId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_new);

        getInitView();
        LinearLayout search = (LinearLayout) findViewById(R.id.search);
        flatType = new ArrayList<>();
        pgType = new ArrayList<>();

        if ((ArrayList<RoomOccupancyListPojo>) getIntent().getSerializableExtra("roomOccupancyList") != null) {
            roomOccupancyList = (ArrayList<RoomOccupancyListPojo>) getIntent().getSerializableExtra("roomOccupancyList");
            for (int i = 0; i < roomOccupancyList.size(); i++) {
                pgType.add(roomOccupancyList.get(i));
                // this should be dynamic then code will be change according array
            }
        } else {
            roomOccupancyList = new ArrayList<>();
        }

        locationList = (ArrayList<CityLocationsListPojo>) getIntent().getSerializableExtra("locationList");

        if (getIntent().getSerializableExtra("propCatsList") != null) {
            propCatsList = (ArrayList<PropertyCategoryListPojo>) getIntent().getSerializableExtra("propCatsList");
            for (int i = 0; i < propCatsList.size(); i++) {
                if (propCatsList.get(i).getCategoryType().getCategoryTypeName().equalsIgnoreCase("Flat")) {
                    flatType.add(propCatsList.get(i));
                }
                // this should be dynamic then code will be change according array
            }

            /*List<String> clientList = new ArrayList<>(Arrays.asList("One Room", "Double Sharing", "Triple Sharing"));
            for(int i = 0; i < clientList.size(); i++)
            {
                propCatsList.get(i).setCategoryName(clientList.get(i));
                    pgType.add(propCatsList.get(i));
                    booking_adapter = new BookingListAdapter(ctx, pgType);
                    booking_grid.setAdapter(booking_adapter);


            }
            */
        } else {
            propCatsList = new ArrayList<>();
        }
        if (getIntent().getSerializableExtra("propCatTypesList") != null) {
            propCatTypesList = (ArrayList<PropertyCategoryTypeListPojo>) getIntent().getSerializableExtra("propCatTypesList");
        } else {
            propCatTypesList = new ArrayList<>();
        }
        if (getIntent().getSerializableExtra("propTypesList") != null) {
            propTypesList = (ArrayList<PropertyTypesListPojo>) getIntent().getSerializableExtra("propTypesList");
        } else {
            propTypesList = new ArrayList<>();
        }
        city_name = getIntent().getStringExtra("city_name");
        categoryId = (int) getIntent().getLongExtra("occupancy_id", 0);

        visitDate = getIntent().getStringExtra("visitDate");
//        occupancy_type = getIntent().getStringExtra("occupancy_type");
        locationsName = getIntent().getStringExtra("locationsName");

        if (locationsName.equals(""))
            display_location.setText(getString(R.string.all_location));
        else
            display_location.setText(locationsName);
        if (categoryId == 101) {
            tenantTypeLL.setVisibility(View.VISIBLE);
            bookingTypeLL.setVisibility(View.VISIBLE);
            flatTypeLL.setVisibility(View.GONE);
        } else if (categoryId == 201) {
            tenantTypeLL.setVisibility(View.GONE);
            bookingTypeLL.setVisibility(View.GONE);
            flatTypeLL.setVisibility(View.VISIBLE);
        }

        category_adapter = new CategoryListAdapter(ctx, propCatTypesList);
        category_grid.setAdapter(category_adapter);
        setGridViewHeightBasedOnChildren(category_grid, 3);

        tenant_adapter = new TenantListAdapter(ctx, propTypesList);
        tenant_grid.setAdapter(tenant_adapter);
        setGridViewHeightBasedOnChildren(tenant_grid, 3);

        flat_adapter = new FlatListAdapter(ctx, flatType);
        flat_grid.setAdapter(flat_adapter);
        setGridViewHeightBasedOnChildren(flat_grid, 3);

        booking_adapter = new BookingListAdapter(ctx, pgType);
        booking_grid.setAdapter(booking_adapter);
        setGridViewHeightBasedOnChildren(booking_grid, 3);

        ImageView cross = (ImageView) findViewById(R.id.cross);
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        change_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayLocationDialog();
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                intent.putExtra("visitDate", visitDate);
//                intent.putExtra("genderId", genderId);
//                intent.putExtra("occupancy_type", bookingTypeId);
//                intent.putExtra("flat_type", flatTypeId);
//                intent.putExtra("occupancy_id", categoryId);
                intent.putExtra("city_name", city_name);
                intent.putExtra("locationsName", locationsName);
                intent.putExtra("locationList", locationList);
                intent.putExtra("roomOccupancyList", roomOccupancyList);
                intent.putExtra("propCatsList", propCatsList);
                intent.putExtra("propCatTypesList", propCatTypesList);
                intent.putExtra("propTypesList", propTypesList);
                finish();
            }
        });
    }

    public void getInitView() {
        LinearLayout search = (LinearLayout) findViewById(R.id.search);
        change_location = (TextView) findViewById(R.id.change_location);

        tenantTypeLL = (LinearLayout) findViewById(R.id.tenantTypeLL);
        bookingTypeLL = (LinearLayout) findViewById(R.id.bookingTypeLL);
        flatTypeLL = (LinearLayout) findViewById(R.id.flatTypeLL);

        display_location = (TextView) findViewById(R.id.display_location);
        change_location = (TextView) findViewById(R.id.change_location);

        category_grid = (GridView) findViewById(R.id.category_grid);
        booking_grid = (GridView) findViewById(R.id.booking_grid);
        tenant_grid = (GridView) findViewById(R.id.tenant_grid);
        flat_grid = (GridView) findViewById(R.id.flat_grid);
    }

    private void displayLocationDialog() {
        location_list_dialog = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        location_list_dialog.setContentView(R.layout.locations_list_dialog);

        mRecyclerViewDialog = (RecyclerView) location_list_dialog.findViewById(R.id.mp3songs_artistdialog_recycler);
        LinearLayout dialog_doneLL = (LinearLayout) location_list_dialog.findViewById(R.id.dialog_doneLL);

        location_list_dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        mRecyclerViewDialog.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(ctx);
        mRecyclerViewDialog.setLayoutManager(mLayoutManager);

        location_list_dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                setText();
            }
        });

        dialog_doneLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                location_list_dialog.dismiss();
            }
        });

        LocationListAdapter adapter = new LocationListAdapter(ctx, locationList);
        if (mRecyclerViewDialog != null)
            mRecyclerViewDialog.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        location_list_dialog.show();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.category_layout:
                int index = (int) v.getTag(R.string.key);
                ArrayList<PropertyCategoryTypeListPojo> data_cat_type_list = (ArrayList<PropertyCategoryTypeListPojo>) v.getTag(R.string.data);
                categoryId = data_cat_type_list.get(index).getCategoryTypeId();
                for (int i = 0; i < data_cat_type_list.size(); i++) {
                    if (i == index) {
                        data_cat_type_list.get(i).setChecked(true);
                    } else {
                        data_cat_type_list.get(i).setChecked(false);
                    }
                }
                if (data_cat_type_list.get(index).getCategoryTypeName().equalsIgnoreCase("PG")) {
                    tenantTypeLL.setVisibility(View.VISIBLE);
                    bookingTypeLL.setVisibility(View.VISIBLE);
                    flatTypeLL.setVisibility(View.GONE);
                    for (int i = 0; i < flatType.size(); i++)
                        flatType.get(i).setChecked(false);
                    flat_adapter.notifyDataSetChanged();
                } else if (data_cat_type_list.get(index).getCategoryTypeName().equalsIgnoreCase("Flat")) {
                    tenantTypeLL.setVisibility(View.GONE);
                    bookingTypeLL.setVisibility(View.GONE);
                    flatTypeLL.setVisibility(View.VISIBLE);

                    for (int i = 0; i < pgType.size(); i++)
                        pgType.get(i).setMflag(false);
                    for (int i = 0; i < roomOccupancyList.size(); i++) {
                        roomOccupancyList.get(i).setMflag(false);
                    }
                    for (int i = 0; i < propTypesList.size(); i++)
                        propTypesList.get(i).setChecked(false);
                    tenant_adapter.notifyDataSetChanged();
                    booking_adapter.notifyDataSetChanged();
                }
                category_adapter.notifyDataSetChanged();
                break;
            case R.id.tenant_layout:
                int index_tenant = (int) v.getTag(R.string.key);
                ArrayList<PropertyTypesListPojo> data_tenant_list = (ArrayList<PropertyTypesListPojo>) v.getTag(R.string.data);
                genderId = data_tenant_list.get(index_tenant).getPgTypeId();
                for (int i = 0; i < data_tenant_list.size(); i++) {

                    if (i == index_tenant) {
                        data_tenant_list.get(i).setChecked(true);
                    } else {
                        data_tenant_list.get(i).setChecked(false);
                    }
                }
                tenant_adapter.notifyDataSetChanged();
                break;

            case R.id.flat_layout:
                int index_flat = (int) v.getTag(R.string.key);
                PropertyCategoryListPojo data_flat_list = (PropertyCategoryListPojo) v.getTag(R.string.data);
                flatTypeId = data_flat_list.getCategoryId();
                if (data_flat_list.isChecked()) {
                    int check = 0;
                    for (int i = 0; i < propCatsList.size(); i++) {
                        if (propCatsList.get(i).isChecked())
                            check = check + 1;
                    }
                    if (check >= 2)
                        data_flat_list.setChecked(false);
                } else {
                    data_flat_list.setChecked(true);
                }

                flat_adapter.notifyDataSetChanged();
                break;

            case R.id.booking_layout:
                int index_booking = (int) v.getTag(R.string.key);
                RoomOccupancyListPojo data_booking_list = (RoomOccupancyListPojo) v.getTag(R.string.data);
                bookingTypeId = data_booking_list.getId();
                if (data_booking_list.isMflag()) {
                    int check = 0;
                    for (int i = 0; i < roomOccupancyList.size(); i++) {
                        if (roomOccupancyList.get(i).isMflag())
                            check = check + 1;
                    }
                    if (check >= 2)
                        data_booking_list.setMflag(false);
                } else {
                    data_booking_list.setMflag(true);
                }


                booking_adapter.notifyDataSetChanged();
                break;

            default:
                break;
        }
    }


    public class LocationListAdapter extends RecyclerView.Adapter<LocationListAdapter.ViewHolder> {

        private ArrayList<CityLocationsListPojo> locationList;
        private Activity context;

        public LocationListAdapter(Activity context, ArrayList<CityLocationsListPojo> arrayArtistList) {
            this.context = context;
            this.locationList = arrayArtistList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.locations_row_layout, parent, false);
            v.setOnClickListener(new MyOnClickListener());
            ViewHolder viewHolder = new ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            final CityLocationsListPojo data = locationList.get(position);
            holder.artist_name.setText(data.getDisplayName());
            if (data.isSelected()) {
                holder.check.setChecked(true);
                holder.check.setButtonDrawable(R.mipmap.checkbox_selected);
            } else {
                holder.check.setChecked(false);
                holder.check.setButtonDrawable(R.mipmap.checkbox);
            }
        }

        class MyOnClickListener implements View.OnClickListener {
            @Override
            public void onClick(View v) {
                int itemPosition = mRecyclerViewDialog.getChildPosition(v);
                final CityLocationsListPojo data = locationList.get(itemPosition);
                CheckBox check = (CheckBox) v.findViewById(R.id.check_box);
                if (!data.isSelected()) {
                    check.setButtonDrawable(R.mipmap.checkbox_selected);
                    check.setChecked(true);
                    data.setSelected(true);
                } else {
                    check.setButtonDrawable(R.mipmap.checkbox);
                    check.setChecked(false);
                    data.setSelected(false);
                }
            }
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            private TextView artist_name;
            private CheckBox check;

            public ViewHolder(View itemView) {
                super(itemView);
                artist_name = (TextView) itemView.findViewById(R.id.artist_name);
                check = (CheckBox) itemView.findViewById(R.id.check_box);
            }
        }

        @Override
        public int getItemCount() {
            return locationList.size();
        }
    }

    private void setText() {
        locationsName = "";
        for (int i = 0; i < locationList.size(); i++) {
            if (locationList.get(i).isSelected()) {
                locationsName += locationList.get(i).getDisplayName() + ", ";
            }
        }

        if (locationsName.length() > 0)
            locationsName = locationsName.substring(0, locationsName.length() - 2);

        if (locationsName.equals(""))
            display_location.setText(getString(R.string.all_location));
        else
            display_location.setText(locationsName);

    }

}
