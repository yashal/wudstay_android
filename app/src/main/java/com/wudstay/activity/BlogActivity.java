package com.wudstay.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.wudstay.R;
import com.wudstay.adapter.BlogAdapter;
import com.wudstay.pojo.FeedsPojo;
import com.wudstay.util.WudStayConstants;
import com.wudstay.util.WudstayDialogs;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class BlogActivity extends BaseActivity {

    private XmlPullParserFactory xmlFactoryObject;
    private ArrayList<FeedsPojo> arr;
    private FeedsPojo obj;
    private ListView blog_list;
    private BlogActivity ctx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog);
        setDrawerAndToolbar("Blog");
        WudStayConstants.ACTIVITIES.add(ctx);

        /*ImageView back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });*/

        blog_list = (ListView) findViewById(R.id.blog_list);
        arr = new ArrayList<>();
        fetchXML();
    }

    public void parseXMLAndStoreIt(XmlPullParser myParser) {

        try {
            boolean insideItem = false;
            // Returns the type of current event: START_TAG, END_TAG, etc..
            int event = myParser.getEventType();
            while (event != XmlPullParser.END_DOCUMENT)
            {
                String name=myParser.getName();
                switch (event){
                    case XmlPullParser.START_TAG:
                        if(name.equalsIgnoreCase("item"))
                            insideItem = true;
                        if(insideItem) {
                            if (name.equalsIgnoreCase("title")) {
                                obj = new FeedsPojo();
                                obj.setTitle(myParser.nextText());
                            } else if (name.equalsIgnoreCase("link")) {
                                obj.setLink(myParser.nextText());
                            } else if (name.equalsIgnoreCase("pubDate")) {
                                obj.setPubDate(myParser.nextText());
                            } else if (name.equalsIgnoreCase("dc:creator")) {
                                obj.setCreator(myParser.nextText());
                            } else if (name.equalsIgnoreCase("description")) {
                                String html = myParser.nextText().toString();

                                int startPos = html.indexOf("src=\"") + 5;
                                int endPos = html.indexOf("\"", startPos);
                                String src = html.substring(startPos, endPos);
                                obj.setImageSrc(src);
                                arr.add(obj);
                            }
                        }
                        break;

                    case XmlPullParser.END_TAG:
                        if(name.equalsIgnoreCase("item"))
                            insideItem = false;
                        break;
                }
                event = myParser.next();
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    BlogAdapter adapter = new BlogAdapter(arr, BlogActivity.this);
                    blog_list.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
            });
        }

        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fetchXML() {
        final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
        progressDialog.showProgressDialog(ctx);
        progressDialog.getlDialog().setCancelable(Boolean.FALSE);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    URL url = new URL(WudStayConstants.BLOG);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();

                    conn.setReadTimeout(20000 /* milliseconds */);
                    conn.setConnectTimeout(25000 /* milliseconds */);
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);

                    // Starts the query
                    conn.connect();
                    InputStream stream = conn.getInputStream();

                    xmlFactoryObject = XmlPullParserFactory.newInstance();
                    XmlPullParser myparser = xmlFactoryObject.newPullParser();

                    myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                    myparser.setInput(stream, null);

                    parseXMLAndStoreIt(myparser);

                    blog_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                            Intent intent = new Intent(BlogActivity.this, BlogDetailActivity.class);
                            intent.putExtra("blogData", arr.get(arg2));
                            startActivity(intent);
                        }
                    });

                    if(progressDialog.getlDialog() != null && progressDialog.getlDialog().isShowing()) {
                        progressDialog.getlDialog().dismiss();
                    }

                    stream.close();

                }

                catch (Exception e) {
                    if(progressDialog.getlDialog() != null && progressDialog.getlDialog().isShowing()) {
                        progressDialog.getlDialog().dismiss();
                    }

//					AppUtils.showErrorAlert(BlogActivity.this, AppConstants.DEFAULT_ERROR_TITLE, AppConstants.DEFAULT_ERROR_MESSAGE, AppConstants.TEXT_OK);
//					System.out.println(e.getMessage());
                }
            }
        });
        thread.start();
    }
}
