package com.wudstay.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wudstay.R;
import com.wudstay.util.WudstayDialogs;

public class NeedsToTalkActivity extends BaseActivity {

    private LinearLayout first_quarter_layout, second_quarter_layout, third_quarter_layout, forth_quarter_layout;
    private TextView first_quarter_text, second_quarter_text, third_quarter_text, forth_quarter_text;
    private TextView callMeBack;
    private ImageView back;
    private Activity ctx = this;
    private String call_time = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_need_to_talk);

        first_quarter_layout = (LinearLayout) findViewById(R.id.first_quarter_layout);
        second_quarter_layout = (LinearLayout) findViewById(R.id.second_quarter_layout);
        third_quarter_layout = (LinearLayout) findViewById(R.id.third_quarter_layout);
        forth_quarter_layout = (LinearLayout) findViewById(R.id.forth_quarter_layout);

        first_quarter_text = (TextView) findViewById(R.id.first_quarter_text);
        second_quarter_text = (TextView) findViewById(R.id.second_quarter_text);
        third_quarter_text = (TextView) findViewById(R.id.third_quarter_text);
        forth_quarter_text = (TextView) findViewById(R.id.forth_quarter_text);

        back = (ImageView) findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        callMeBack = (TextView) findViewById(R.id.callMeBack);

        first_quarter_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFirstQuarterSelected();
                call_time = first_quarter_text.getText().toString();
            }
        });

        second_quarter_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSecondQuarterSelected();
                call_time = second_quarter_text.getText().toString();
            }
        });

        third_quarter_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setThirdQuarterSelected();
                call_time = third_quarter_text.getText().toString();
            }
        });

        forth_quarter_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setForthQuarterSelected();
                call_time = forth_quarter_text.getText().toString();
            }
        });

        callMeBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (call_time.isEmpty()) {
                    Toast.makeText(ctx, "Please select call time", Toast.LENGTH_SHORT).show();
                } else {
                    WudstayDialogs thanksDialog = new WudstayDialogs(ctx);
                    thanksDialog.showThanksDialog(call_time);
                }
            }
        });
    }

    private void setFirstQuarterSelected() {
        setLightColorLayout(first_quarter_layout, first_quarter_text);
        setDarkColorLayout(second_quarter_layout, second_quarter_text);
        setDarkColorLayout(third_quarter_layout, third_quarter_text);
        setDarkColorLayout(forth_quarter_layout, forth_quarter_text);
    }

    private void setSecondQuarterSelected() {
        setDarkColorLayout(first_quarter_layout, first_quarter_text);
        setLightColorLayout(second_quarter_layout, second_quarter_text);
        setDarkColorLayout(third_quarter_layout, third_quarter_text);
        setDarkColorLayout(forth_quarter_layout, forth_quarter_text);
    }

    private void setThirdQuarterSelected() {
        setDarkColorLayout(first_quarter_layout, first_quarter_text);
        setDarkColorLayout(second_quarter_layout, second_quarter_text);
        setLightColorLayout(third_quarter_layout, third_quarter_text);
        setDarkColorLayout(forth_quarter_layout, forth_quarter_text);
    }

    private void setForthQuarterSelected() {
        setDarkColorLayout(first_quarter_layout, first_quarter_text);
        setDarkColorLayout(second_quarter_layout, second_quarter_text);
        setDarkColorLayout(third_quarter_layout, third_quarter_text);
        setLightColorLayout(forth_quarter_layout, forth_quarter_text);
    }

}
