package com.wudstay.activity;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wudstay.R;
import com.wudstay.adapter.ComplaintAdapter;
import com.wudstay.util.DepthPageTransformer;
import com.wudstay.util.WudStayConstants;

public class ComplaintsActivity extends BaseActivity implements View.OnClickListener {

    private TextView new_complaint_text, old_complaint_text;
    private LinearLayout new_complaint_Layout, old_complaint_layout, new_complaint_selector, old_complaint_selector;
    private ImageView back;
    private ViewPager viewPager;
    private ComplaintAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaints);
        setDrawerAndToolbar("Complaint");
        WudStayConstants.ACTIVITIES.add(ComplaintsActivity.this);

        getURL();

        new_complaint_text = (TextView) findViewById(R.id.tab1_text);
        old_complaint_text = (TextView) findViewById(R.id.tab2_text);
        new_complaint_selector = (LinearLayout) findViewById(R.id.tab1_selector);
        old_complaint_selector = (LinearLayout) findViewById(R.id.tab2_selector);

        new_complaint_Layout = (LinearLayout) findViewById(R.id.tab1_layout);
        old_complaint_layout = (LinearLayout) findViewById(R.id.tab2_layout);
        viewPager = (ViewPager) findViewById(R.id.complaint_viewpager);

//        back = (ImageView) findViewById(R.id.back);
//
//        back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//            }
//        });

        pagerAdapter = new ComplaintAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        viewPager.setOffscreenPageLimit(1);
        viewPager.setPageTransformer(true, new DepthPageTransformer());

        new_complaint_text.setText(getResources().getString(R.string.new_complaints));
        old_complaint_text.setText(getResources().getString(R.string.old_complaints));

        new_complaint_Layout.setOnClickListener(this);
        old_complaint_layout.setOnClickListener(this);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                setStyle(position);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });
    }


    private void setStyle(int position) {
        if (position == 0) {
            new_complaint_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
            old_complaint_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));

            new_complaint_selector.setVisibility(View.VISIBLE);
            old_complaint_selector.setVisibility(View.GONE);
        } else if (position == 1) {
            new_complaint_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
            old_complaint_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));

            old_complaint_selector.setVisibility(View.VISIBLE);
            new_complaint_selector.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tab1_layout:
                if (viewPager.getCurrentItem() != 0) {
                    viewPager.setCurrentItem(0);
                    setStyle(0);
                }
                break;

            case R.id.tab2_layout:
                if (viewPager.getCurrentItem() != 1) {
                    viewPager.setCurrentItem(1);
                    setStyle(1);
                }
                break;

            default:
                break;
        }
    }
}
