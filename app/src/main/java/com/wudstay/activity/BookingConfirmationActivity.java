package com.wudstay.activity;

import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.DatePicker;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.payu.india.Interfaces.PaymentRelatedDetailsListener;
import com.payu.india.Model.PaymentParams;
import com.payu.india.Model.PayuConfig;
import com.payu.india.Model.PayuHashes;
import com.payu.india.Model.PayuResponse;
import com.payu.india.Payu.Payu;
import com.payu.india.Payu.PayuConstants;
import com.payu.india.Payu.PayuErrors;
import com.payu.payuui.Activity.PayUBaseActivity;
import com.wudstay.R;
import com.wudstay.adapter.BookingFlatListAdapter;
import com.wudstay.adapter.BookingTenantListAdapter;
import com.wudstay.adapter.NewBookingListAdapter;
import com.wudstay.model.ApiClient;
import com.wudstay.model.ApiInterface;
import com.wudstay.pojo.FavoriteResponsePojo;
import com.wudstay.pojo.PGDetailNewPojo;
import com.wudstay.pojo.PayURequestDataPojo;
import com.wudstay.pojo.PayURequestParametersPojo;
import com.wudstay.pojo.PayUResponsePojo;
import com.wudstay.pojo.PayUVOPojo;
import com.wudstay.pojo.PayuHashsPojo;
import com.wudstay.pojo.PropertyCategoryListPojo;
import com.wudstay.pojo.PropertyTypesListPojo;
import com.wudstay.pojo.RoomOccupancyListPojo;
import com.wudstay.util.ConnectionDetector;
import com.wudstay.util.PayuGateaway;
import com.wudstay.util.WudStayConstants;
import com.wudstay.util.WudstayDialogs;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.RECEIVE_SMS;

public class BookingConfirmationActivity extends BaseActivity implements BookmarkCallback, PaymentRelatedDetailsListener, View.OnClickListener {

    private TextView coming_saturday, coming_sunday, booking_personName, booking_address, pic_date_text,
            terms_condition;
    private PGDetailNewPojo pgDetail;
    private String address = "";
    private BookingConfirmationActivity ctx = this;
    private LinearLayout confirm;
    private LinearLayout datePick, saturday_layout, sunday_layout, terms_conditionLayout;
    private ImageView favorite;
    private Calendar myCalendar;
    private int month, year, day;
    private String visitDate;
    private int genderId = 0;
    private TextView token_text;
    private String occupancy_type = "";

    private ConnectionDetector cd;
    private WudstayDialogs dialog;
    private String mobileNumber;
    private String guestName;
    private String guestEmail;
    private final static int RECEIVE_SMS_RESULT = 102;
    private SharedPreferences sharedPreferences;
    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected;
    private boolean termsConditionIsSelected = true;
    private String pgId;
    private CollapsingToolbarLayout collapsingToolbar;

    private PaymentParams mPaymentParams;
    private PayuConfig payuConfig;
    private Intent intent;
    private String totalAmount;
    private String transaction_id;
    private PayuResponse mPayuResponse;
    private PayuGateaway payuGateway;

    private NewBookingListAdapter booking_adapter;
    private BookingFlatListAdapter flat_adapter;
    private BookingTenantListAdapter tenant_adapter;
    private GridView tenant_grid, booking_grid, flat_grid;
    private ArrayList<RoomOccupancyListPojo> pgType;
    private ArrayList<PropertyCategoryListPojo> propCatsList, flatType;
    private ArrayList<PropertyTypesListPojo> propTypesList;
    private ArrayList<RoomOccupancyListPojo> roomOccupancyList;
    private int categoryId;
    private LinearLayout tenantTypeLL, bookingTypeLL, flatTypeLL;
    private String occ_type = "Pg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_confirmation_new);

        setDrawerAndToolbar("Booking Confirmation");
        WudStayConstants.ACTIVITIES.add(ctx);

        getWudstayURL();

        pgDetail = (PGDetailNewPojo) getIntent().getSerializableExtra("pgDetail");

        ImageView pgImage = (ImageView) findViewById(R.id.pgImage);
        setImageInLayout(ctx, 600, (int) (getScreenHeight() / 2.4), pgDetail.getMainImage(), pgImage);

        ImageView back = (ImageView) findViewById(R.id.back);

        findAllGlobalViews();
        flatType = new ArrayList<>();
        pgType = new ArrayList<>();

        if(pgDetail.isFavouritePg())
            favorite.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.favorite_red));
        else
            favorite.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.favorite));

        token_text.setText("Confirm booking by paying the token amount of "+getResources().getString(R.string.Rs) +" "+ pgDetail.getTokenAmount());

//        occupancy_type = getIntent().getStringExtra("occupancy_type");
        visitDate = getIntent().getStringExtra("visitDate");
        if (getIntent().getStringExtra("genderId") != null && !getIntent().getStringExtra("genderId").equals(""))
            genderId = Integer.parseInt(getIntent().getStringExtra("genderId"));

        cd = new ConnectionDetector(getApplicationContext());
        dialog = new WudstayDialogs(ctx);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        address = getAddress(pgDetail.getDisplayAddress());
        booking_address.setText(address);

        booking_personName.setText(pgDetail.getContactPersonName());

        myCalendar = Calendar.getInstance();
        day = myCalendar.DAY_OF_MONTH;
        month = myCalendar.MONTH;
        year = myCalendar.YEAR;

        // for Saturday
        String saturday = getNextSaturday();
        final String[] separated = saturday.split("/");
        coming_saturday.setText(separated[0] + getDayOfMonthSuffix(Integer.parseInt(separated[0])) + "\n" + separated[1]);

        // for Sunday
        String sunday = getNextSunday();
        final String[] checkOut_separated = sunday.split("/");
        coming_sunday.setText(checkOut_separated[0] + getDayOfMonthSuffix(Integer.parseInt(checkOut_separated[0])) + "\n" + checkOut_separated[1]);

        if (!visitDate.equals("")) {
            day = Integer.parseInt(visitDate.split("/")[0]);
            month = Integer.parseInt(visitDate.split("/")[1]) - 1;
            year = Integer.parseInt(visitDate.split("/")[2]);
            if (day == Integer.parseInt(separated[0]) && month + 1 == Integer.parseInt(getMonth(separated[1]))) {
                setSaturdaySelected();
            } else if (day == Integer.parseInt(checkOut_separated[0]) && month + 1 == Integer.parseInt(getMonth(checkOut_separated[1]))) {
                setSundaySelected();
            } else {
                setPickDateSelected();
                myCalendar.set(year, month, day);
                updateLabel();
            }
        }

        saturday_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSaturdaySelected();
                pic_date_text.setText(getResources().getString(R.string.pick_date));

                visitDate = separated[0] + "/" + getMonth(separated[1]) + "/" + separated[2];
            }
        });

        sunday_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSundaySelected();
                pic_date_text.setText(getResources().getString(R.string.pick_date));

                visitDate = checkOut_separated[0] + "/" + getMonth(checkOut_separated[1]) + "/" + checkOut_separated[2];
            }
        });

        datePick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                visitDate = "";
                pic_date_text.setText(getResources().getString(R.string.pick_date));
                setPickDateSelected();
                showDatePicker();
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("hh visitDate is "+visitDate);
                if (visitDate.equals("")) {
                    Toast.makeText(ctx, "Please select Booking date", Toast.LENGTH_SHORT).show();
                } else if (!termsConditionIsSelected) {
                    Toast.makeText(ctx, "Please accept the terms and conditions", Toast.LENGTH_SHORT).show();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        addPermissionDialogMarshMallowForSMS();
                    } else {
                        goAhead();
                    }
                }
            }
        });

        SpannableString ss = new SpannableString(getResources().getString(R.string.terms_condition));
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent intent = new Intent(ctx, WebViewsActivity.class);
                intent.putExtra("header", getString(R.string.tnc));
                intent.putExtra("url", WudStayConstants.TERMS_AND_CONDITIONS);
                startActivity(intent);
            }
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        ss.setSpan(clickableSpan, 9, terms_condition.getText().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        terms_condition.setText(ss);
        terms_condition.setMovementMethod(LinkMovementMethod.getInstance());
        terms_condition.setHighlightColor(Color.BLUE);

//        terms_conditionLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (!termsConditionIsSelected) {
//                    setLightColorLayout(terms_conditionLayout, terms_condition);
//                    termsConditionIsSelected = true;
//                } else {
//                    setDarkColorLayout(terms_conditionLayout, terms_condition);
//                    termsConditionIsSelected = false;
//                }
//            }
//        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("pgDetail",pgDetail);
                setResult(RESULT_OK,returnIntent);
                finish();
            }
        });

        favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddRemoveFavoriteNew(pgDetail, ctx);
            }
        });

        setToolBarValues();

        if ((ArrayList<PropertyCategoryListPojo>) getIntent().getSerializableExtra("propCatsList") != null)
        {
            propCatsList = (ArrayList<PropertyCategoryListPojo>) getIntent().getSerializableExtra("propCatsList");
            for (int i = 0; i < propCatsList.size(); i++) {
                if (propCatsList.get(i).getCategoryType().getCategoryTypeName().equalsIgnoreCase("Flat")) {
                    flatType.add(propCatsList.get(i));
                }
            }
        }

        if ((ArrayList<RoomOccupancyListPojo>) getIntent().getSerializableExtra("roomOccupancyList") != null)
        {
            roomOccupancyList = (ArrayList<RoomOccupancyListPojo>) getIntent().getSerializableExtra("roomOccupancyList");
            for (int i = 0; i < roomOccupancyList.size(); i++) {
                pgType.add(roomOccupancyList.get(i));
                // this should be dynamic then code will be change according array
            }
        }
        else
        {
            roomOccupancyList = new ArrayList<>();
        }

        if ((ArrayList<PropertyTypesListPojo>) getIntent().getSerializableExtra("propTypesList") != null)
        {
            propTypesList = (ArrayList<PropertyTypesListPojo>) getIntent().getSerializableExtra("propTypesList");
        }
        else
        {
            propTypesList = new ArrayList<>();
        }
        categoryId = getIntent().getIntExtra("occupancy_id", 0);
        System.out.println("xxghxgxgxgxg new booking "+categoryId);
        if (categoryId==101)
        {
            tenantTypeLL.setVisibility(View.VISIBLE);
            bookingTypeLL.setVisibility(View.VISIBLE);
            flatTypeLL.setVisibility(View.GONE);
        }
        else if (categoryId==201)
        {
            tenantTypeLL.setVisibility(View.GONE);
            bookingTypeLL.setVisibility(View.GONE);
            flatTypeLL.setVisibility(View.VISIBLE);
        }

        if (propTypesList.size() > 0)
        {
            tenant_adapter = new BookingTenantListAdapter(ctx, propTypesList);
            tenant_grid.setAdapter(tenant_adapter);
            setGridViewHeightBasedOnChildren(tenant_grid, 3);
        }

        if (pgType.size() > 0)
        {
            booking_adapter = new NewBookingListAdapter(ctx, pgType);
            booking_grid.setAdapter(booking_adapter);
            setGridViewHeightBasedOnChildren(booking_grid, 3);
        }

        if (flatType.size() > 0)
        {
            flat_adapter = new BookingFlatListAdapter(ctx, flatType);
            flat_grid.setAdapter(flat_adapter);
            setGridViewHeightBasedOnChildren(flat_grid, 3);
        }

    }

    private void setToolBarValues() {
        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapse_toolbar);
        collapsingToolbar.setTitle("");

        collapsingToolbar.setCollapsedTitleTextColor(ContextCompat.getColor(ctx, R.color.white));
        collapsingToolbar.setExpandedTitleColor(ContextCompat.getColor(ctx, R.color.white));

        int height = (int) (getScreenHeight() / 2.4);

        AppBarLayout appbar = (AppBarLayout) findViewById(R.id.MyAppbar);
        appbar.requestLayout();
        appbar.getLayoutParams().height = height;

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) appbar.getLayoutParams();
        AppBarLayout.Behavior behavior = new AppBarLayout.Behavior();
        behavior.setDragCallback(new AppBarLayout.Behavior.DragCallback() {
            @Override
            public boolean canDrag(AppBarLayout appBarLayout) {
                return false;
            }
        });
        params.setBehavior(behavior);
    }

    private void findAllGlobalViews() {

        saturday_layout = (LinearLayout) findViewById(R.id.saturday_layout);
        sunday_layout = (LinearLayout) findViewById(R.id.sunday_layout);
        coming_saturday = (TextView) findViewById(R.id.coming_saturday);
        coming_sunday = (TextView) findViewById(R.id.coming_sunday);
        datePick = (LinearLayout) findViewById(R.id.datePick);
        pic_date_text = (TextView) findViewById(R.id.booking_pic_date_text);

        booking_personName = (TextView) findViewById(R.id.booking_personName);
        booking_address = (TextView) findViewById(R.id.booking_address);

        favorite = (ImageView) findViewById(R.id.favorite);
        terms_condition = (TextView) findViewById(R.id.terms_condition);
        terms_conditionLayout = (LinearLayout) findViewById(R.id.terms_conditionLayout);

        confirm = (LinearLayout) findViewById(R.id.confirm);
        token_text = (TextView) findViewById(R.id.token_text);

        booking_grid = (GridView)findViewById(R.id.booking_grid);
        tenant_grid = (GridView)findViewById(R.id.tenant_grid);
        flat_grid = (GridView)findViewById(R.id.flat_grid);
        tenantTypeLL = (LinearLayout)findViewById(R.id.tenantTypeLL);
        bookingTypeLL = (LinearLayout)findViewById(R.id.bookingTypeLL);
        flatTypeLL = (LinearLayout)findViewById(R.id.flatTypeLL);

    }

    private void setSaturdaySelected() {
        setLightColorLayout(saturday_layout, coming_saturday);
        setDarkColorLayout(sunday_layout, coming_sunday);
        setDarkColorLayout(datePick, pic_date_text);
    }

    private void setSundaySelected() {
        setDarkColorLayout(saturday_layout, coming_saturday);
        setLightColorLayout(sunday_layout, coming_sunday);
        setDarkColorLayout(datePick, pic_date_text);
    }

    private void setPickDateSelected() {
        setDarkColorLayout(saturday_layout, coming_saturday);
        setDarkColorLayout(sunday_layout, coming_sunday);
        setLightColorLayout(datePick, pic_date_text);
    }

    public void goAhead() {
        if (getFromPrefs(WudStayConstants.NAME) == null || getFromPrefs(WudStayConstants.NAME).equals("")) {
            WudstayDialogs enterMobileDialog = new WudstayDialogs(ctx);
            enterMobileDialog.showEnterMobileNoNewDialog("BookingConfirmationActivity");
        } else {
//            guestName = getFromPrefs(WudStayConstants.NAME);
//            guestEmail = getFromPrefs(WudStayConstants.EMAIL);
//            mobileNumber = getFromPrefs(WudStayConstants.MOBILE);

//            processPayment();
            getPayuRequestParameters();
        }

    }


    private void addPermissionDialogMarshMallowForSMS() {
        ArrayList<String> permissions = new ArrayList<>();
        int resultCode = 0;

        permissions.add(RECEIVE_SMS);
        resultCode = RECEIVE_SMS_RESULT;

        //filter out the permissions we have already accepted
        permissionsToRequest = findUnAskedPermissions(permissions);
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.
        permissionsRejected = findRejectedPermissions(permissions);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (permissionsToRequest.size() > 0) {//we need to ask for permissions
                //but have we already asked for them?
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                //mark all these as asked..
                for (String perm : permissionsToRequest) {
                    markAsAsked(perm, sharedPreferences);
                }
            } else {
                //show the success banner
                if (permissionsRejected.size() < permissions.size()) {
                    //this means we can show success because some were already accepted.
                    //permissionSuccess.setVisibility(View.VISIBLE);
                    goAhead();
                }

                if (permissionsRejected.size() > 0) {
                    //we have none to request but some previously rejected..tell the user.
                    //It may be better to show a dialog here in a prod application

                    requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                    //mark all these as asked..
                    for (String perm : permissionsToRequest) {
                        markAsAsked(perm, sharedPreferences);
                    }
                }
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case RECEIVE_SMS_RESULT:
                if (hasPermission(RECEIVE_SMS)) {
                    //        permissionSuccess.setVisibility(View.VISIBLE);
                    goAhead();
                } else {
                    permissionsRejected.add(RECEIVE_SMS);
                    clearMarkAsAsked(RECEIVE_SMS);
                    String message = "permission for send and view SMS was rejected. Please allow to run the app.";
                    makePostRequestSnack(message, permissionsRejected.size());
                }
                break;
        }
    }

    private void showDatePicker() {
        DatePickerDialog dialog = new DatePickerDialog(ctx, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker arg0, int year, int month, int day) {
                myCalendar.set(year, month, day);
                ctx.year = year;
                ctx.month = month;
                ctx.day = day;
                updateLabel();
            }
        }, year, month, day);
        dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        dialog.show();
    }

    private void updateLabel() {
        SimpleDateFormat sdf = getDateFormat();
        visitDate = sdf.format(myCalendar.getTime());
        DateFormat outputFormat = new SimpleDateFormat("dd/MMMM");
        String inputDateStr = sdf.format(myCalendar.getTime());
        Date date = null;
        try {
            date = sdf.parse(inputDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outputDateStr = outputFormat.format(date);
        String[] separated = outputDateStr.split("/");
        pic_date_text.setText(separated[0] + getDayOfMonthSuffix(Integer.parseInt(separated[0])) + "\n" + separated[1]);
    }

    public void getPayuRequestParameters() {
        getWudstayURL();
        if (categoryId==101)
        {
            occupancy_type="";
            for (int i = 0; i < roomOccupancyList.size(); i++) {
                if (roomOccupancyList.get(i).isMflag()) {
                    occupancy_type += String.valueOf(roomOccupancyList.get(i).getId()) + ",";
                }
            }
            if (occupancy_type.length() > 0)
                occupancy_type = occupancy_type.substring(0, occupancy_type.length() - 1);
        }
        else if (categoryId==201)
        {
            occupancy_type="";
            for (int i = 0; i < propCatsList.size(); i++) {
                if (propCatsList.get(i).isChecked()) {
                    occupancy_type += String.valueOf(propCatsList.get(i).getCategoryId()) + ",";
                }
            }


            if (occupancy_type.length() > 0)
                occupancy_type = occupancy_type.substring(0, occupancy_type.length() - 1);
        }


        pgId = ""+pgDetail.getId();
        if (cd.isConnectingToInternet()) {
            final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<PayURequestParametersPojo> call = apiService.getPayuRequestParameters(getFromPrefs(WudStayConstants.NAME), getFromPrefs(WudStayConstants.EMAIL), getFromPrefs(WudStayConstants.MOBILE),
                    pgId, pgDetail.getTokenAmount()+"", WudStayConstants.SOURCE, occupancy_type, "", visitDate, "44fZt5:"+getFromPrefs(WudStayConstants.MOBILE));
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<PayURequestParametersPojo>() {
                @Override
                public void onResponse(Call<PayURequestParametersPojo> call, Response<PayURequestParametersPojo> response) {
                    if (response.body().getResponseCode() == 1) {
                        transaction_id = response.body().getResponseObject().getPayUVO().getTransactionId();
                        saveIntoPrefs("transaction_id", transaction_id);

                        navigateToBaseActivity(response.body().getResponseObject());
                    }
                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<PayURequestParametersPojo> call, Throwable t) {
                    // Log error here since request failed
                    System.out.println("retrofit hh failure" + t.getMessage());
                    progressDialog.getlDialog().dismiss();
                }
            });
        } else {
            dialog.displayCommonDialog(WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }

    private void navigateToBaseActivity(PayURequestDataPojo mPaymentParamsPojo) {
        intent = new Intent(this, PayUBaseActivity.class);
        mPaymentParams = new PaymentParams();
        payuConfig = new PayuConfig();

        PayUVOPojo payUVO = mPaymentParamsPojo.getPayUVO();
        PayuHashsPojo payUHashVO = mPaymentParamsPojo.getPayuHashs();

        mPaymentParams.setKey(payUVO.getMerchantKey());
        mPaymentParams.setAmount(payUVO.getAmount() + "");
        mPaymentParams.setProductInfo(payUVO.getProductInfo());
        mPaymentParams.setFirstName(payUVO.getFirstName());
        mPaymentParams.setEmail(payUVO.getEmail());
        mPaymentParams.setTxnId(payUVO.getTransactionId());
        mPaymentParams.setSurl(payUVO.getSurl());
        mPaymentParams.setFurl(payUVO.getFurl());
        mPaymentParams.setUdf1(payUVO.getUdf1());
        mPaymentParams.setUdf2("");
        mPaymentParams.setUdf3("");
        mPaymentParams.setUdf4("");
        mPaymentParams.setUdf5("");
        mPaymentParams.setUserCredentials(payUVO.getMerchantKey()+":"+getFromPrefs(WudStayConstants.MOBILE));

        payuConfig.setEnvironment(PayuConstants.PRODUCTION_ENV);

        PayuHashes payuHashes = new PayuHashes();
        payuHashes.setPaymentHash(payUHashVO.getPaymentHash());
        payuHashes.setMerchantIbiboCodesHash(payUHashVO.getMerchantCodesHash());
        payuHashes.setPaymentRelatedDetailsForMobileSdkHash(payUHashVO.getDetailsForMobileSdk());
        payuHashes.setVasForMobileSdkHash(payUHashVO.getMobileSdk());
        payuHashes.setDeleteCardHash(payUHashVO.getDeleteHash());
        payuHashes.setEditCardHash(payUHashVO.getEditUserCardHash());
        payuHashes.setSaveCardHash(payUHashVO.getSaveUserCardHash());

        launchSdkUI(payuHashes);
    }

    public void launchSdkUI(PayuHashes payuHashes) {

        mPaymentParams.setHash(payuHashes.getPaymentHash());

        intent.putExtra(PayuConstants.PAYU_CONFIG, payuConfig);
        intent.putExtra(PayuConstants.PAYMENT_PARAMS, mPaymentParams);
        intent.putExtra(PayuConstants.PAYU_HASHES, payuHashes);

        intent.putExtra(PayuConstants.ENV, PayuConstants.PRODUCTION_ENV);

        Payu.setInstance(this);

        startActivityForResult(intent, PayuConstants.PAYU_REQUEST_CODE);
    }

    @Override
    public void onStart() {
        super.onStart();
        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WudStayConstants.ACTION_RECEIVED_OTP);
        bManager.registerReceiver(OTPBroadcastReceiver, intentFilter);
    }

    public void onStop() {
        super.onStop();
        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(this);
        bManager.unregisterReceiver(OTPBroadcastReceiver);
    }

    BroadcastReceiver OTPBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(WudStayConstants.ACTION_RECEIVED_OTP)) {
                if (otpEditText != null) {
                    otpEditText.setText(intent.getStringExtra("otpcode"));
                    submitOTP(otpEditText);
                }
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PayuConstants.PAYU_REQUEST_CODE) {
            if (data != null) {
                if (resultCode == RESULT_OK) {
                    if (cd.isConnectingToInternet()) {
                        getIosPayuResponse();
                    } else {
                        dialog.displayCommonDialog(WudStayConstants.NO_INTERNET_CONNECTED);
                    }
                } else {
                    if (!data.getStringExtra("result").contains("status=failure")) {
                        new AlertDialog.Builder(this)
                                .setCancelable(false)
                                .setMessage(data.getStringExtra("result"))
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {

                                    }
                                }).show();
                    } else {
                        new AlertDialog.Builder(this)
                                .setCancelable(false)
                                .setMessage("Transaction failed, please try again")
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {

                                    }
                                }).show();
                    }
                }

            } else {
//                Toast.makeText(this, "Could not receive data", Toast.LENGTH_LONG).show();
                dialog.displayCommonDialog("Transaction cancelled by User");
            }
        }
    }

    private void getIosPayuResponse() {
        final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
        progressDialog.showProgressDialog(ctx);
        progressDialog.getlDialog().setCancelable(Boolean.FALSE);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<PayUResponsePojo> call = apiService.getPayuResponse(getFromPrefs("transaction_id"));
        System.out.println("retrofit URL " + call.request());
        call.enqueue(new Callback<PayUResponsePojo>() {
            @Override
            public void onResponse(Call<PayUResponsePojo> call, Response<PayUResponsePojo> response) {
                if (response.body().getResponseCode() == 1) {

                    if (response.body().getResponseObject() != null) {
                        Toast.makeText(ctx, "Your payment is successful", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(ctx, HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent);
                        finish();
                    } else {
//                        Toast.makeText(BookingConfirmationActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
                progressDialog.getlDialog().dismiss();
            }

            @Override
            public void onFailure(Call<PayUResponsePojo> call, Throwable t) {
                // Log error here since request failed
                System.out.println("retrofit hh failure" + t.getMessage());
                progressDialog.getlDialog().dismiss();
            }
        });
    }

    @Override
    public void onSuccess(FavoriteResponsePojo obj) {
        if (obj != null && obj.getResponseObject().isActive()) {
            favorite.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.favorite_red));
            pgDetail.setFavouritePg(true);
        } else {
            favorite.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.favorite));
            pgDetail.setFavouritePg(false);
        }
    }

    @Override
    public void onError() {

    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("pgDetail",pgDetail);
        setResult(RESULT_OK,returnIntent);
        finish();
    }

    private void showPayonlineDialog(){
        final WudstayDialogs d = new WudstayDialogs(ctx);
        d.showPaymentModeDialog(this, mPayuResponse);
    }

    @Override
    public void onPaymentRelatedDetailsResponse(PayuResponse payuResponse) {
        mPayuResponse = payuResponse;
//        payuGateway.progressDialog.closeDialog();
        if(payuResponse.isResponseAvailable() && payuResponse.getResponseStatus().getCode() == PayuErrors.NO_ERROR){ // ok we are good to go
            showPayonlineDialog();
        }else{
//            Toast.makeText(this, "Something went wrong : " + payuResponse.getResponseStatus().getResult(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.tenant_layout:
                int  index_tenant = (int)view.getTag(R.string.key);
                ArrayList<PropertyTypesListPojo> data_tenant_list = (ArrayList<PropertyTypesListPojo>)view.getTag(R.string.data);
                genderId = data_tenant_list.get(index_tenant).getPgTypeId();
                for (int i=0; i < data_tenant_list.size(); i++) {

                    if (i == index_tenant)
                    {
                        data_tenant_list.get(i).setChecked(true);
                    }
                    else
                    {
                        data_tenant_list.get(i).setChecked(false);
                    }
                }
                tenant_adapter.notifyDataSetChanged();
                System.out.println("hh genderId  outer is "+genderId);
                break;

            case R.id.flat_layout:
                int  index_flat = (int)view.getTag(R.string.key);
                PropertyCategoryListPojo data_flat_list = (PropertyCategoryListPojo)view.getTag(R.string.data);
//                occupancy_type = ""+data_flat_list.getCategoryId();
                if (data_flat_list.isChecked())
                {
                    int check = 0;
                    for (int i = 0; i < propCatsList.size(); i++) {
                        if (propCatsList.get(i).isChecked())
                            check = check + 1;
                    }
                    if (check >= 2)
                        data_flat_list.setChecked(false);
                }
                else
                {
                    data_flat_list.setChecked(true);
                }
                flat_adapter.notifyDataSetChanged();
                System.out.println("hh flatTypeId  outer is "+occupancy_type);
                break;

            case R.id.booking_layout:
                int  index_booking = (int)view.getTag(R.string.key);
                RoomOccupancyListPojo data_booking_list = (RoomOccupancyListPojo)view.getTag(R.string.data);
//                occupancy_type = ""+data_booking_list.getId();
                if (data_booking_list.isMflag())
                {
                    int check = 0;
                    for (int i = 0; i < roomOccupancyList.size(); i++) {
                        if (roomOccupancyList.get(i).isMflag())
                            check = check + 1;
                    }
                    if (check >= 2)
                        data_booking_list.setMflag(false);
                }
                else
                {
                    data_booking_list.setMflag(true);
                }
                booking_adapter.notifyDataSetChanged();
                System.out.println("hh bookingTypeId  outer is "+occupancy_type);
                break;

            default:
                break;

        }
        /*Intent intent;
        int id = view.getId();
        if(id == R.id.linear_layout_credit_debit_card) {
            if (payuGateway != null) {
                Dialog d = (Dialog) view.getTag();
                if (d != null) {
                    d.dismiss();
                }
                intent = new Intent(ctx, PayUCreditDebitCardActivity.class);
                intent.putParcelableArrayListExtra(PayuConstants.CREDITCARD, mPayuResponse.getCreditCard());
                intent.putParcelableArrayListExtra(PayuConstants.DEBITCARD, mPayuResponse.getDebitCard());
                payuGateway.launchActivity(intent);
            }
        }
        else if(id == R.id.linear_layout_saved_card){
            if (payuGateway != null) {
                Dialog d = (Dialog) view.getTag();
                if (d != null) {
                    d.dismiss();
                }
                intent = new Intent(ctx, PayUStoredCardsActivity.class);
                intent.putParcelableArrayListExtra(PayuConstants.STORED_CARD, mPayuResponse.getStoredCards());
                payuGateway.launchActivity(intent);
            }
        }
        else if(id == R.id.linear_layout_netbanking){// this button is inside Pay online dialog, just handling the dialog button click here
            if (payuGateway != null) {
                Dialog d = (Dialog) view.getTag();
                if (d != null) {
                    d.dismiss();
                }
                intent = new Intent(ctx, PayUNetBankingActivity.class);
                intent.putParcelableArrayListExtra(PayuConstants.NETBANKING, mPayuResponse.getNetBanks());
                payuGateway.launchActivity(intent);
            }
        }else if(id == R.id.linear_layout_payumoney){// this button is inside Pay online dialog, just handling the dialog button click here
            if(payuGateway!=null){
                Dialog d = (Dialog) view.getTag();
                if(d!=null){
                    d.dismiss();
                }
                payuGateway.launchPayumoney();
            }
        }*/
    }

    /*public void processPayment(){
        pgId = pgDetail.getId();
        if (cd.isConnectingToInternet()) {
            // first get payu data from payu sdk and then show the dialog
            payuGateway = new PayuGateaway(ctx, pgId, pgDetail.getTokenAmount()+"", occupancy_type, visitDate, getFromPrefs(WudStayConstants.NAME)
                    , getFromPrefs(WudStayConstants.EMAIL), getFromPrefs(WudStayConstants.MOBILE));
            payuGateway.navigateToPayUSdk();
        }
        else {
            dialog.displayCommonDialog(WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }*/
}
