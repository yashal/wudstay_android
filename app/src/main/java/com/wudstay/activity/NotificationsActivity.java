package com.wudstay.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.wudstay.R;
import com.wudstay.adapter.NotificationListAdapter;
import com.wudstay.model.ApiClient;
import com.wudstay.model.ApiInterface;
import com.wudstay.pojo.NotificationListPojo;
import com.wudstay.pojo.NotificationPojo;
import com.wudstay.pojo.NotificationReadPojo;
import com.wudstay.util.ConnectionDetector;
import com.wudstay.util.WudStayConstants;
import com.wudstay.util.WudstayDialogs;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationsActivity extends BaseActivity implements View.OnClickListener {

    private ImageView back;
    private NotificationsActivity ctx = this;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private ConnectionDetector cd;
    private WudstayDialogs dialog;
    private ArrayList<NotificationListPojo> notificationList;
    private LinearLayout no_notification_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        setDrawerAndToolbar("Notifications");
        WudStayConstants.ACTIVITIES.add(ctx);

        getURL();

        recyclerView = (RecyclerView) findViewById(R.id.notification_recycler_view);
        no_notification_layout = (LinearLayout) findViewById(R.id.no_notification_layout);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);

        cd = new ConnectionDetector(ctx);
        dialog = new WudstayDialogs(ctx);

        getNotificationHistory();
    }

    private void getNotificationHistory() {
        if (cd.isConnectingToInternet()) {
            final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<NotificationPojo> call = apiService.getUserNotificationList(ctx.getFromPrefs(WudStayConstants.MOBILE));
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<NotificationPojo>() {
                @Override
                public void onResponse(Call<NotificationPojo> call, Response<NotificationPojo> response) {
                    if (response.body().getResponseCode() == 1) {
                        if (response.body().getResponseObject() != null) {
                            notificationList = response.body().getResponseObject().getListOfComplaints();
                            if (notificationList != null && notificationList.size() > 0) {
                                mAdapter = new NotificationListAdapter(ctx, notificationList);
                                recyclerView.setAdapter(mAdapter);
                                no_notification_layout.setVisibility(View.GONE);
                            } else {
                                no_notification_layout.setVisibility(View.VISIBLE);
                            }
                        } else {
                            no_notification_layout.setVisibility(View.VISIBLE);
                        }
                    } else {
                        no_notification_layout.setVisibility(View.VISIBLE);
                    }
                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<NotificationPojo> call, Throwable t) {
                    // Log error here since request failed.
                    System.out.println("hh failure retrofit URL " + t.getMessage());
                    progressDialog.getlDialog().dismiss();
                }
            });
        } else {
            dialog.displayCommonDialog(WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getURL();
    }

    @Override
    public void onClick(View view) {

        NotificationListPojo data_notification = (NotificationListPojo) view.getTag(R.string.key);

        // for unread notifications
        if (data_notification.getStatus() == 0) {
            getReadMarkNotification(data_notification);
        }
    }

    private void getReadMarkNotification(final NotificationListPojo data) {
        String str_id = "" + data.getId();
        if (cd.isConnectingToInternet()) {
            final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<NotificationReadPojo> call = apiService.readMarkNotification(ctx.getFromPrefs(WudStayConstants.MOBILE), str_id);
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<NotificationReadPojo>() {
                @Override
                public void onResponse(Call<NotificationReadPojo> call, Response<NotificationReadPojo> response) {
                    if (response.body().getResponseCode() == 1) {
                        if (response.body().getResponseObject() != null) {
                            data.setStatus(1);
                            mAdapter.notifyDataSetChanged();
                        }
                    }
                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<NotificationReadPojo> call, Throwable t) {
                    // Log error here since request failed.
                    System.out.println("hh failure retrofit URL " + t.getMessage());
                    progressDialog.getlDialog().dismiss();
                }
            });
        } else {
            dialog.displayCommonDialog(WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }
}