package com.wudstay.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.wudstay.R;
import com.wudstay.adapter.PGDetailPagerNewAdapter;
import com.wudstay.model.ApiClient;
import com.wudstay.model.ApiInterface;
import com.wudstay.pojo.FavoriteResponsePojo;
import com.wudstay.pojo.PGDetailNewPojo;
import com.wudstay.pojo.PGDetailResponseNewPojo;
import com.wudstay.pojo.PropertyCategoryListPojo;
import com.wudstay.pojo.PropertyTypesListPojo;
import com.wudstay.pojo.RoomOccupancyListPojo;
import com.wudstay.util.ConnectionDetector;
import com.wudstay.util.WudStayConstants;
import com.wudstay.util.WudstayDialogs;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PgDetailNewActivity extends BaseActivity implements OnMapReadyCallback, BookmarkCallback {

    private ViewPager viewPager;
    private ConnectionDetector cd;
    private WudstayDialogs dialog;
    private PgDetailNewActivity ctx = this;
    private int id;
    private PGDetailNewPojo pgDetail;
    private TextView counter, pg_detail_PGName, pg_detail_Address, how_to_reach;
    private LinearLayout description_layout,sold_outLayout,scheduleVisit,bookNow;
    private HorizontalScrollView scrollView, occupancy_list;
    private ImageView favorite;
    private int height;
    private MapFragment mapFragment;
    private GoogleMap googleMap;
    private RelativeLayout mapLayout;
    private String visitDate = "";
    private String genderId = "";
    private String occupancy_type = "";
    private ArrayList<PropertyCategoryListPojo> propCatsList;
    private ArrayList<RoomOccupancyListPojo> roomOccupancyList;
    private ArrayList<PropertyTypesListPojo> propTypesList;
    private int categoryId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pg_detail_new);

        setDrawerAndToolbar("Pg Details");
        WudStayConstants.ACTIVITIES.add(ctx);

        getWudstayURL();

        viewPager = (ViewPager) findViewById(R.id.pgdetail_pager);
        counter = (TextView) findViewById(R.id.counter);
        pg_detail_PGName = (TextView) findViewById(R.id.pg_detail_PGName);
        pg_detail_Address = (TextView) findViewById(R.id.pg_detail_Address);
        how_to_reach = (TextView) findViewById(R.id.how_to_reach);
        description_layout = (LinearLayout) findViewById(R.id.description_layout);
        sold_outLayout = (LinearLayout) findViewById(R.id.sold_out);
        scrollView = (HorizontalScrollView) findViewById(R.id.facilities_list);
        occupancy_list = (HorizontalScrollView) findViewById(R.id.occupancy_list);
        ImageView back = (ImageView) findViewById(R.id.back);
        favorite = (ImageView) findViewById(R.id.favorite);
        mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.pgdetails_map);
        mapLayout = (RelativeLayout) findViewById(R.id.mapLayout);
        scheduleVisit = (LinearLayout) findViewById(R.id.scheduleVisit);
        bookNow = (LinearLayout) findViewById(R.id.bookNow);

        cd = new ConnectionDetector(getApplicationContext());
        dialog = new WudstayDialogs(ctx);

        id = getIntent().getIntExtra("id", 0);
        visitDate = getIntent().getStringExtra("visitDate");
        genderId = getIntent().getStringExtra("genderId");
//        occupancy_type = getIntent().getStringExtra("occupancy_type");

        getPgDetails(id);

        if (getIntent().getSerializableExtra("propCatsList") != null) {
            propCatsList = (ArrayList<PropertyCategoryListPojo>) getIntent().getSerializableExtra("propCatsList");
        } else {
            propCatsList = new ArrayList<>();
        }
        if (getIntent().getSerializableExtra("roomOccupancyList") != null) {
            roomOccupancyList = (ArrayList<RoomOccupancyListPojo>) getIntent().getSerializableExtra("roomOccupancyList");
        } else {
            roomOccupancyList = new ArrayList<>();
        }
        if (getIntent().getSerializableExtra("propTypesList") != null) {
            propTypesList = (ArrayList<PropertyTypesListPojo>) getIntent().getSerializableExtra("propTypesList");
        } else {
            propTypesList = new ArrayList<>();
        }
        categoryId = (int) getIntent().getLongExtra("occupancy_id", 0);
        DisplayMetrics metrics = new DisplayMetrics();
        ctx.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        height = metrics.heightPixels / 4;

        final NestedScrollView mainScrollView = (NestedScrollView) findViewById(R.id.main_scrollview);
        ImageView transparentImageView = (ImageView) findViewById(R.id.transparent_image);

        transparentImageView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        mainScrollView.requestDisallowInterceptTouchEvent(true);
                        // Disable touch on transparent view
                        return false;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        mainScrollView.requestDisallowInterceptTouchEvent(false);
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        mainScrollView.requestDisallowInterceptTouchEvent(true);
                        return false;

                    default:
                        return true;
                }
            }
        });


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddRemoveFavoriteNew(pgDetail, ctx);
            }
        });

        scheduleVisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(pgDetail.getNoOfRooms()==0) {}else{
                    Intent intent = new Intent(ctx, ScheduleVisitActivity.class);
                    intent.putExtra("pgDetail", pgDetail);
                    startActivityForResult(intent, 2);
                }
            }
        });

        bookNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(pgDetail.getNoOfRooms()==0) {}else {
                    Intent intent = new Intent(ctx, BookingConfirmationActivity.class);
                    intent.putExtra("pgDetail", pgDetail);
                    intent.putExtra("visitDate", visitDate);
                    intent.putExtra("genderId", genderId);
//                intent.putExtra("occupancy_type", occupancy_type);
                    intent.putExtra("propCatsList", propCatsList);
                    intent.putExtra("roomOccupancyList", roomOccupancyList);
                    intent.putExtra("propTypesList", propTypesList);
                    intent.putExtra("occupancy_id", categoryId);
                    startActivityForResult(intent, 2);
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 2) {
                pgDetail = (PGDetailNewPojo) data.getSerializableExtra("pgDetail");
                if (pgDetail.isFavouritePg())
                    favorite.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.favorite_red));
                else
                    favorite.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.favorite));
            }
        }
        if (resultCode == RESULT_CANCELED) {
            //Write your code if there's no result
        }
    }

    private void setToolBarValues() {
        CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapse_toolbar);
        collapsingToolbar.setTitle(pgDetail.getDisplayName());

        collapsingToolbar.setCollapsedTitleTextColor(ContextCompat.getColor(ctx, R.color.white));
        collapsingToolbar.setExpandedTitleColor(ContextCompat.getColor(ctx, R.color.white));

        int height = (int) (getScreenHeight() / 2.4);

        AppBarLayout appbar = (AppBarLayout) findViewById(R.id.MyAppbar);
        appbar.requestLayout();
        appbar.getLayoutParams().height = height;
    }

    private void getPgDetails(int pgId) {
        if (cd.isConnectingToInternet()) {
            final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<PGDetailResponseNewPojo> call = apiService.getPGDetailNew("" + pgId, getFromPrefs(WudStayConstants.MOBILE));
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<PGDetailResponseNewPojo>() {
                @Override
                public void onResponse(Call<PGDetailResponseNewPojo> call, final Response<PGDetailResponseNewPojo> response) {
                    if (response.body() != null && response.body().getResponseCode() == 1) {

                        pgDetail = response.body().getResponseObject();
                        if (pgDetail.isFavouritePg())
                            favorite.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.favorite_red));
                        else
                            favorite.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.favorite));
                        setToolBarValues();
                        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                            @Override
                            public void onPageSelected(int position) {

                            }

                            @Override
                            public void onPageScrolled(int arg0, float arg1, int arg2) {
                                counter.setText(arg0 + 1 + "  of  " + pgDetail.getImageList().size());
                            }

                            @Override
                            public void onPageScrollStateChanged(int arg0) {
                            }
                        });
                        pg_detail_PGName.setText(pgDetail.getDisplayName());
                        pg_detail_Address.setText(pgDetail.getDisplayAddress());
                        how_to_reach.setText(pgDetail.getHowToReach());
                        //17/07/2017
                            if(pgDetail.getNoOfRooms()==0){
                                 scheduleVisit.setVisibility(View.GONE);
                                 bookNow.setVisibility(View.GONE);
                                 sold_outLayout.setVisibility(View.VISIBLE);
                            }
                        ////////////
                        setDescription();
                        setFacilities();
                        setOccupancy();
                        mapFragment.getMapAsync(ctx);
                        mapLayout.requestLayout();
                        mapLayout.getLayoutParams().height = height;

                        if (response.body().getResponseObject().getImageList() != null && response.body().getResponseObject().getImageList().size() > 0) {
                            PGDetailPagerNewAdapter adapter = new PGDetailPagerNewAdapter(ctx, response.body().getResponseObject().getImageList(), 600);
                            viewPager.setAdapter(adapter);
                        }

                    }
                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<PGDetailResponseNewPojo> call, Throwable t) {
                    // Log error here since request failed
                    System.out.println("retrofit hh failure " + t.getMessage());
                    progressDialog.getlDialog().dismiss();
                }
            });
        } else {
            dialog.displayCommonDialog(WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }

    private void setDescription() {
        if (pgDetail.getDescriptionList() != null && pgDetail.getDescriptionList().size() > 0) {
            for (int i = 0; i < pgDetail.getDescriptionList().size(); i++) {
                TextView tv_title = new TextView(ctx);
                tv_title.setText(pgDetail.getDescriptionList().get(i).getDescriptionTitle());
                tv_title.setTextSize(getResources().getDimension(R.dimen.title_desc));
                tv_title.setTypeface(null, Typeface.BOLD);

                LinearLayout.LayoutParams titleLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                titleLayoutParams.setMargins(0, 30, 0, 0);
                tv_title.setLayoutParams(titleLayoutParams);
                tv_title.setTextColor(ContextCompat.getColor(ctx, R.color.complain_text_color));

                TextView tv_desc = new TextView(ctx);
                tv_desc.setText(pgDetail.getDescriptionList().get(i).getDescription());
                tv_desc.setTextSize(getResources().getDimension(R.dimen.text_desc));
                tv_desc.setTextColor(ContextCompat.getColor(ctx, R.color.desc_color));

                LinearLayout.LayoutParams descLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                descLayoutParams.setMargins(0, 14, 0, 0);
                tv_desc.setLayoutParams(descLayoutParams);

                description_layout.addView(tv_title);
                description_layout.addView(tv_desc);
            }
        }
    }

    private void setFacilities() {
        LinearLayout topLinearLayout = new LinearLayout(this);
        // topLinearLayout.setLayoutParams(android.widget.LinearLayout.LayoutParams.FILL_PARENT,android.widget.LinearLayout.LayoutParams.FILL_PARENT);
        topLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
        topLinearLayout.setPadding(5, 5, 5, 5);
        for (int i = 0; i < pgDetail.getAmenityList().size(); i++) {

            LinearLayout innerLayout = new LinearLayout(this);
            innerLayout.setOrientation(LinearLayout.VERTICAL);
            LinearLayout.LayoutParams innerLayoutLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            innerLayoutLayoutParams.setMargins(0, 14, 30, 0);

            innerLayout.setLayoutParams(innerLayoutLayoutParams);

            final ImageView imageView = new ImageView(this);

            LinearLayout.LayoutParams imageLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            imageLayoutParams.gravity = Gravity.CENTER;
            imageView.setLayoutParams(imageLayoutParams);
            imageView.setImageResource(getImage(pgDetail.getAmenityList().get(i).getLinkedId()));

            TextView facility_name = new TextView(ctx);
            facility_name.setText(pgDetail.getAmenityList().get(i).getName());
            facility_name.setTextSize(getResources().getDimension(R.dimen.text_desc));
            facility_name.setTextColor(ContextCompat.getColor(ctx, R.color.desc_color));

            LinearLayout.LayoutParams descLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            descLayoutParams.setMargins(0, 5, 0, 0);
            descLayoutParams.gravity = Gravity.CENTER;
            facility_name.setLayoutParams(descLayoutParams);

            innerLayout.addView(imageView);
            innerLayout.addView(facility_name);
            topLinearLayout.addView(innerLayout);

            imageView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Log.e("Tag", "" + imageView.getTag());
                }
            });

        }
        scrollView.addView(topLinearLayout);
    }

    private void setOccupancy() {
        LinearLayout topLinearLayout = new LinearLayout(this);
        LinearLayout.LayoutParams topLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
         topLinearLayout.setLayoutParams(topLayoutParams);
        topLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
        topLinearLayout.setPadding(5, 5, 5, 5);
        for (int i = 0; i < pgDetail.getOccupancyList().size(); i++) {

            LinearLayout innerLayout = new LinearLayout(this);
            innerLayout.setOrientation(LinearLayout.VERTICAL);
            LinearLayout.LayoutParams innerLayoutLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            innerLayoutLayoutParams.setMargins(0, 14, 30, 0);

            innerLayout.setLayoutParams(innerLayoutLayoutParams);

            TextView occupancy_price = new TextView(ctx);
            occupancy_price.setText(getResources().getString(R.string.Rs) + " " + pgDetail.getOccupancyList().get(i).getPrice() + " p.m.");
            occupancy_price.setTextSize(getResources().getDimension(R.dimen.text_desc));
            occupancy_price.setTextColor(ContextCompat.getColor(ctx, R.color.black));

            TextView occupancy_name = new TextView(ctx);
            occupancy_name.setText(pgDetail.getOccupancyList().get(i).getOccupancyName());
            occupancy_name.setTextSize(getResources().getDimension(R.dimen.text_desc));
            occupancy_name.setTextColor(ContextCompat.getColor(ctx, R.color.desc_color));

//            LinearLayout.LayoutParams descLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//            descLayoutParams.setMargins(0, 5, 0, 0);
//            descLayoutParams.gravity = Gravity.CENTER;
//            occupancy_name.setLayoutParams(descLayoutParams);

            innerLayout.addView(occupancy_price);
            innerLayout.addView(occupancy_name);
            topLinearLayout.addView(innerLayout);


        }
        occupancy_list.addView(topLinearLayout);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;

        // Add a marker in Sydney, Australia, and move the camera.
        this.googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//        this.googleMap.setMyLocationEnabled(true);
        this.googleMap.getUiSettings().setZoomControlsEnabled(false);
        this.googleMap.getUiSettings().setCompassEnabled(false);
        this.googleMap.getUiSettings().setZoomGesturesEnabled(true);

        udpateMapView();
    }

    private void udpateMapView() {
        CameraPosition cameraPosition = null;
        if (googleMap != null) {
//            googleMap.clear();
            googleMap.addMarker(new MarkerOptions().position(new LatLng(pgDetail.getLatitude(), pgDetail.getLongitude())).icon(BitmapDescriptorFactory.fromResource(R.mipmap.map_pin))
                    .title(pgDetail.getDisplayName()).snippet(pgDetail.getDisplayAddress()));
            cameraPosition = new CameraPosition.Builder().target(
                    new LatLng(pgDetail.getLatitude(), pgDetail.getLongitude())).zoom(13).build();
            if (cameraPosition != null) {
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 2000, null);
            }
        }
    }

    @Override
    public void onSuccess(FavoriteResponsePojo obj) {
        if (obj != null && obj.getResponseObject().isActive()) {
            favorite.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.favorite_red));
            pgDetail.setFavouritePg(true);
        } else {
            favorite.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.favorite));
            pgDetail.setFavouritePg(false);
        }
    }

    @Override
    public void onError() {

    }
}
