package com.wudstay.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.wudstay.R;
import com.wudstay.adapter.PGDetailPagerAdapter;
import com.wudstay.pojo.FavoriteResponsePojo;
import com.wudstay.pojo.PgDetailPojo;
import com.wudstay.util.WudStayConstants;

public class PGDetailActivity extends BaseActivity implements OnMapReadyCallback, BookmarkCallback {

    private ViewPager viewPager;
    private GoogleMap googleMap;
    private PgDetailPojo pgDetail;
    private int height;
    private PGDetailActivity ctx = this;
    private String visitDate = "";
    private String genderId = "";
    private String occupancy_type = "";
    private LinearLayout description_layout;
    private ImageView favorite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pg_details);

        setDrawerAndToolbar("Pg Details");
        WudStayConstants.ACTIVITIES.add(ctx);

        viewPager = (ViewPager) findViewById(R.id.pgdetail_pager);

        pgDetail = (PgDetailPojo) getIntent().getSerializableExtra("pgDetail");
        visitDate = getIntent().getStringExtra("visitDate");
        genderId = getIntent().getStringExtra("genderId");
        occupancy_type = getIntent().getStringExtra("occupancy_type");

        PGDetailPagerAdapter adapter = new PGDetailPagerAdapter(this, pgDetail.getRoomImagesOthers(), 600);
        viewPager.setAdapter(adapter);

        ImageView back = (ImageView) findViewById(R.id.back);
        favorite = (ImageView) findViewById(R.id.favorite);

        TextView pg_detail_PGName = (TextView) findViewById(R.id.pg_detail_PGName);
        TextView pg_detail_Address = (TextView) findViewById(R.id.pg_detail_Address);
        TextView pg_detail_price = (TextView) findViewById(R.id.pg_detail_price);
        TextView double_sharing_price = (TextView) findViewById(R.id.double_sharing_price);
        TextView triple_sharing_price = (TextView) findViewById(R.id.triple_sharing_price);
        TextView how_to_reach = (TextView) findViewById(R.id.how_to_reach);
        final TextView counter = (TextView) findViewById(R.id.counter);
        LinearLayout scheduleVisit = (LinearLayout) findViewById(R.id.scheduleVisit);
        LinearLayout bookNow = (LinearLayout) findViewById(R.id.bookNow);
        description_layout = (LinearLayout) findViewById(R.id.description_layout);
        HorizontalScrollView scrollView = (HorizontalScrollView) findViewById(R.id.facilities_list);

        if (pgDetail.isFavouritePg())
            favorite.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.favorite_red));
        else
            favorite.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.favorite));

        LinearLayout topLinearLayout = new LinearLayout(this);
        // topLinearLayout.setLayoutParams(android.widget.LinearLayout.LayoutParams.FILL_PARENT,android.widget.LinearLayout.LayoutParams.FILL_PARENT);
        topLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
        topLinearLayout.setPadding(5, 5, 5, 5);
        for (int i = 0; i < pgDetail.getAmenityList().size(); i++) {

            LinearLayout innerLayout = new LinearLayout(this);
            innerLayout.setOrientation(LinearLayout.VERTICAL);
            LinearLayout.LayoutParams innerLayoutLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            innerLayoutLayoutParams.setMargins(0, 14, 30, 0);

            innerLayout.setLayoutParams(innerLayoutLayoutParams);

            final ImageView imageView = new ImageView(this);

            LinearLayout.LayoutParams imageLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            imageLayoutParams.gravity = Gravity.CENTER;
            imageView.setLayoutParams(imageLayoutParams);
            imageView.setImageResource(getImage(pgDetail.getAmenityList().get(i).getLinkedId()));

            TextView facility_name = new TextView(ctx);
            facility_name.setText(pgDetail.getAmenityList().get(i).getName());
            facility_name.setTextSize(getResources().getDimension(R.dimen.text_desc));
            facility_name.setTextColor(ContextCompat.getColor(ctx, R.color.desc_color));

            LinearLayout.LayoutParams descLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            descLayoutParams.setMargins(0, 5, 0, 0);
            descLayoutParams.gravity = Gravity.CENTER;
            facility_name.setLayoutParams(descLayoutParams);

            innerLayout.addView(imageView);
            innerLayout.addView(facility_name);
            topLinearLayout.addView(innerLayout);

            imageView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Log.e("Tag", "" + imageView.getTag());
                }
            });

        }

        scrollView.addView(topLinearLayout);

        DisplayMetrics metrics = new DisplayMetrics();
        ctx.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        height = metrics.heightPixels / 4;

        pg_detail_PGName.setText(pgDetail.getDisplayName());
        pg_detail_Address.setText(pgDetail.getDisplayAddress());
        pg_detail_price.setText(getResources().getString(R.string.Rs) + " " + pgDetail.getPriceSingleOccupancy() + " p.m.");
        double_sharing_price.setText(getResources().getString(R.string.Rs) + " " + pgDetail.getPriceDoubleOccupancy() + " p.m.");
        triple_sharing_price.setText(getResources().getString(R.string.Rs) + " " + pgDetail.getPriceTripleOccupancy() + " p.m.");
        how_to_reach.setText(pgDetail.getHowToReach());
        setDescription();

        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.pgdetails_map);
        mapFragment.getMapAsync(this);

        RelativeLayout mapLayout = (RelativeLayout) findViewById(R.id.mapLayout);
        mapLayout.requestLayout();
        mapLayout.getLayoutParams().height = height;

        final NestedScrollView mainScrollView = (NestedScrollView) findViewById(R.id.main_scrollview);
        ImageView transparentImageView = (ImageView) findViewById(R.id.transparent_image);

        transparentImageView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        mainScrollView.requestDisallowInterceptTouchEvent(true);
                        // Disable touch on transparent view
                        return false;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        mainScrollView.requestDisallowInterceptTouchEvent(false);
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        mainScrollView.requestDisallowInterceptTouchEvent(true);
                        return false;

                    default:
                        return true;
                }
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("pgDetail", pgDetail);
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        });

        favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddRemoveFavorite(pgDetail, ctx);
            }
        });

        bookNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ctx, BookingConfirmationActivity.class);
                intent.putExtra("pgDetail", pgDetail);
                intent.putExtra("visitDate", visitDate);
                intent.putExtra("genderId", genderId);
                intent.putExtra("occupancy_type", occupancy_type);
                startActivityForResult(intent, 2);
            }
        });

        scheduleVisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ctx, ScheduleVisitActivity.class);
                intent.putExtra("pgDetail", pgDetail);
                startActivityForResult(intent, 2);
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                counter.setText(arg0 + 1 + "  of  " + pgDetail.getRoomImagesOthers().size());
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });

        setToolBarValues();
    }

    private void setToolBarValues() {
        CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapse_toolbar);
        collapsingToolbar.setTitle(pgDetail.getDisplayName());

        collapsingToolbar.setCollapsedTitleTextColor(ContextCompat.getColor(ctx, R.color.white));
        collapsingToolbar.setExpandedTitleColor(ContextCompat.getColor(ctx, R.color.white));

        int height = (int) (getScreenHeight() / 2.4);

        AppBarLayout appbar = (AppBarLayout) findViewById(R.id.MyAppbar);
        appbar.requestLayout();
        appbar.getLayoutParams().height = height;
    }

    private void setDescription() {
        if (pgDetail.getDescriptionList() != null && pgDetail.getDescriptionList().size() > 0) {
            for (int i = 0; i < pgDetail.getDescriptionList().size(); i++) {
                TextView tv_title = new TextView(ctx);
                tv_title.setText(pgDetail.getDescriptionList().get(i).getDescriptionTitle());
                tv_title.setTextSize(getResources().getDimension(R.dimen.title_desc));
                tv_title.setTypeface(null, Typeface.BOLD);

                LinearLayout.LayoutParams titleLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                titleLayoutParams.setMargins(0, 30, 0, 0);
                tv_title.setLayoutParams(titleLayoutParams);
                tv_title.setTextColor(ContextCompat.getColor(ctx, R.color.complain_text_color));

                TextView tv_desc = new TextView(ctx);
                tv_desc.setText(pgDetail.getDescriptionList().get(i).getDescription());
                tv_desc.setTextSize(getResources().getDimension(R.dimen.text_desc));
                tv_desc.setTextColor(ContextCompat.getColor(ctx, R.color.desc_color));

                LinearLayout.LayoutParams descLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                descLayoutParams.setMargins(0, 14, 0, 0);
                tv_desc.setLayoutParams(descLayoutParams);

                description_layout.addView(tv_title);
                description_layout.addView(tv_desc);
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;

        // Add a marker in Sydney, Australia, and move the camera.
        this.googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//        this.googleMap.setMyLocationEnabled(true);
        this.googleMap.getUiSettings().setZoomControlsEnabled(false);
        this.googleMap.getUiSettings().setCompassEnabled(false);
        this.googleMap.getUiSettings().setZoomGesturesEnabled(true);

        udpateMapView();
    }

    private void udpateMapView() {
        CameraPosition cameraPosition = null;
        if (googleMap != null) {
//            googleMap.clear();
            googleMap.addMarker(new MarkerOptions().position(new LatLng(pgDetail.getLatitude(), pgDetail.getLongitude())).icon(BitmapDescriptorFactory.fromResource(R.mipmap.map_pin))
                    .title(pgDetail.getDisplayName()).snippet(pgDetail.getDisplayAddress()));
            cameraPosition = new CameraPosition.Builder().target(
                    new LatLng(pgDetail.getLatitude(), pgDetail.getLongitude())).zoom(13).build();
            if (cameraPosition != null) {
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 2000, null);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 2) {
                pgDetail = (PgDetailPojo) data.getSerializableExtra("pgDetail");
                if (pgDetail.isFavouritePg())
                    favorite.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.favorite_red));
                else
                    favorite.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.favorite));
            }
        }
        if (resultCode == RESULT_CANCELED) {
            //Write your code if there's no result
        }
    }

    @Override
    public void onSuccess(FavoriteResponsePojo obj) {
        if (obj != null && obj.getResponseObject().isActive()) {
            favorite.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.favorite_red));
            pgDetail.setFavouritePg(true);
        } else {
            favorite.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.favorite));
            pgDetail.setFavouritePg(false);
        }
    }

    @Override
    public void onError() {

    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("pgDetail", pgDetail);
        setResult(RESULT_OK, returnIntent);
        finish();
    }
}
