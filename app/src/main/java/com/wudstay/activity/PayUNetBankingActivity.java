package com.wudstay.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.payu.india.Model.PaymentDetails;
import com.payu.india.Model.PaymentParams;
import com.payu.india.Model.PayuConfig;
import com.payu.india.Model.PayuHashes;
import com.payu.india.Model.PostData;
import com.payu.india.Payu.Payu;
import com.payu.india.Payu.PayuConstants;
import com.payu.india.Payu.PayuErrors;
import com.payu.india.PostParams.PaymentPostParams;
import com.payu.payuui.Activity.PaymentsActivity;
import com.wudstay.R;
import com.wudstay.adapter.PayUNetBankingAdapter;

import java.util.ArrayList;


public class PayUNetBankingActivity extends AppCompatActivity implements View.OnClickListener {

    private String bankcode;
    private Bundle bundle;
    private ArrayList<PaymentDetails> netBankingList;
    private Spinner spinnerNetbanking;
    //    private String[] netBanksNamesArray;
//    private String[] netBanksCodesArray;
    private PaymentParams mPaymentParams;
    private PayuHashes payuHashes;

    private LinearLayout payNowButton;

    private PayUNetBankingAdapter payUNetBankingAdapter;
    private Toolbar toolbar;
    private PayuConfig payuConfig;

    private TextView amountTextView;
    private TextView transactionIdTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_netbanking);

        // todo lets set the toolbar

        (payNowButton = (LinearLayout) findViewById(R.id.button_pay_now)).setOnClickListener(this);
        spinnerNetbanking = (Spinner) findViewById(R.id.spinner_netbanking);

        Payu.setInstance(this);

         ImageView back = (ImageView) findViewById(R.id.back);

         back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        bundle = getIntent().getExtras();

        if (bundle != null && bundle.getParcelableArrayList(PayuConstants.NETBANKING) != null) {
            netBankingList = new ArrayList<PaymentDetails>();
            netBankingList = bundle.getParcelableArrayList(PayuConstants.NETBANKING);


            payUNetBankingAdapter = new PayUNetBankingAdapter(this, netBankingList);
            spinnerNetbanking.setAdapter(payUNetBankingAdapter);
            spinnerNetbanking.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int index, long l) {
                    bankcode = netBankingList.get(index).getBankCode();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        } else {
            Toast.makeText(this, "Could not get netbanking list Data from the previous activity", Toast.LENGTH_LONG).show();
        }

        mPaymentParams = bundle.getParcelable(PayuConstants.PAYMENT_PARAMS);
        payuHashes = bundle.getParcelable(PayuConstants.PAYU_HASHES);
        payuConfig = bundle.getParcelable(PayuConstants.PAYU_CONFIG);
        payuConfig = null != payuConfig ? payuConfig : new PayuConfig();

        (amountTextView = (TextView) findViewById(R.id.text_view_amount)).setText(PayuConstants.AMOUNT + ": "  + getResources().getString(R.string.Rs) + " " + mPaymentParams.getAmount());
//        (transactionIdTextView = (TextView) findViewById(R.id.text_view_transaction_id)).setText(PayuConstants.TXNID + ": " + mPaymentParams.getTxnId());

    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_pay_now) {
            // okey we need hash fist
            PostData postData = new PostData();
            mPaymentParams.setHash(payuHashes.getPaymentHash());
            mPaymentParams.setBankCode(bankcode);

            try {
                postData = new PaymentPostParams(mPaymentParams, PayuConstants.NB).getPaymentPostParams();
            } catch (Exception e) {
                e.printStackTrace();
            }

//            postData = new NBPostParams(mPaymentParams, mNetBank).getNBPostParams();
            if (postData.getCode() == PayuErrors.NO_ERROR) {
                // launch webview
                payuConfig.setData(postData.getResult());
                Intent intent = new Intent(this, PaymentsActivity.class);
                intent.putExtra(PayuConstants.PAYU_CONFIG, payuConfig);
                startActivityForResult(intent, PayuConstants.PAYU_REQUEST_CODE);
            } else {
                Toast.makeText(this, postData.getResult(), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PayuConstants.PAYU_REQUEST_CODE) {
            setResult(resultCode, data);
            finish();
        }
    }

}

