package com.wudstay.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.payu.india.Model.PayuResponse;
import com.payu.india.Payu.PayuConstants;
import com.wudstay.R;
import com.wudstay.activity.BaseActivity;
import com.wudstay.activity.HomeActivity;
import com.wudstay.activity.RegistrationActivity;
import com.wudstay.model.ApiClient;
import com.wudstay.model.ApiInterface;
import com.wudstay.pojo.IsRegisteredPojo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class WudstayDialogs {

    private Activity ctx;
    private Dialog lDialog;
    private boolean closeActivity = true;

    /*public static ProgressDialog showLoading(Activity activity) {
        ProgressDialog mProgressDialog = new ProgressDialog(activity);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.show();
        return mProgressDialog;
    }*/


    public WudstayDialogs(Activity ctx) {
        this.ctx = ctx;
    }

    public void displayCommonDialog(String msg) {
        Button OkButtonLogout;
        final Dialog DialogLogOut = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        DialogLogOut.setContentView(R.layout.custom_alert_dialog);
        TextView loogout_msg = (TextView) DialogLogOut.findViewById(R.id.text_exit);
        loogout_msg.setText(msg);
        OkButtonLogout = (Button) DialogLogOut.findViewById(R.id.btn_yes_exit);
        Button CancelButtonLogout = (Button) DialogLogOut.findViewById(R.id.btn_no_exit);
        CancelButtonLogout.setVisibility(View.GONE);

        OkButtonLogout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                DialogLogOut.dismiss();
            }
        });
        CancelButtonLogout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                DialogLogOut.dismiss();
            }
        });
        DialogLogOut.show();
    }

    public void displayDialogWithCancel(String msg) {
        Button OkButtonLogout;
        final Dialog DialogLogOut = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        DialogLogOut.setContentView(R.layout.custom_alert_dialog);
        TextView loogout_msg = (TextView) DialogLogOut.findViewById(R.id.text_exit);
        loogout_msg.setText(msg);
        OkButtonLogout = (Button) DialogLogOut.findViewById(R.id.btn_yes_exit);
        Button CancelButtonLogout = (Button) DialogLogOut.findViewById(R.id.btn_no_exit);

        OkButtonLogout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                DialogLogOut.dismiss();
                for (int i = 0; i < WudStayConstants.ACTIVITIES.size(); i++) {
                    if (WudStayConstants.ACTIVITIES.get(i) != null)
                        WudStayConstants.ACTIVITIES.get(i).finish();
                }
                ((BaseActivity) ctx).saveIntoPrefs(WudStayConstants.NAME, "");
                ((BaseActivity) ctx).saveIntoPrefs(WudStayConstants.MOBILE, "");
                ((BaseActivity) ctx).saveIntoPrefs(WudStayConstants.EMAIL, "");
                ((BaseActivity) ctx).saveIntoPrefs(WudStayConstants.USER_TYPE, "");
                ((BaseActivity) ctx).saveIntoPrefs(WudStayConstants.IMAGE, "");
                Intent intent = new Intent(ctx, HomeActivity.class);
                ctx.startActivity(intent);

            }
        });
        CancelButtonLogout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                DialogLogOut.dismiss();
            }
        });
        DialogLogOut.show();
    }


    public void showEnterMobileNoNewDialog(final String from) {
        lDialog = new Dialog(ctx, R.style.Theme_Dialog_Custom_Anim);
        lDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        lDialog.setContentView(R.layout.dialog_enter_mobile_layout);
        lDialog.setCanceledOnTouchOutside(true);
        ImageView cancelDialog = (ImageView) lDialog.findViewById(R.id.imgClose);
        final EditText edName = (EditText) lDialog.findViewById(R.id.edName);
        final EditText edNumber = (EditText) lDialog.findViewById(R.id.edNumber);
        final EditText edEmail = (EditText) lDialog.findViewById(R.id.edEmail);
        final TextView header_dialog = (TextView) lDialog.findViewById(R.id.header_dialog);
        final TextView content_dialog = (TextView) lDialog.findViewById(R.id.content_dialog);
        edName.setFocusable(true);
        edName.setFocusableInTouchMode(true);

        edName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    lDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                }
            }
        });
        edName.requestFocus();
        Button btnSubmit = (Button) lDialog.findViewById(R.id.btnSubmit);
        if(from.equals("ScheduleVisitActivity"))
        {
            header_dialog.setText("Please fill the details below to schedule a visit");
            content_dialog.setText("");
            content_dialog.setVisibility(View.GONE);
        }
        else if (!from.equals("ScheduleVisitActivity") && !from.equals("BookingConfirmationActivity")) {
            header_dialog.setText("Please fill the details below");
            content_dialog.setText("");
            content_dialog.setVisibility(View.GONE);
        }

        cancelDialog.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                lDialog.dismiss();
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String name = edName.getText().toString().trim();
                String number = edNumber.getText().toString().trim();
                String email = edEmail.getText().toString().trim();
                if (name.isEmpty()) {
                    Toast.makeText(ctx, "Please enter Name", Toast.LENGTH_SHORT).show();
                } else if (email.isEmpty()) {
                    Toast.makeText(ctx, "Please enter Email", Toast.LENGTH_SHORT).show();
                } else if (!email.matches("[0-9]+") && !((BaseActivity) ctx).isValidEmail(email)) {
                    Toast.makeText(ctx, "Please enter valid Email", Toast.LENGTH_SHORT).show();
                } else if (number.isEmpty()) {
                    Toast.makeText(ctx, "Please enter Mobile", Toast.LENGTH_SHORT).show();
                } else if (number.length() != 10) {
                    Toast.makeText(ctx, "Please enter valid Mobile", Toast.LENGTH_SHORT).show();
                } else {
                    closeActivity = false;
                    lDialog.dismiss();
                    checkIsRegisteredUser(name, email, number, from, ctx);
                }

            }

        });

        lDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if(closeActivity){
                    if (!from.equals("ScheduleVisitActivity") && !from.equals("BookingConfirmationActivity")) {
                        RegistrationActivity act = (RegistrationActivity) ctx;
                        act.finishActivity();
                    }
                }
            }
        });


        lDialog.getWindow().
                setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        LinearLayout llOut = (LinearLayout) lDialog.findViewById(R.id.llOutside);

        llOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lDialog.dismiss();
            }
        });
        lDialog.show();
    }

    public void checkIsRegisteredUser(final String name, final String email, final String number, final String from, final Activity ctx) {
        ConnectionDetector cd = new ConnectionDetector(ctx);
        ((BaseActivity)ctx).getWudstayURL();
        if (cd.isConnectingToInternet()) {
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<IsRegisteredPojo> call = apiService.isRegistered(number);
            call.enqueue(new Callback<IsRegisteredPojo>() {
                @Override
                public void onResponse(Call<IsRegisteredPojo> call, Response<IsRegisteredPojo> response) {
                    System.out.println("retrofit URL " + call.request());
                    if (response.body() != null && response.body().getResponseCode() == 1) {
                        String user_type = response.body().getResponseObject().getUserType();
                        BaseActivity context = (BaseActivity) ctx;
                        context.saveIntoPrefs(WudStayConstants.USER_TYPE, user_type);
                        context.getMobileVerificationCode(name, email, number, from, ctx);
                    }
                    else{
                        Toast.makeText(ctx, "Something went wrong", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<IsRegisteredPojo> call, Throwable t) {
                    // Log error here since request failed
                    System.out.println("hh failure: " + t.getMessage());
                }
            });
        }
    }

    public void showThanksDialog(String call_time) {
        lDialog = new Dialog(ctx, R.style.Theme_Dialog_Custom_Anim);
        lDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        lDialog.setContentView(R.layout.dialog_thanks_layout);
        lDialog.setCanceledOnTouchOutside(false);
        final TextView content_thanks = (TextView) lDialog.findViewById(R.id.content_thanks_dialog);

        content_thanks.setText(call_time);

        Button btnSubmit = (Button) lDialog.findViewById(R.id.thanks_dialog_btnSubmit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                lDialog.dismiss();
                ctx.finish();

            }

        });

        lDialog.show();
    }
    public void showPaymentModeDialog(View.OnClickListener onClick, PayuResponse payuResponse) {
        lDialog = new Dialog(ctx, R.style.Theme_Dialog_Custom_AnimBottomIn);
        lDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        lDialog.setContentView(R.layout.dialog_choose_payment_mode);
        ImageView imgClose = (ImageView) lDialog.findViewById(R.id.imgClose);
        LinearLayout llCreditDebit = (LinearLayout) lDialog.findViewById(R.id.linear_layout_credit_debit_card);
        LinearLayout llSaveCards = (LinearLayout) lDialog.findViewById(R.id.linear_layout_saved_card);
//        LinearLayout llPayZapp = (LinearLayout) lDialog.findViewById(R.id.llPayzapp);
        LinearLayout llPayuMoney = (LinearLayout) lDialog.findViewById(R.id.linear_layout_payumoney);
        LinearLayout linear_layout_netbanking = (LinearLayout) lDialog.findViewById(R.id.linear_layout_netbanking);

//        llPayZapp.setTag(lDialog);
        llCreditDebit.setTag(lDialog);
        llSaveCards.setTag(lDialog);
        llPayuMoney.setTag(lDialog);
        linear_layout_netbanking.setTag(lDialog);

//        llPayZapp.setOnClickListener(onClick);
        llCreditDebit.setOnClickListener(onClick);
        llSaveCards.setOnClickListener(onClick);
        llPayuMoney.setOnClickListener(onClick);
        linear_layout_netbanking.setOnClickListener(onClick);

        LinearLayout llOut = (LinearLayout) lDialog.findViewById(R.id.llOutside);
        llOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lDialog.dismiss();
            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lDialog.dismiss();
            }
        });

        if(payuResponse.isNetBanksAvailable()){ // okay we have net banks now.
            linear_layout_netbanking.setVisibility(View.VISIBLE);
        }
        if(payuResponse.isCreditCardAvailable() || payuResponse.isDebitCardAvailable()){
            llCreditDebit.setVisibility(View.VISIBLE);
        }
        if(payuResponse.isStoredCardsAvailable()){
            llSaveCards.setVisibility(View.VISIBLE);
        }
        if(payuResponse.isPaisaWalletAvailable() && payuResponse.getPaisaWallet().get(0).getBankCode().contains(PayuConstants.PAYUW)) {
            llPayuMoney.setVisibility(View.VISIBLE);
        }

        lDialog.show();
    }

    public void showProgressDialog(Activity activity) {
        lDialog = new Dialog(ctx, R.style.Theme_Dialog_Custom_My);
        lDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        lDialog.setContentView(R.layout.dialog_loading);
        ImageView imgCard = (ImageView) lDialog.findViewById(R.id.imgCard);
        imgCard.startAnimation(
                AnimationUtils.loadAnimation(ctx, R.anim.wudstay_loading_animation) );
        if (!activity.isFinishing() && !lDialog.isShowing())
            lDialog.show();
    }
    public Dialog getlDialog() {
        return lDialog;
    }
}
