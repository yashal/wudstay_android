package com.wudstay.util;

import android.content.Intent;
import android.widget.Toast;

import com.payu.india.Model.MerchantWebService;
import com.payu.india.Model.PaymentParams;
import com.payu.india.Model.PayuConfig;
import com.payu.india.Model.PayuHashes;
import com.payu.india.Model.PostData;
import com.payu.india.Payu.PayuConstants;
import com.payu.india.Payu.PayuErrors;
import com.payu.india.PostParams.MerchantWebServicePostParams;
import com.payu.india.PostParams.PaymentPostParams;
import com.payu.india.Tasks.GetPaymentRelatedDetailsTask;
import com.payu.payuui.Activity.PayUBaseActivity;
import com.payu.payuui.Activity.PaymentsActivity;
import com.wudstay.activity.BookingConfirmationActivity;
import com.wudstay.model.ApiClient;
import com.wudstay.model.ApiInterface;
import com.wudstay.pojo.PayURequestDataPojo;
import com.wudstay.pojo.PayURequestParametersPojo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PayuGateaway {

    private BookingConfirmationActivity acti;

    int env = PayuConstants.PRODUCTION_ENV;
    PayuHashes payuHashes;
    private PostData postData;
    String pgId, totalAmount, occupancy_type, visitDate, name, email, mobile;

    Intent intent;
    private PaymentParams mPaymentParams;
    private PayuConfig payuConfig;
    private PayURequestParametersPojo responseVO = null;


    public PayuGateaway(BookingConfirmationActivity activity, String pgId, String totalAmount, String occupancy_type, String visitDate,
                        String name, String email, String mobile) {
        acti = activity;
        this.pgId = pgId;
        this.totalAmount = totalAmount;
        this.occupancy_type = occupancy_type;
        this.visitDate = visitDate;
        this.name = name;
        this.email = email;
        this.mobile = mobile;
    }

    public void navigateToPayUSdk() {
        intent = new Intent(acti, PayUBaseActivity.class);
        mPaymentParams = new PaymentParams();
        payuConfig = new PayuConfig();

        payuConfig.setEnvironment(env);

        getPayuRequestParameters();

    }

    public void launchPayumoney() {
        // lets try to get the post params
        mPaymentParams.setHash(payuHashes.getPaymentHash());

        try {
            postData = new PaymentPostParams(mPaymentParams, PayuConstants.PAYU_MONEY).getPaymentPostParams();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (postData.getCode() == PayuErrors.NO_ERROR) {
            // launch webview
            payuConfig.setData(postData.getResult());
            Intent intent = new Intent(acti, PaymentsActivity.class);
            intent.putExtra(PayuConstants.PAYU_CONFIG, payuConfig);
            acti.startActivityForResult(intent, PayuConstants.PAYU_REQUEST_CODE);
        } else {
            Toast.makeText(acti, postData.getResult(), Toast.LENGTH_LONG).show();
        }

    }


    private void callOnCreateOfBase() {
        // lets collect the details from bundle to fetch the payment related details for a merchant
        payuConfig = null != payuConfig ? payuConfig : new PayuConfig();

        // TODO add null pointer check here

        MerchantWebService merchantWebService = new MerchantWebService();
        merchantWebService.setKey(mPaymentParams.getKey());
        merchantWebService.setCommand(PayuConstants.PAYMENT_RELATED_DETAILS_FOR_MOBILE_SDK);
        merchantWebService.setVar1(mPaymentParams.getUserCredentials() == null ? "default" : mPaymentParams.getUserCredentials());

        merchantWebService.setHash(payuHashes.getPaymentRelatedDetailsForMobileSdkHash());

        PostData postData = new MerchantWebServicePostParams(merchantWebService).getMerchantWebServicePostParams();
        if (postData.getCode() == PayuErrors.NO_ERROR) {
            // ok we got the post params, let make an api call to payu to fetch the payment related details
            payuConfig.setData(postData.getResult());

            GetPaymentRelatedDetailsTask paymentRelatedDetailsForMobileSdkTask = new GetPaymentRelatedDetailsTask(acti);
            paymentRelatedDetailsForMobileSdkTask.execute(payuConfig);
        }
    }

    public void launchActivity(Intent intent) {
        intent.putExtra(PayuConstants.PAYU_HASHES, payuHashes);
        intent.putExtra(PayuConstants.PAYMENT_PARAMS, mPaymentParams);
        payuConfig.setData(null);
        intent.putExtra(PayuConstants.PAYU_CONFIG, payuConfig);

        // salt
        intent.putExtra(PayuConstants.SALT, responseVO.getResponseObject().getPayuHashs().getSalt());

        acti.startActivityForResult(intent, PayuConstants.PAYU_REQUEST_CODE);
    }

    public void getPayuRequestParameters() {

        final WudstayDialogs progressDialog = new WudstayDialogs(acti);
        progressDialog.showProgressDialog(acti);
        progressDialog.getlDialog().setCancelable(Boolean.FALSE);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<PayURequestParametersPojo> call = apiService.getPayuRequestParameters(name, email, mobile, pgId, totalAmount,
                "Android", occupancy_type, "", visitDate, "44fZt5:"+mobile);
        System.out.println("retrofit URL " + call.request());
        call.enqueue(new Callback<PayURequestParametersPojo>() {
            @Override
            public void onResponse(Call<PayURequestParametersPojo> call, Response<PayURequestParametersPojo> response) {
                if (response.body().getResponseCode() == 1) {
                    String transaction_id = response.body().getResponseObject().getPayUVO().getTransactionId();
                    acti.saveIntoPrefs("transaction_id", transaction_id);

                    responseVO = response.body();
                    payuHashes = new PayuHashes();
                    if (responseVO != null && responseVO.getResponseCode() == 1) {

                        PayURequestDataPojo vo = responseVO.getResponseObject();
                        mPaymentParams.setKey(vo.getPayUVO().getMerchantKey());
                        mPaymentParams.setAmount(vo.getPayUVO().getAmount() + "");
                        mPaymentParams.setProductInfo(vo.getPayUVO().getProductInfo());
                        mPaymentParams.setFirstName(vo.getPayUVO().getFirstName());
                        mPaymentParams.setEmail(vo.getPayUVO().getEmail());
                        mPaymentParams.setTxnId(vo.getPayUVO().getTransactionId());
                        mPaymentParams.setSurl(vo.getPayUVO().getSurl());
                        mPaymentParams.setFurl(vo.getPayUVO().getFurl());
                        mPaymentParams.setUdf1(vo.getPayUVO().getUdf1());
                        mPaymentParams.setUdf2("");
                        mPaymentParams.setUdf3("");
                        mPaymentParams.setUdf4("");
                        mPaymentParams.setUdf5("");
                        mPaymentParams.setUserCredentials(vo.getPayUVO().getMerchantKey()+":"+mobile);
//                        mPaymentParams.setStoreCard(1);

                        payuConfig.setEnvironment(PayuConstants.PRODUCTION_ENV);

                        payuHashes.setPaymentHash(vo.getPayuHashs().getPaymentHash());
                        payuHashes.setMerchantIbiboCodesHash(vo.getPayuHashs().getMerchantCodesHash());
                        payuHashes.setPaymentRelatedDetailsForMobileSdkHash(vo.getPayuHashs().getDetailsForMobileSdk());
                        payuHashes.setVasForMobileSdkHash(vo.getPayuHashs().getMobileSdk());
                        payuHashes.setDeleteCardHash(vo.getPayuHashs().getDeleteHash());
                        payuHashes.setEditCardHash(vo.getPayuHashs().getEditUserCardHash());
                        payuHashes.setSaveCardHash(vo.getPayuHashs().getSaveUserCardHash());

                        callOnCreateOfBase();
                    } else {
                        if (responseVO != null)
                            Toast.makeText(acti, responseVO.getMessage(), Toast.LENGTH_LONG).show();
                        else {
                            Toast.makeText(acti, "Something went wrong... Try again", Toast.LENGTH_LONG).show();
                        }
                    }

                }
                progressDialog.getlDialog().dismiss();
            }

            @Override
            public void onFailure(Call<PayURequestParametersPojo> call, Throwable t) {
                // Log error here since request failed
                System.out.println("retrofit hh failure" + t.getMessage());
                progressDialog.getlDialog().dismiss();
            }
        });

    }
}
