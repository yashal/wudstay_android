package com.wudstay.util;

import android.app.Application;

import com.splunk.mint.Mint;


/**
 * Created by quepplin1 on 5/17/2016.
 */
public class WudstayApplication extends Application {


    @Override
    public void onCreate() {
        super.onCreate();

        Mint.initAndStartSession(getApplicationContext(), "be5aa41f");

    }
}
