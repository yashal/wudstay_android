package com.wudstay.util;

import android.app.Activity;

import java.util.ArrayList;

/**
 * Created by Administrator on 23-Aug-16.
 */
public class WudStayConstants {

    public static ArrayList<Activity> ACTIVITIES = new ArrayList<>();
    public final static String PREF_NAME = "com.wudstay.prefs";
    public final static String DEFAULT_VALUE = "";
    //    public final static String BASE_URL_WUDSTAY = "http://54.251.116.126:8080/wudstay/webservice/";
//  public final static String BASE_URL_TMS = "http://54.251.116.126:8080/rentout/webservice/";
//  public static String BASE_URL = "http://54.251.116.126:8080/wudstay/webservice/";
//    public static String BASE_URL_IMAGE = "http://54.251.116.126:8080/wudstay/";
    public final static String BASE_URL_TMS = "http://www.wudstay.com/rentout/webservice/";
    public final static String BASE_URL_WUDSTAY = "http://www.wudstay.com/webservice/";
    public static String BASE_URL = "http://www.wudstay.com/webservice/";
    public static String BASE_URL_IMAGE = "http://www.wudstay.com/";
    public final static String NO_INTERNET_CONNECTED = "No internet connection available";
    public static final String ACTION_RECEIVED_OTP = "wudsta.ACTION_RECEIVED_OTP";
    //    public static final String USER_CREDENTIALS = "wudstay:1234";
    public static final String SOURCE = "Android";
    public static String APP_NOT_AVAILABLE = "Sorry, unable to find market app.";
    public static String MOBILE = "mobile";
    public static String EMAIL = "email";
    public static String NAME = "name";
    public static String IMAGE = "image";
    public static String USER_TYPE = "user_type";
    public static String ABOUT_US = "http://www.wudstay.com/m/appAboutUs.do";
    public static String CONTACT_US = "http://www.wudstay.com/m/appContactUs.do";
    public static String PRIVACY_POLICY = "http://www.wudstay.com/m/appPrivacyPolicy.do";
    public static String CANCELLATION_POLICY = "http://www.wudstay.com/m/appCancellationPolicy.do";
    public static String TERMS_AND_CONDITIONS = "http://www.wudstay.com/m/appTermsAndCondition.do";
    public static String BLOG = "http://blog.wudstay.com/feed/";
    public static String AMOUNT = "Amount to be paid";
    public static String WRONG_OTP_MSG = "Please enter the correct OTP";
}
