package com.wudstay.model;


import com.wudstay.pojo.ImageUploadPojo;
import com.wudstay.pojo.ProfilePojo;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by yesh on 10/14/2015.
 */
public interface FileUploadSer {


    public interface FileUploadService {
        @Multipart
        @POST("uploadCandidateDp.do")
        Call<ImageUploadPojo> upload(@Part("mobileNumber") RequestBody mobileNumber,
                                     @Part MultipartBody.Part uploadedFile);


        @Multipart
        @POST("updateMyProfile.do")
        Call<ProfilePojo> uploadCompleteProfile(@Part("mobileNumber") RequestBody mobileNumber, @Part MultipartBody.Part uploadedFile,
                                                @Part("firstName") RequestBody firstName, @Part("lastName") RequestBody lastName,
                                                @Part("email") RequestBody email, @Part("homeAddressLine1") RequestBody homeAddressLine1,
                                                @Part("homeAddressLine2") RequestBody homeAddressLine2, @Part("cityName") RequestBody cityName,
                                                @Part("stateId") RequestBody stateId, @Part("pin") RequestBody pin, @Part("phone") RequestBody phone,
                                                @Part("emergencyContactPersonFirstName") RequestBody emergencyContactPersonFirstName, @Part("emergencyContactPersonLastName") RequestBody emergencyContactPersonLastName,
                                                @Part("emergencyContactPersonMobile") RequestBody emergencyContactPersonMobile, @Part("emergencyContactPersonEmail") RequestBody emergencyContactPersonEmail,
                                                @Part("emergencyContactPersonRelation") RequestBody emergencyContactPersonRelation, @Part("emergencyContactPersonFullAddress") RequestBody emergencyContactPersonFullAddress,
                                                @Part("emergencyContactPersonPhone") RequestBody emergencyContactPersonPhone

                                             );
    }

}
