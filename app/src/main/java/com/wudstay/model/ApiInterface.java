package com.wudstay.model;


import com.wudstay.pojo.FavoriteResponsePojo;
import com.wudstay.pojo.IsRegisteredPojo;
import com.wudstay.pojo.MobileNoVerificationPojo;
import com.wudstay.pojo.MyFavPgPojo;
import com.wudstay.pojo.NewUpdatePojo;
import com.wudstay.pojo.NotificationPojo;
import com.wudstay.pojo.NotificationReadPojo;
import com.wudstay.pojo.PGCitiesPojo;
import com.wudstay.pojo.PGDetailResponseNewPojo;
import com.wudstay.pojo.PayURequestParametersPojo;
import com.wudstay.pojo.PayUResponsePojo;
import com.wudstay.pojo.PaymentHistoryPojo;
import com.wudstay.pojo.PgComplaintPojo;
import com.wudstay.pojo.PgListNewResponsePojo;
import com.wudstay.pojo.PgListResponsePojo;
import com.wudstay.pojo.PgLocationsPojo;
import com.wudstay.pojo.ProfilePojo;
import com.wudstay.pojo.ScheduleVisitPojo;
import com.wudstay.pojo.getPgComplaintPojo;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("getAllPgCities.do")
    Call<PGCitiesPojo> getPGCities();

    @GET("textbasedcitysearch.do")
    Call<PGCitiesPojo> getLocations(@Query("text") String text);

    @GET("scheduleVisit.do")
    Call<ScheduleVisitPojo> scheduleVisit(@Query("pgId") Long pgId, @Query("name") String name, @Query("email") String email, @Query("mobileNumber") String mobileNumber, @Query("meetingTime") String meetingTime, @Query("meetingDate") String meetingDate);

    @GET("searchPgs.do")
    Call<PgListResponsePojo> getPGList(@Query("pgCityId") String pgCityId, @Query("pglocalityIds") String pglocalityIds, @Query("pgGenderIds") String pgGenderIds, @Query("bookingOccupancy") String bookingOccupancy, @Query("moveInDate") String moveInDate, @Query("mobileNumber") String mobileNumber);

    @GET("getAllPgLocationInCity.do")
    Call<PgLocationsPojo> getLocationsList(@Query("cityId") Long cityId);

    @GET("sendMobileNumberVerificationCode.do")
    Call<MobileNoVerificationPojo> getMobileNoVerification(@Query("name") String name, @Query("email") String email, @Query("mobileNumber") String mobileNumber);

    @GET("verifyMobileNumber.do")
    Call<MobileNoVerificationPojo> getSendOTP(@Query("mobileNumber") String mobileNumber, @Query("verificationCode") String verificationCode);

    @GET("isregistered.do")
    Call<IsRegisteredPojo> isRegistered(@Query("mobileNumber") String mobileNumber);

    @GET("getUserProfile.do")
    Call<ProfilePojo> getUserProfile(@Query("mobileNumber") String mobileNumber);

    @GET("getPayuRequestParameters.do")
    Call<PayURequestParametersPojo> getPayuRequestParameters(@Query("name") String name, @Query("email") String email, @Query("mobileNumber") String mobileNumber, @Query("pgId") String pgId,
                                                             @Query("totalAmount") String totalAmount, @Query("source") String source, @Query("occupancy") String occupancy, @Query("roomNumber") String roomNumber,
                                                             @Query("moveInDate") String moveInDate, @Query("user_credentials") String user_credentials);

    @GET("updateUserProfile.do")
    Call<ProfilePojo> updateUserProfile(@Query("mobileNumber") String mobileNumber, @Query("firstName") String firstName, @Query("lastName") String lastName, @Query("email") String email,
                                        @Query("homeAddressLine1") String homeAddressLine1, @Query("homeAddressLine2") String homeAddressLine2, @Query("cityName") String cityName, @Query("stateId") String stateId,
                                        @Query("pin") String pin, @Query("phone") String phone, @Query("emergencyContactPersonFirstName") String emergencyContactPersonFirstName,
                                        @Query("emergencyContactPersonLastName") String emergencyContactPersonLastName, @Query("emergencyContactPersonMobile") String emergencyContactPersonMobile,
                                        @Query("emergencyContactPersonEmail") String emergencyContactPersonEmail, @Query("emergencyContactPersonRelation") String emergencyContactPersonRelation,
                                        @Query("emergencyContactPersonFullAddress") String emergencyContactPersonFullAddress, @Query("emergencyContactPersonPhone") String emergencyContactPersonPhone);


    @GET("doPgComplaint.do")
    Call<PgComplaintPojo> NewPgComplaint(@Query("mobileNumber") String mobileNumber, @Query("pgComplaintType") String pgComplaintType, @Query("description") String description);

    @GET("getPgComplaintList.do")
    Call<getPgComplaintPojo> getPgComplaint(@Query("mobileNumber") String mobileNumber, @Query("maxItems") String maxItems);

    @GET("pgPaymentHistory.do")
    Call<PaymentHistoryPojo> getPaymentHistory(@Query("mobileNumber") String mobileNumber);

    @GET("getPgUserNotificationList.do")
    Call<NotificationPojo> getUserNotificationList(@Query("mobileNumber") String mobileNumber);

    @GET("readMarkNotification.do")
    Call<NotificationReadPojo> readMarkNotification(@Query("mobileNumber") String mobileNumber, @Query("userNotificationId") String userNotificationId);

    @GET("getNewUpdateIsMandatory.do")
    Call<NewUpdatePojo> getNewUpdate(@Query("appType") String appType, @Query("currentVersion") String currentVersion);

    @GET("getIosPayuResponse.do")
    Call<PayUResponsePojo> getPayuResponse(@Query("transactionId") String transactionId);

    @GET("favouritePg.do")
    Call<FavoriteResponsePojo> updateFavorite(@Query("mobileNumber") String mobileNumber, @Query("pgId") String pgId, @Query("isRemove") String isRemove);

    @GET("getPayuRequestParameters.do")
    Call<PayURequestParametersPojo> PaymentPayuRequestParameters(@Query("name") String name, @Query("email") String email, @Query("mobileNumber") String mobileNumber, @Query("pgId") String pgId,
                                                             @Query("totalAmount") String totalAmount, @Query("source") String source, @Query("occupancy") String occupancy, @Query("roomNumber") String roomNumber,
                                                             @Query("moveInDate") String moveInDate, @Query("user_credentials") String user_credentials, @Query("isMonthlyRent") String isMonthlyRent, @Query("invoiceNumber") String invoiceNumber);

    @GET("myFavouritePgList.do")
    Call<MyFavPgPojo> getFavPgList(@Query("mobileNumber") String mobileNumber);

    @GET("searchProperties.do")
    Call<PgListNewResponsePojo> getPGListNew(@Query("cityId") String cityId, @Query("cityUrl") String cityUrl,
                                             @Query("currentPage") int currentPage, @Query("sortBy") String sortBy,
                                             @Query("propLocationId") long propLocationId, @Query("locationIds") String locationIds,
                                             @Query("propCategoryTypeId") long propCategoryTypeId, @Query("propertyTypeIds") String propertyTypeIds,
                                             @Query("propCategoryCatId") long propCategoryCatId, @Query("propertyCatIds") String propertyCatIds,
                                             @Query("lowestPrice") int lowestPrice,  @Query("maximumPrice") int maximumPrice,
                                             @Query("occupancyIds") String occupancyIds );

//    , @Query("pglocalityIds") String pglocalityIds, @Query("pgGenderIds") String pgGenderIds, @Query("bookingOccupancy") String bookingOccupancy, @Query("moveInDate") String moveInDate, @Query("mobileNumber") String mobileNumber

    @GET("propertyDetails.do")
    Call<PGDetailResponseNewPojo> getPGDetailNew(@Query("propertyId") String propertyId, @Query("mobileNumber") String mobileNumber);
}
