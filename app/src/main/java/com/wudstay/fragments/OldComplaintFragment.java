package com.wudstay.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.wudstay.R;
import com.wudstay.activity.ComplaintsActivity;
import com.wudstay.adapter.ComplaintListAdapter;
import com.wudstay.model.ApiClient;
import com.wudstay.model.ApiInterface;
import com.wudstay.pojo.PgComplaintDataPojo;
import com.wudstay.pojo.getPgComplaintPojo;
import com.wudstay.util.ConnectionDetector;
import com.wudstay.util.WudStayConstants;
import com.wudstay.util.WudstayDialogs;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Yash ji on 4/22/2016.
 */
public class OldComplaintFragment extends Fragment {

    private ComplaintsActivity ctx;
    private View view;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private ConnectionDetector cd;
    private WudstayDialogs dialog;
    private ArrayList<PgComplaintDataPojo> complaintList;
    private String maxItems = "";
    private LinearLayout no_complaint_layout;

    private boolean fragmentResume = false;
    private boolean fragmentVisible = false;
    private boolean fragmentOnCreated = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_old_complaint, container, false);

        ctx = (ComplaintsActivity) getActivity();

//        cd = new ConnectionDetector(ctx);
//        dialog = new WudstayDialogs(ctx);
        no_complaint_layout = (LinearLayout) view.findViewById(R.id.no_complaint_layout);
        recyclerView = (RecyclerView) view.findViewById(R.id.complaint_recycler_view);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);

        if (complaintList == null)
            complaintList = new ArrayList<>();

        if (!fragmentResume && fragmentVisible && complaintList != null && complaintList.size() == 0) { //only when first time fragment is created
            getComplaintList();
        }


        return view;
    }

    private void getComplaintList()
    {
        cd = new ConnectionDetector(ctx);
        dialog = new WudstayDialogs(ctx);
        if (cd.isConnectingToInternet()) {
            final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<getPgComplaintPojo> call = apiService.getPgComplaint(ctx.getFromPrefs(WudStayConstants.MOBILE), maxItems);
            call.enqueue(new Callback<getPgComplaintPojo>() {
                @Override
                public void onResponse(Call<getPgComplaintPojo> call, Response<getPgComplaintPojo> response) {
                    System.out.println("retrofit URL " + call.request());
                    if (response.body() != null){

                        if (response.body().getResponseCode() == 1) {
                            complaintList = response.body().getResponseObject().getListOfComplaints();
                            if(complaintList != null && complaintList.size() > 0) {
                                mAdapter = new ComplaintListAdapter(ctx, complaintList);
                                recyclerView.setAdapter(mAdapter);
                                no_complaint_layout.setVisibility(View.GONE);
                            }
                            else{
                                no_complaint_layout.setVisibility(View.VISIBLE);
                            }
                        }
                        else{
                            no_complaint_layout.setVisibility(View.VISIBLE);
                        }
                    }

                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<getPgComplaintPojo> call, Throwable t) {
                    // Log error here since request failed
                    progressDialog.getlDialog().dismiss();
                }
            });
        } else {
            dialog.displayCommonDialog(WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser && isResumed() && complaintList != null && complaintList.size() == 0) {   // only at fragment screen is resumed
            fragmentResume = true;
            fragmentVisible = false;
            fragmentOnCreated = true;

            getComplaintList();
        } else if (isVisibleToUser) {        // only at fragment onCreated
            fragmentResume = false;
            fragmentVisible = true;
            fragmentOnCreated = true;
        } else if (!isVisibleToUser && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false;
            fragmentResume = false;
        }

    }


}
