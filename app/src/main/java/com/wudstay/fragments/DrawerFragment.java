package com.wudstay.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wudstay.R;
import com.wudstay.activity.BaseActivity;
import com.wudstay.activity.BlogActivity;
import com.wudstay.activity.ComplaintsActivity;
import com.wudstay.activity.FavouritesActivity;
import com.wudstay.activity.HomeActivity;
import com.wudstay.activity.NeedsToTalkActivity;
import com.wudstay.activity.NotificationsActivity;
import com.wudstay.activity.PaymentActivity;
import com.wudstay.activity.ProfileActivity;
import com.wudstay.activity.RegistrationActivity;
import com.wudstay.activity.SettingsActivity;
import com.wudstay.activity.WebViewsActivity;
import com.wudstay.util.ConnectionDetector;
import com.wudstay.util.WudStayConstants;
import com.wudstay.util.WudstayDialogs;


public class DrawerFragment extends Fragment {

    public ActionBarDrawerToggle mDrawerToggle;
    private int selectedPos = -1;
    private DrawerLayout drawerlayout;
    private TextView notification_title, download_title;
    private TextView logout_title, userName, myAcc, cart_count_drawer;
    private ImageView userImage;
    private LinearLayout talk, name_text_layout, dear_user_text_layout;

    private ConnectionDetector cd;
    private WudstayDialogs dialog;
    private LinearLayout logout;

    public DrawerFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.left_slide_list_layout, container, false);

        selectedPos = -1;

        cd = new ConnectionDetector(getActivity());
        dialog = new WudstayDialogs(getActivity());

        dear_user_text_layout = (LinearLayout) v.findViewById(R.id.dear_user_text_layout);
        final LinearLayout home = (LinearLayout) v.findViewById(R.id.home_menu_option);
        final LinearLayout notification = (LinearLayout) v.findViewById(R.id.notification_menu_option);
        final LinearLayout payment = (LinearLayout) v.findViewById(R.id.payment_menu_option);
        talk = (LinearLayout) v.findViewById(R.id.talk_menu_option);
        name_text_layout = (LinearLayout) v.findViewById(R.id.name_text_layout);
        final LinearLayout settings = (LinearLayout) v.findViewById(R.id.settings_menu_option);
        final LinearLayout complaint = (LinearLayout) v.findViewById(R.id.complaint_menu_option);
        final LinearLayout blog = (LinearLayout) v.findViewById(R.id.blog_menu_option);
        final LinearLayout about_us = (LinearLayout) v.findViewById(R.id.about_us_menu_option);
        final LinearLayout favourites = (LinearLayout) v.findViewById(R.id.favourite_menu_option);
        final LinearLayout contact_us = (LinearLayout) v.findViewById(R.id.contact_us_menu_option);
        final LinearLayout privacy_policy = (LinearLayout) v.findViewById(R.id.privacy_menu_option);
        logout = (LinearLayout) v.findViewById(R.id.logout_option);
        final LinearLayout tnc_menu_option = (LinearLayout) v.findViewById(R.id.tnc_menu_option);
        final LinearLayout cancellation_policy = (LinearLayout) v.findViewById(R.id.cancel_menu_option);

        setClickListener(home, 0);
        setClickListener(notification, 1);
        setClickListener(payment, 2);
        setClickListener(talk, 3);
        setClickListener(complaint, 4);
        setClickListener(settings, 5);
        setClickListener(blog, 6);
        setClickListener(about_us, 7);
        setClickListener(favourites, 8);
        setClickListener(contact_us, 9);
        setClickListener(tnc_menu_option, 10);
        setClickListener(privacy_policy, 11);
        setClickListener(logout, 12);
        setClickListener(cancellation_policy, 13);

        ImageView home_icon = (ImageView) v.findViewById(R.id.home_icon);
        ImageView talk_icon = (ImageView) v.findViewById(R.id.talk_icon);
        ImageView notification_icon = (ImageView) v.findViewById(R.id.notification_icon);
        ImageView payment_icon = (ImageView) v.findViewById(R.id.payment_icon);
        ImageView complaint_icon = (ImageView) v.findViewById(R.id.complaint_icon);
        ImageView settings_icon = (ImageView) v.findViewById(R.id.settings_icon);
        ImageView logout_icon = (ImageView) v.findViewById(R.id.logout_icon);

        TextView home_title = (TextView) v.findViewById(R.id.home_title);
        TextView talk_title = (TextView) v.findViewById(R.id.talk_title);
        TextView notification_title = (TextView) v.findViewById(R.id.notification_title);
        TextView payment_title = (TextView) v.findViewById(R.id.payment_title);
        TextView complaint_title = (TextView) v.findViewById(R.id.complaint_title);
        TextView settings_title = (TextView) v.findViewById(R.id.settings_title);
        TextView logout_title = (TextView) v.findViewById(R.id.logout_title);


        userName = (TextView) v.findViewById(R.id.home_menu_UserName);
        myAcc = (TextView) v.findViewById(R.id.home_menu_MyAcc);
        userImage = (ImageView) v.findViewById(R.id.home_menu_UserImage);

        if (getFromPrefs("name") != null && !getFromPrefs("name").equals("")) {
            logout.setVisibility(View.VISIBLE);

            userName.setText(getFromPrefs("name"));
            myAcc.setText("Edit Profile");
            if (getFromPrefs("image") != null && !getFromPrefs("image").equals(""))
                ((BaseActivity) getActivity()).setProfileImageInLayout(getActivity(), (int) getResources().getDimension(R.dimen.profile_image), (int) getResources().getDimension(R.dimen.profile_image),
                        getFromPrefs("image"), userImage);
        } else {
            logout.setVisibility(View.GONE);
            userName.setText(getString(R.string.dear_user));
            myAcc.setText("Register/Login");
        }

        dear_user_text_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        myAcc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getFromPrefs("mobile") == null || getFromPrefs("mobile").equals("")) {
                    Intent intent = new Intent(getActivity(), RegistrationActivity.class);
                    intent.putExtra("navigate_from", "MyAcc");
                    startActivity(intent);
                    drawerlayout.closeDrawers();
                } else {
                    startActivity(new Intent(getActivity(), ProfileActivity.class));
                    drawerlayout.closeDrawers();
                }

            }
        });

        return v;
    }

    private void setClickListener(final LinearLayout layout, final int pos) {
        layout.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        break;
                    case MotionEvent.ACTION_UP:
                        selectedPos = pos;
                        drawerlayout.closeDrawers();
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        selectedPos = -1;
                        break;
                }
                return true;
            }
        });
    }

    @SuppressLint("NewApi")
    public void setUp(final DrawerLayout drawerlayout) {

        this.drawerlayout = drawerlayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerlayout, R.string.open_drawer, R.string.close_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
                ((BaseActivity) getActivity()).hideSoftKeyboard();
                if (getFromPrefs("name") != null && !getFromPrefs("name").equals("")) {
                    logout.setVisibility(View.VISIBLE);
                    userName.setText(getFromPrefs("name"));
                    myAcc.setText("Edit Profile");
                    if (getFromPrefs("image") != null && !getFromPrefs("image").equals(""))
                        ((BaseActivity) getActivity()).setProfileImageInLayout(getActivity(), (int) getResources().getDimension(R.dimen.profile_image), (int) getResources().getDimension(R.dimen.profile_image),
                                getFromPrefs("image"), userImage);
                } else {
                    logout.setVisibility(View.GONE);
                    userName.setText(getString(R.string.dear_user));
                    myAcc.setText("Register/Login");
                }
            }

            @SuppressLint("NewApi")
            @Override
            public void onDrawerClosed(View drawerView) {
                ActivityManager am = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
                ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
                super.onDrawerClosed(drawerView);
                if (selectedPos != -1) {
                    switch (selectedPos) {
                        case 0:
                            if (!cn.getShortClassName().equals(".activity.HomeActivity")) {
                                navigateToHome();
                            } else {
                                drawerlayout.closeDrawers();
                            }
                            break;
                        case 1:
                            if (!cn.getShortClassName().equals(".activity.NotificationsActivity")) {
                                navigateToNotifications();
                            } else {
                                drawerlayout.closeDrawers();
                            }
                            break;
                        case 2:
                            if (!cn.getShortClassName().equals(".activity.PaymentActivity")) {
                                if (getFromPrefs("mobile") == null || getFromPrefs("mobile").equals("")) {
                                    Intent intent = new Intent(getActivity(), RegistrationActivity.class);
                                    intent.putExtra("navigate_from", "PaymentActivity");
                                    startActivity(intent);
                                    drawerlayout.closeDrawers();
                                } else {
                                    navigateToPayments();
                                }

                            } else {
                                drawerlayout.closeDrawers();
                            }
                            break;
                        case 3:
                            if (!cn.getShortClassName().equals(".activity.NeedsToTalkActivity")) {
                                navigateToTalks();
                            } else {
                                drawerlayout.closeDrawers();
                            }
                            break;
                        case 4:
                            if (!cn.getShortClassName().equals(".activity.ComplaintsActivity")) {
                                if (getFromPrefs("mobile") == null || getFromPrefs("mobile").equals("")) {
                                    Intent intent = new Intent(getActivity(), RegistrationActivity.class);
                                    intent.putExtra("navigate_from", "ComplaintsActivity");
                                    startActivity(intent);
                                    drawerlayout.closeDrawers();
                                } else {
                                    navigateToComplaints();
                                }
                            } else {
                                drawerlayout.closeDrawers();
                            }
                            break;
                        case 5:
                            if (!cn.getShortClassName().equals(".activity.SettingsActivity")) {
                                navigateToSettingsScreens();
                            } else {
                                drawerlayout.closeDrawers();
                            }
                            break;

                        case 6:
                            if (!cn.getShortClassName().equals(".activity.BlogActivity")) {
                                navigateToBlog();
                            } else {
                                drawerlayout.closeDrawers();
                            }
                            break;

                        case 7:
                            navigateToAboutUs();
                            break;

                        case 8:
                            if (!cn.getShortClassName().equals(".activity.FavouritesActivity")) {
                                if (getFromPrefs("mobile") == null || getFromPrefs("mobile").equals("")) {
                                    Intent intent = new Intent(getActivity(), RegistrationActivity.class);
                                    intent.putExtra("navigate_from", "FavouritesActivity");
                                    startActivity(intent);
                                    drawerlayout.closeDrawers();
                                } else {
                                    navigateTofavourites();
                                }
                            } else {
                                drawerlayout.closeDrawers();
                            }
                            break;

                        case 9:
                            navigateToContactUs();
                            break;

                        case 10:
                            navigateToTnc();
                            break;

                        case 11:
                            navigateToPrivacyPolicy();
                            break;

                        case 12:
                            logout();
                            break;

                        case 13:
                            navigateToCancellationPolicy();
                            break;

                        default:
                            break;
                    }
                    selectedPos = -1;
                }
            }
        };

        drawerlayout.setDrawerListener(mDrawerToggle);

    }

    public String getFromPrefs(String key) {
        SharedPreferences prefs = getActivity().getSharedPreferences(WudStayConstants.PREF_NAME, Context.MODE_PRIVATE);
        return prefs.getString(key, WudStayConstants.DEFAULT_VALUE);
    }

    private void navigateToHome() {
        drawerlayout.closeDrawers();
        for (int i = 0; i < WudStayConstants.ACTIVITIES.size(); i++) {
            if (WudStayConstants.ACTIVITIES.get(i) != null)
                WudStayConstants.ACTIVITIES.get(i).finish();
        }
        getActivity().finish();
        Intent intent = new Intent(getActivity(), HomeActivity.class);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    private void navigateToSettingsScreens() {
        drawerlayout.closeDrawers();
        Intent intent = new Intent(getActivity(), SettingsActivity.class);
        startActivity(intent);
    }

    private void navigateToPayments() {
        drawerlayout.closeDrawers();
        removeActivity(getResources().getString(R.string.package_name) + ".PaymentActivity");
        Intent mIntent = new Intent(getActivity(), PaymentActivity.class);
        startActivity(mIntent);

    }

    private void navigateTofavourites() {
        drawerlayout.closeDrawers();
        removeActivity(getResources().getString(R.string.package_name) + ".FavouritesActivity");
        Intent mIntent = new Intent(getActivity(), FavouritesActivity.class);
        startActivity(mIntent);

    }

    private void navigateToNotifications() {
        drawerlayout.closeDrawers();
        removeActivity(getResources().getString(R.string.package_name) + ".NotificationsActivity");
        Intent mIntent = new Intent(getActivity(), NotificationsActivity.class);
        startActivity(mIntent);

    }

    private void navigateToComplaints() {
        removeActivity(getResources().getString(R.string.package_name) + ".ComplaintsActivity");
        Intent mIntent = new Intent(getActivity(), ComplaintsActivity.class);
        startActivity(mIntent);
        drawerlayout.closeDrawers();
    }

    private void navigateToTalks() {
        drawerlayout.closeDrawers();
        Intent mIntent = new Intent(getActivity(), NeedsToTalkActivity.class);
        startActivity(mIntent);
    }

    private void navigateToBlog() {
        drawerlayout.closeDrawers();
        removeActivity(getResources().getString(R.string.package_name) + ".BlogActivity");
        Intent intent = new Intent(getActivity(), BlogActivity.class);
        startActivity(intent);

    }

    private void navigateToAboutUs() {
        drawerlayout.closeDrawers();
        removeActivity(getResources().getString(R.string.package_name) + ".WebViewsActivity");
        Intent intent = new Intent(getActivity(), WebViewsActivity.class);
        intent.putExtra("url", WudStayConstants.ABOUT_US);
        intent.putExtra("header", getString(R.string.about_us));
        startActivity(intent);

    }

    private void navigateToContactUs() {
        drawerlayout.closeDrawers();
        removeActivity(getResources().getString(R.string.package_name) + ".WebViewsActivity");
        Intent intent = new Intent(getActivity(), WebViewsActivity.class);
        intent.putExtra("url", WudStayConstants.CONTACT_US);
        intent.putExtra("header", getString(R.string.contact_us));
        startActivity(intent);
    }

    private void navigateToTnc() {
        drawerlayout.closeDrawers();
        removeActivity(getResources().getString(R.string.package_name) + ".WebViewsActivity");
        Intent intent = new Intent(getActivity(), WebViewsActivity.class);
        intent.putExtra("url", WudStayConstants.TERMS_AND_CONDITIONS);
        intent.putExtra("header", getString(R.string.tnc));
        startActivity(intent);
    }

    private void navigateToPrivacyPolicy() {
        drawerlayout.closeDrawers();
        removeActivity(getResources().getString(R.string.package_name) + ".WebViewsActivity");
        Intent intent = new Intent(getActivity(), WebViewsActivity.class);
        intent.putExtra("url", WudStayConstants.PRIVACY_POLICY);
        intent.putExtra("header", getString(R.string.privacy_policy));
        startActivity(intent);
    }

    private void navigateToCancellationPolicy() {
        drawerlayout.closeDrawers();
        Intent intent = new Intent(getActivity(), WebViewsActivity.class);
        intent.putExtra("url", WudStayConstants.CANCELLATION_POLICY);
        intent.putExtra("header", "Cancellation Policy");
        startActivity(intent);
    }

    private void logout() {
        WudstayDialogs logout_dialogs = new WudstayDialogs(getActivity());
        logout_dialogs.displayDialogWithCancel("Are you sure you want to Logout");
    }

    private void removeActivity(String activity) {
        for (int i = 0; i < WudStayConstants.ACTIVITIES.size(); i++) {
            if (WudStayConstants.ACTIVITIES.get(i) != null && WudStayConstants.ACTIVITIES.get(i).toString().contains(activity)) {
                WudStayConstants.ACTIVITIES.get(i).finish();
                WudStayConstants.ACTIVITIES.remove(i);
                break;
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

}
