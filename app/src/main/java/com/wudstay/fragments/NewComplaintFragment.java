package com.wudstay.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.wudstay.R;
import com.wudstay.activity.ComplaintsActivity;
import com.wudstay.model.ApiClient;
import com.wudstay.model.ApiInterface;
import com.wudstay.pojo.PgComplaintPojo;
import com.wudstay.util.ConnectionDetector;
import com.wudstay.util.WudStayConstants;
import com.wudstay.util.WudstayDialogs;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Yash ji on 4/22/2016.
 */
public class NewComplaintFragment extends Fragment {

    private ComplaintsActivity ctx;
    private View view;
    private LinearLayout clearnessLayout, powerBackupLayout, fittingLayout, securityLayout, otherLayout;
    private EditText complaint_description;
    private ImageView otherImage, securityImage, fittingImage, powerBackupImage, clearnessImage;
    private int complaintType = 0;
    private LinearLayout new_complaint_confirm;
    private ConnectionDetector cd;
    private WudstayDialogs dialog;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_new_complaint, container, false);

        clearnessLayout = (LinearLayout)view.findViewById(R.id.clearnessLayout);
        powerBackupLayout = (LinearLayout)view.findViewById(R.id.powerBackupLayout);
        fittingLayout = (LinearLayout)view.findViewById(R.id.fittingLayout);
        securityLayout = (LinearLayout)view.findViewById(R.id.securityLayout);
        otherLayout = (LinearLayout)view.findViewById(R.id.otherLayout);

        new_complaint_confirm = (LinearLayout)view.findViewById(R.id.new_complaint_confirm);

        complaint_description = (EditText) view.findViewById(R.id.new_complaint_description);

        otherImage = (ImageView) view.findViewById(R.id.otherImage);
        securityImage = (ImageView) view.findViewById(R.id.securityImage);
        fittingImage = (ImageView) view.findViewById(R.id.fittingImage);
        powerBackupImage = (ImageView) view.findViewById(R.id.powerBackupImage);
        clearnessImage = (ImageView) view.findViewById(R.id.clearnessImage);

        ctx = (ComplaintsActivity) getActivity();
        cd = new ConnectionDetector(ctx);
        dialog = new WudstayDialogs(ctx);

        clearnessLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioButtonClickListener(clearnessImage, powerBackupImage,fittingImage, securityImage, otherImage );
                complaintType = 1;
            }
        });
        powerBackupLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioButtonClickListener(powerBackupImage, clearnessImage,fittingImage, securityImage, otherImage );
                complaintType = 2;
            }
        });
        fittingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioButtonClickListener(fittingImage, powerBackupImage, clearnessImage, securityImage, otherImage );
                complaintType = 3;
            }
        });
        securityLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioButtonClickListener(securityImage, fittingImage, powerBackupImage, clearnessImage,  otherImage );
                complaintType = 4;
            }
        });
        otherLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioButtonClickListener(otherImage, fittingImage, powerBackupImage, clearnessImage, securityImage );
                complaintType = 0;
            }
        });

        new_complaint_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mobileNumber = ctx.getFromPrefs(WudStayConstants.MOBILE);
                String str_complaintType = ""+complaintType;
                String description = complaint_description.getText().toString();

                if (mobileNumber == null ||mobileNumber.equals(""))
                {
                    Toast.makeText(ctx, "Please register your Mobile", Toast.LENGTH_SHORT).show();
                }
                else if(str_complaintType == null ||str_complaintType.equals(""))
                {
                    Toast.makeText(ctx, "Please Select Complaint Type", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    makePgComplaint(mobileNumber, str_complaintType, description);
                }

            }
        });

        return view;
    }

    private void makePgComplaint(String mobileNumber, String str_complaintType, String description)
    {
        if (cd.isConnectingToInternet()) {

            final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<PgComplaintPojo> call = apiService.NewPgComplaint(mobileNumber, str_complaintType, description);
            call.enqueue(new Callback<PgComplaintPojo>() {
                @Override
                public void onResponse(Call<PgComplaintPojo> call, Response<PgComplaintPojo> response) {
                    System.out.println("retrofit URL " + call.request());
                    if (response.body().getResponseCode() == 1) {
                        Toast.makeText(ctx, "Your complaint has been registered. Your complaint ID is "+response.body().getResponseObject().getId(), Toast.LENGTH_SHORT).show();
                        ctx.finish();
                    }
                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<PgComplaintPojo> call, Throwable t) {
                    // Log error here since request failed
                    progressDialog.getlDialog().dismiss();
                }
            });
        } else {
            dialog.displayCommonDialog(WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }

    private void radioButtonClickListener(ImageView selectedOne, ImageView unselectedOne, ImageView unselectedTwo, ImageView unselectedThree, ImageView unselectedfour)
    {
        selectedOne.setImageResource(R.mipmap.radio_selected);
        unselectedOne.setImageResource(R.mipmap.radio_button);
        unselectedTwo.setImageResource(R.mipmap.radio_button);
        unselectedThree.setImageResource(R.mipmap.radio_button);
        unselectedfour.setImageResource(R.mipmap.radio_button);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

    }
}